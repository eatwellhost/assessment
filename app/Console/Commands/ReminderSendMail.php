<?php

namespace App\Console\Commands;

use App\Http\Controllers\SentMailController;
use Illuminate\Console\Command;

class ReminderSendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:sendmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'untuk mengirim email reminder H-7 tanggal akhir periode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sendmail = new SentMailController;
        $this->info($sendmail->ReminderSendMail());
        //DB::statement('update tpm_tikets set status_id = 6 where due_date < now()::date AND ( status_id = 1 OR status_id = 2 OR status_id = 5)');
    }
}
