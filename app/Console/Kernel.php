<?php

namespace App\Console;

use App\Console\Commands\QueueSendMail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     * 
     */
    protected $commands = [
        QueueSendMail::class
    ];
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('queue:sendmail')->everyMinute()->withoutOverlapping()->appendOutputTo(storage_path('logs/schedule.log'));//->runInBackground();
        $schedule->command('reminder:sendmail')->dailyAt('7:00')->withoutOverlapping()->appendOutputTo(storage_path('logs/schedule.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
