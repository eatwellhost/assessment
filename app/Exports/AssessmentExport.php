<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;

use DB;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class AssessmentExport implements FromView, WithTitle, WithColumnWidths, ShouldAutoSize, WithEvents,WithStyles, WithDrawings
{
    public function __construct($pegawai,$joindate, $praassessment,$assessment, $dataCollection,$induks,$result,$standar,$poin,$summary){
        $this->pegawai = $pegawai;
        $this->joindate= $joindate;
        $this->praassessment = $praassessment;
        $this->assessment = $assessment;
        $this->datas = $dataCollection;
        $this->induks = $induks;
        $this->result = $result;
        $this->standar = $standar;
        $this->poin = $poin;
        $this->summary = $summary;
    }


    public function view(): View
    {  
        return view('laporan.assessment.cetakassessment', [
            'pegawai' => $this->pegawai,
            'joindate' => $this->joindate, 
            'praassessment' => $this->praassessment,
            'assessment' => $this->assessment,
            'datas' => $this->datas,
            'induks'=> $this->induks,
            'result' => $this->result,
            'standar' => $this->standar,
            'poin' => $this->poin,
            'summary' =>  $this->summary
        ]);
    }

    public function title(): string
    {
        return 'Hasil Assessment';
    }

    public function columnWidths(): array
    {
        // Set column widths for specific columns
        return [
            'A' => 4, // Set width of column A to 15
            'B' => 2, // Set width of column B to 20
            'C' => 9, 
            'D' => 9, 
            'E' => 9, 
            'F' => 15, 
            'G' => 9,

            'H' => 25,
            'I' => 13, 

            'J' => 19, 
            'K' => 19, 
            'L' => 19,
       ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is the logo');
        $drawing->setPath(public_path('assets/media/logos/logo.png')); // Path to your image
        $drawing->setHeight(80); // Fixed height for the image
        $drawing->setWidth(150);
        $drawing->setCoordinates('B1'); // Position the image on the sheet
        $drawing->setOffsetX(10); // Adjust horizontal offset
        $drawing->setOffsetY(10); // Adjust vertical offset

        return $drawing;
    }


    public function styles(Worksheet $sheet)
    {
        $highestRow = $sheet->getHighestRow();
        return [
            // Setting the font size for a range of cells (e.g., A1 to C10)
            "A1:L{$highestRow}" => [
                'font' => [
                    'size' => 12, // Font size
                    'name' => 'Arial', // Font type
                ],
            ],
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();
                $alphabet       = $event->sheet->getHighestDataColumn();
                $totalRow       = $event->sheet->getHighestDataRow();
                $cellRange      = 'A1:'.$alphabet.$totalRow;
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setWrapText(true);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);

                for ($row = 1; $row <= $highestRow; $row++) {
                    $cellValue = $event->sheet->getCell("A{$row}")->getValue();
                    if ($cellValue === 'split') {
                        $event->sheet->setBreak('A' . ($row + 1), \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::BREAK_ROW);
                    }
                }
                // foreach (range(1, $sheet->getHighestRow()) as $row) {
                //     $sheet->getRowDimension($row)->setRowHeight(-1);
                // }

                // foreach (range('B', $sheet->getHighestColumn()) as $column) {
                //     $sheet->getColumnDimension($column)->setAutoSize(true);
                // }
                
                // for ($row = 1; $row <= $highestRow; $row++) {
                //     for ($col = 'A'; $col <= $highestColumn; $col++) {
                //         $cellValue = $event->sheet->getCell("{$col}{$row}")->getValue();
                        
                //         // If the cell contains the value "split", adjust the print area
                //         if ($cellValue === 'Karyawan/ti') {
                //             $event->sheet->getDelegate()->getPageSetup()->setPrintArea("A1:{$highestColumn}{$row}");
                            
                //             break 2; // Break both loops
                //         }
                //     }
                // }

                $event->sheet->getDelegate()->getPageMargins()->setTop(0.75); // Set top margin in inches
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.5); // Set right margin in inches
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0.75); // Set bottom margin in inches
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.5);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
            },
        ];
    }
}