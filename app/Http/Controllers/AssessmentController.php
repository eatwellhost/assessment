<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Models\Assessment;
use App\Models\AssessmentDetail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use App\Models\Menu;
use App\Models\Pegawai;
use App\Models\Setting\Jenis;
use App\Models\Setting\Level;
use App\Models\Setting\LevelAssessment;
use App\Models\Setting\Periode;
use App\Models\Setting\PoinPenilaian;
use App\Models\Setting\Point;
use App\Models\Setting\PraAssessment;
use App\Models\Setting\SesiPenilai;
use Carbon\Carbon;
use DB;
use Svg\Tag\Rect;

class AssessmentController extends Controller
{
    protected $__route;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $view = strtolower(str_replace(' ',' ',Menu::where('route_name','assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ',' ',Menu::where('route_name','assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ',' ',Menu::where('route_name','assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ',' ',Menu::where('route_name','assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));
        $this->__route = 'assessment';
        $this->pagetitle = 'Assessment Pegawai';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        return view($this->__route.'.index',[
            'pagetitle' => 'Assessment Pegawai',
            'breadcrumb' => $this->pagetitle,
            'pegawai' => Pegawai::get(),
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
        ]);
    }

    public function inputassessment(Request $request,$id)
    {
        $now = Carbon::now()->format('Y-m-d');

        $data1 = DB::table('new_view_pra_assessment')->orWhere(function ($query) {
            $query->orwhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', pegawai_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', atasan_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', rekan_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', tim_id)");
        })->where('id', $id)->where('tgl_mulai','<=', $now)->where('tgl_akhir','>=',$now);

        $data2 = DB::table('view_pra_assessment')->union($data1);
        $masterData = $data2->orWhere(function ($query) {
            $query->orwhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', pegawai_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', atasan_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', rekan_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', tim_id)");
        })->where('id', $id)->where('tgl_mulai','<=', $now)->where('tgl_akhir','>=',$now)->first();
        if(!$masterData){
            return view($this->__route.'.inputassessment',['pagetitle' => 'Assessment Pegawai',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
            'can' => false
            ]);
        }
        
        $atasan_id = explode(',', $masterData->atasan_id);
        $rekan_id = explode(',', $masterData->rekan_id);
        $tim_id = explode(',', $masterData->tim_id);

        if(Auth::user()->pegawai_id == $masterData->pegawai_id){
            $sesi = SesiPenilai::where('id', 1)->get();
        }elseif(in_array(Auth::user()->pegawai_id,$atasan_id)){
            $sesi = SesiPenilai::where('id', 2)->get();
        }elseif(in_array(Auth::user()->pegawai_id,$rekan_id)){
            $sesi = SesiPenilai::where('id', 3)->get();
        }elseif(in_array(Auth::user()->pegawai_id, $tim_id)){
            $sesi = SesiPenilai::where('id', 4)->get();
        }else{
            $sesi = null;
        }

        $pegawai = DB::table('view_detail_pegawai')->select('*')->where('id',$masterData->pegawai_id)->first();
        $point = Point::whereRaw('FIND_IN_SET(?, points.level)', [$masterData->level_id])->get();

        $point_penilaian = PoinPenilaian::orderby('id')->get();
        $dataCollection = new Collection;  
            foreach ($point->where('parent_id',0) as $key => $induk) {
                $dataCollection->push($induk);
                $prevInduk = null;
                foreach ($point->where('parent_id',$induk->id) as $key => $sub1) {
                    if ($prevInduk === $sub1->id) {
                        continue;
                    }
                    $dataCollection->push($sub1);
                    $prevSub1 = null;
                    
                    foreach ($point->where('parent_id',$sub1->id) as $key => $sub2) {
                        if ($prevSub1 === $sub2->id) {
                            continue;
                        }
                        $dataCollection->push($sub2);
                        $prevSub2 = null;
                        foreach ($point->where('parent_id',$sub2->id) as $key => $sub3) {
                            if ($prevSub2 === $sub3->id) {
                                continue;
                            }
                            $dataCollection->push($sub3);
                            $prevSub3 = null;
                            foreach ($point->where('parent_id',$sub3->id) as $key => $sub4) {
                                if ($prevSub3 === $sub4->id) {
                                    continue;
                                }
                                $prevSub3 = $sub4->id;
                            }
                            $prevSub2 = $sub3->id;
                        }
                        $prevSub1 = $sub2->id;
                    }
                    $prevInduk = $sub1->id;
                }
            }

            $result_assessment = isset(session('form_data')['jawaban']) ? count(session('form_data')['jawaban']) : 0;//AssessmentDetail::where('assessment_id', $id)->whereNotNull('jawaban')->whereNotNull('detail')->count();
            $saran  = session('form_data') ? session('form_data')['saran'] : null;
            $point = session('form_data') ? session('form_data')['point'] : null;
            $result_data = $dataCollection->where('parent_id','!=',0)->count();
            $formData = session('form_data');
            $sessions = false;

            if($result_assessment != $result_data){
                $sessions = true;
            }

            
        return view($this->__route.'.inputassessment',[
            'pagetitle' => 'Assessment Pegawai',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
            'can' => true,
            'data' => $masterData,
            'pegawai' => $pegawai,
            'point' =>$dataCollection,
            'sesi' => $sesi,
            'point_penilaian' => $point_penilaian,
            'data_old' => $formData,
            'sessions' => $sessions,
            'actionform'=> 'insert'
        ]);
    }

    public function datatable(Request $request)
    {
        try{
            $now = Carbon::now()->format('Y-m-d');

            $data1 = DB::table('new_view_pra_assessment');
            $data2 = DB::table('view_pra_assessment')->union($data1);

            $masterData = DB::table(DB::raw("({$data2->toSql()}) as master"));
            $masterData->select('master.*','assessments.praassessment_id','assessments.is_submit')
            ->leftJoin('assessments', function ($join) {
                $join->on('master.id', '=', 'assessments.praassessment_id')
                     ->where('assessments.penilai_id', '=', Auth::user()->pegawai_id);
            })
            ->orWhere(function ($query) {
                $query->orwhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', master.pegawai_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', master.atasan_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', master.rekan_id)")
                ->orWhereRaw("FIND_IN_SET('".Auth::user()->pegawai_id."', master.tim_id)");
               
            })
            ->whereNull('praassessment_id')
            ->where('tgl_mulai','<=', $now)->where('tgl_akhir','>=',$now);

            return datatables()->of($masterData)
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('nik'))) {
                    $query->where('nik', 'like', "%{$request->input('nik')}%");
                }
                if (!empty($request->input('nama'))) {
                    $query->where('nama','like', "%{$request->input('nama')}%");
                }
            })
            ->editColumn('nama', function($row){
                return $row->nik . ' - ' . $row->nama;
            })
            ->editColumn('periode', function($row){
                return $row->periode.'<br/>'.GeneralController::tglFormat($row->tgl_mulai,1).' s/d '.GeneralController::tglFormat($row->tgl_akhir,1);
            })
            ->addColumn('action', function ($row){
                $id = (int)$row->id;
                $button = '<div align="center">';
                $button .= '<a href="'.route('assessment.inputassessment',['id' => $row->id]).'" class="btn btn-info w-30px h-30px btn-sm btn-icon" data-id="'.$id.'" data-toggle="tooltip" title="Assessment '.$row->nama.'">
                <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                <span class="svg-icon svg-icon-info svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Design/Magic.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M1,12 L1,14 L6,14 L6,12 L1,12 Z M0,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,15 C21,15.5522847 20.5522847,16 20,16 L0,16 C-0.55228475,16 -1,15.5522847 -1,15 L-1,11 C-1,10.4477153 -0.55228475,10 0,10 Z" fill="#000000" fill-rule="nonzero" transform="translate(10.000000, 13.000000) rotate(-225.000000) translate(-10.000000, -13.000000) "/>
                        <path d="M17.5,12 L18.5,12 C18.7761424,12 19,12.2238576 19,12.5 L19,13.5 C19,13.7761424 18.7761424,14 18.5,14 L17.5,14 C17.2238576,14 17,13.7761424 17,13.5 L17,12.5 C17,12.2238576 17.2238576,12 17.5,12 Z M20.5,9 L21.5,9 C21.7761424,9 22,9.22385763 22,9.5 L22,10.5 C22,10.7761424 21.7761424,11 21.5,11 L20.5,11 C20.2238576,11 20,10.7761424 20,10.5 L20,9.5 C20,9.22385763 20.2238576,9 20.5,9 Z M21.5,13 L22.5,13 C22.7761424,13 23,13.2238576 23,13.5 L23,14.5 C23,14.7761424 22.7761424,15 22.5,15 L21.5,15 C21.2238576,15 21,14.7761424 21,14.5 L21,13.5 C21,13.2238576 21.2238576,13 21.5,13 Z" fill="#000000" opacity="0.3"/>
                    </g>
                </svg><!--end::Svg Icon--></span>
                </span>
                <!--end::Svg Icon-->
                </a>';

                // $button .= '<button type="button" class="btn btn-light-warning w-30px h-30px btn-sm btn-icon cls-button-edit" data-id="'.$id.'" data-toggle="tooltip" title="Ubah data '.$row->nama.'">
                // <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                // <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                // <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"/>
                // <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"/>
                // <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"/>
                // </svg>
                // </span>
                // <!--end::Svg Icon-->
                // </button>';

                // $button .= '&nbsp;';

                // $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm btn-icon cls-button-delete" data-id="'.$id.'" data-nama="'.$row->nama.'" data-toggle="tooltip" title="Hapus data '.$row->nama.'">
                // <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                // <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                // <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"/>
                // <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="currentColor"/>
                // <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="currentColor"/>
                // </svg>
                // </span>
                // <!--end::Svg Icon-->
                // </button>';

                $button .= '</div>';
                return $button;
            })
            ->rawColumns(['action','periode'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {
        return view($this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
            'actionform' => 'insert',
            'pegawai' => Pegawai::get(),
            'levelAssessment' => LevelAssessment::get(),
            'periode' => Periode::get(),
            'jenisAssesment' => Jenis::get(),
        ]);
    }

    public function store(Request $request)
    {

        // if($request->session){
        //     session(['form_data' => $request->all()]);
        //     $result = [
        //         'flag' => 'session',
        //     ];
        // }else{
            $result = [
                'flag' => 'error',
                'msg' => 'Error System',
                'title' => 'Error'
            ];
            $validator = $this->validateform($request);   
    
            if (!$validator->fails()) {
    
                switch ($request->input('actionform')) {
                    case 'insert': DB::beginTransaction();
                        try{   
                            $assessment = Assessment::where('pegawai_id',$request->pegawai_id)->where('penilai_id', Auth()->user()->pegawai_id)->where('sesi_id', $request->sesi_id)->where('praassessment_id',$request->data_id)->where('is_submit','1')->first();
                                if($assessment == null){
                                    $paramassessment['pegawai_id'] = $request->pegawai_id;
                                    $paramassessment['penilai_id'] = Auth()->user()->pegawai_id;
                                    $paramassessment['sesi_id'] = $request->sesi_id;
                                    $paramassessment['praassessment_id'] = $request->data_id;
                                    $paramassessment['periode_id'] = $request->periode_id;
                                    $paramassessment['level_id'] = $request->level_id;
                                    $paramassessment['point'] = str_replace('&', 'dan',$request->point);
                                    $paramassessment['saran'] = str_replace('&', 'dan',$request->saran);
                                    $paramassessment['is_submit'] = 1;
                                    
                                    $assessment = Assessment::create($paramassessment);
                                }
                            if($request->jawaban){
                                foreach($request->jawaban as $key => $val){
                                    AssessmentDetail::updateorInsert([
                                        'assessment_id' => $assessment->id,
                                        'question_id' => $request->question_id[$key],
                                    ],[
                                        'assessment_id' => $assessment->id,
                                        'question_id' => $request->question_id[$key],
                                        'jawaban'   => $request->jawaban[$key],
                                        // 'detail'   => $request->catatan[$key] != null || $request->catatan[$key] != '' ? str_replace('&', 'dan',$request->catatan[$key]) : '-',
                                    ]);
                                }                               
                            }
                            DB::commit();
                            // session()->forget('form_data');
                            $result = [
                            'flag'  => 'success',
                            'msg' => 'Sukses tambah data',
                            'title' => 'Sukses'
                            ];
                        }catch(\Exception $e){
                            DB::rollback();
                            $result = [
                            'flag'  => 'warning',
                            'msg' => $e->getMessage(),
                            'title' => 'Gagal'
                            ];
                        }
                        // finally {
                        //     // Ensure session is always cleared
                        //     session()->forget('form_data');
                        // }
                        break;
                        
                    case 'update': DB::beginTransaction();
                        try{
                            $PraAssessment = PraAssessment::find((int)$request->input('id'));
                            
                            $PraAssessment->update((array)$param);
    
                            DB::commit();
                            $result = [
                            'flag'  => 'success',
                            'msg' => 'Sukses ubah data',
                            'title' => 'Sukses'
                            ];
                        }catch(\Exception $e){
                            DB::rollback();
                            $result = [
                            'flag'  => 'warning',
                            'msg' => $e->getMessage(),
                            'title' => 'Gagal'
                            ];
                        }
    
                        // finally {
                        //     // Ensure session is always cleared
                        //     session()->forget('form_data');
                        // }
                        break;
                }
            }else{
                $messages = $validator->errors()->all('<li>:message</li>');
                $result = [
                    'flag'  => 'warning',
                    'msg' => '<ul>'.implode('', $messages).'</ul>',
                    'title' => 'Gagal proses data'
                ];                      
            }
        // }
        
        return response()->json($result);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {

        try{
            $data = PraAssessment::find((int)$request->input('id'));
            return view($this->__route.'.form',[
                'actionform' => 'update',
                'pagetitle' => $this->pagetitle,
                'data' => $data,
                'pegawai' => Pegawai::get(),
                'jenisassessment' => Jenis::get(),
                'periode' => Periode::get()

            ]);
        }catch(Exception $e){}

    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try{
            $data = PraAssessment::find((int)$request->input('id'));
            $data->delete();

            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal hapus data',
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);       
    }

    protected function validateform($request)
    {
        $required['jawaban'] = 'required';
        // $required['catatan'] = 'required';
        $required['point'] = 'required';
        $required['saran'] = 'required';
        //$required['guard_name'] = 'required';

        $message['jawaban.required'] = 'Jawaban Wajib Di Pilih';
        // $message['catatan.required'] = 'Catatan perjawaban wajib di input';
        $message['point.required'] = 'Point-point wajib di input';
        $message['saran.required'] = 'Saran Wajib Di input';
        
        //s$message['guard_name.required'] = 'guard name wajib dipilih';

        return Validator::make($request->all(), $required, $message);       
    }


}