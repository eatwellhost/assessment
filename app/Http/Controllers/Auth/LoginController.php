<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use DB;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $is_active_user = User::where('username',strtolower($request->input('username')))->first();
        try {
            if(!$is_active_user){
                $result = [
                    'flag'  => 'error',
                    'msg' => '',
                    'title' => 'Username belum terdaftar'
                ];            
            }
            if($is_active_user){
                $data = [
                    'username' => strtolower($request->input('username')),
                    'password' => md5($request->input('password')),
                ]; 
                $data = request(['username', 'password']);
                if (Auth::guard('web')->Attempt($data,true)) {
                    $result = [
                        'flag'  => 'success',
                        'msg' => 'Berhasil Login',
                        'title' => 'Login sukses'
                    ];
                }else{
                    $result = [
                        'flag'  => 'error',
                        'msg' => 'Periksa kembali password anda',
                        'title' => 'Password Salah'
                    ];
                }   
            }
        } catch (\Throwable $th) {
            $result = [
                'flag'  => 'warning',
                'msg' => (config('app.env') === 'production') ? 'Error' : $th->getMessage(),
                'title' => 'Gagal'
              ];
        }
        

            return response()->json($result);
    }

    public function logout() {
        Auth::guard('web')->logout();
        return redirect()->route('login');
    }

    public function change(Request $request){
        DB::beginTransaction();
        try {     
            $validator = Validator::make($request->all(),[
                'old_password' => ['required'],
                'password' => ['required', 'same:password_confirmation', Password::min(8)->mixedCase()->numbers()->symbols()],
                'password_confirmation' => 'required ',
            ],
            [
                'required' => 'Tidak boleh kosong!',
                'password.min' => 'Password paling sedikit 8 karakter.',
                'password.mixed_case' => 'Password harus mengandung huruf kapital dan huruf kecil.',
                'password.numbers' => 'Password minimal mengandung 1 angka.',
                'password.symbols' => 'Password minimal mengandung 1 karakter khusus.',
                'g-recaptcha-response.required' => 'Lengkapi captcha!'
            ]);

            if ($validator->fails()) {
                $messages = $validator->messages()->all();            
                foreach ($messages as $message) {  
                return response()->json(['flag' => 'error', 'msg' => "'" . $message . "'", 'title' => 'Error']); 
                }
            }

            $users = User::where('id', $request->user_id)->first();
            $status = Hash::check($request->old_password, $users->password);
            if($status){
                $param['password'] = Hash::make($request->password);
                $param['old_password'] = $request->password;
                $param['random_password'] = 'change';
                $param['password_changed_at'] = now();
                $param['email_verified_at'] = now();

                $users->update($param);
                DB::commit();
                $result = [
                    'flag'  => 'success',
                    'msg' => 'Password Berhasil Diubah',
                    'title' => 'Success',
                ];
            }else{
                DB::rollback();
                $result = [
                    'flag'  => 'warning',
                    'msg' => 'Password Lama Salah',
                    'title' => 'Warning',
                ];

            }
            return response()->json($result);

        } catch (\Throwable $th) {
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => (config('app.env') === 'production') ? 'Error' : $th->getMessage(),
                'title' => 'Gagal'
            ];
            return response()->json($result);
        }
    }
}
