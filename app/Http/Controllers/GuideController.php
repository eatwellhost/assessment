<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Config;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;
use Datatables;

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Role as ModelsRole;
use App\Models\Guide;
use App\Models\Menu;


class GuideController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','guide.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ','_',Menu::where('route_name','guide.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ','_',Menu::where('route_name','guide.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ','_',Menu::where('route_name','guide.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));
        $this->__route = 'guide';
        $this->pagetitle = 'User Guide';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view($this->__route.'.index',[
            'pagetitle' => $this->pagetitle,
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
        ]);
    }

    
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        try{
            $id = Auth()->user()->roles->pluck('id')->first();
            if(Auth()->user()->hasRole('Super Admin')){
                $data = Guide::get();
            }else{
                $data = Guide::where('role_id','like',"%{$id}%")->get();
            }
            return datatables()->of($data)
            ->editColumn('name_file', function($row){
                $button = '<div align="center">';
                if($row->name_file){

                $button .= '<a href="'.asset('storage/guide/' .$row->name_file).'" target="_blank" class="btn btn-light-info w-30px h-30px btn-sm btn-icon" data-toggle="tooltip" title="Lihat User Manual">
                <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Files/DownloadedFile.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"/>
                        <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path d="M14.8875071,11.8306874 L12.9310336,11.8306874 L12.9310336,9.82301606 C12.9310336,9.54687369 12.707176,9.32301606 12.4310336,9.32301606 L11.4077349,9.32301606 C11.1315925,9.32301606 10.9077349,9.54687369 10.9077349,9.82301606 L10.9077349,11.8306874 L8.9512614,11.8306874 C8.67511903,11.8306874 8.4512614,12.054545 8.4512614,12.3306874 C8.4512614,12.448999 8.49321518,12.5634776 8.56966458,12.6537723 L11.5377874,16.1594334 C11.7162223,16.3701835 12.0317191,16.3963802 12.2424692,16.2179453 C12.2635563,16.2000915 12.2831273,16.1805206 12.3009811,16.1594334 L15.2691039,12.6537723 C15.4475388,12.4430222 15.4213421,12.1275254 15.210592,11.9490905 C15.1202973,11.8726411 15.0058187,11.8306874 14.8875071,11.8306874 Z" fill="#000000"/>
                    </g>
                </svg><!--end::Svg Icon--></span>
                </a>';
                }
                
                $button .= '</div>';
                return $button;
            })
            ->addColumn('role', function($row){
                $role_id = explode(',',$row->role_id);
                $button = '<div>';
                foreach($role_id as $key => $val){
                    $role = Role::where('id',$val)->pluck('name')->first();
                    $button .= '<span class="badge badge-success">'.$role.'</span>';
                    $button .= '&nbsp;';
                }
                $button .= '</div>';
                return $button;
            })
            ->addColumn('action', function ($row){
                $id = (int)$row->id;
                $button = '<div align="center">';
                if(Auth()->user()->hasRole('Super Admin')){
                $button .= '<button type="button" class="btn btn-light-warning w-30px h-30px btn-sm btn-icon cls-button-edit" data-id="'.$id.'" data-toggle="tooltip" title="Ubah data '.$row->name.'">
                <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"/>
                <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"/>
                <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"/>
                </svg>
                </span>
                <!--end::Svg Icon-->
                </button>';

                $button .= '&nbsp;';

                $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm  btn-icon cls-button-delete" data-id="'.$id.'" data-nama="'.$row->name.'" data-toggle="tooltip" title="Hapus data '.$row->name.'">
                <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"/>
                <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="currentColor"/>
                <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="currentColor"/>
                </svg>
                </span>
                <!--end::Svg Icon-->
                </button>';
                }
                $button .= '</div>';
                return $button;
            })
            ->rawColumns(['action','role','name_file'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view($this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
            'actionform' => 'insert',
            'role' => Role::get()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $result = [
            'flag' => 'error',
            'msg' => 'Error System',
            'title' => 'Error'
        ];
        $validator = $this->validateform($request);
        if (!$validator->fails()) {
            $param = $request->except('actionform','id','roles','upload');
            switch ($request->input('actionform')) {
                case 'insert': DB::beginTransaction();
                    try{
                        if($request->roles){
                            $param['role_id'] = implode(',',$request->input('roles'));
                            if ($request->hasFile('upload')) {
                                $fileType  = $request->file('upload')->getClientOriginalExtension();
                                $fileName   = 'User_Manual_'.$request->roles[0].'_'.date('d-m-Y').'.'.$fileType;

                                Storage::disk('public')->put('guide/' . $fileName, File::get($request->file('upload')));
                                $filePath = storage_path('app/public/guide/' .$fileName);
                            
                                $param['name_file'] = $fileName;
                                $param['path_file'] = $filePath;
                                $param['type_file'] = $fileType;
                            }
                            Guide::create($param);
                        }

                        DB::commit();
                        $result = [
                            'flag'  => 'success',
                            'msg' => 'Sukses tambah data',
                            'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;

                case 'update': DB::beginTransaction();
                    try{
                        $guide = Guide::find((int)$request->input('id'));
                        $param['role_id'] = implode(',',$request->input('roles'));

                        if ($request->hasFile('upload')) {
                            #pdf file
                            if (!empty($guide->name_file)) {
                                $path = storage_path('app/public/guide/'.$guide->name_file);
                                if (File::exists($path)) {
                                    Storage::delete('public/guide/'.$guide->name_file);
                                }
                            }
                            $fileType  = $request->file('upload')->getClientOriginalExtension();
                            $fileName   = 'User_Manual_'.$request->roles[0].'_'.date('d-m-Y').'.'.$fileType;
                            Storage::disk('public')->put('guide/' . $fileName, File::get($request->file('upload')));
                            $filePath = storage_path('app/public/guide/' .$fileName);
                        
                            $param['name_file'] = $fileName;
                            $param['path_file'] = $filePath;
                            $param['type_file'] = $fileType;
                        }
                        $guide->update($param);


                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses ubah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;
            }
        }else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'warning',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];
        }

        return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {

        try{

            $guide = Guide::find((int)$request->input('id'));
            $data = ([
            'pagetitle' => $this->pagetitle,
            'actionform' => 'update',
            'data' => $guide,
            'role' => Role::get(),
            'hisrole' => explode(',',$guide->role_id),
            ]);
                return view($this->__route.'.form',$data);
        }catch(Exception $e){}

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        DB::beginTransaction();
        try{
            $guide = Guide::find((int)$request->input('id'));
            if($guide->name_file){
                $path = storage_path('app/public/guide/'.$guide->name_file);
                if (File::exists($path)) {
                    Storage::delete('public/guide/'.$guide->name_file);
                }
            }
            $guide->delete();
            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => $e->getMessage(),
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateform($request)
    {
        $required['upload'] = 'required';
        $required['roles'] = 'required';

        $message['upload.required'] = 'file wajib diinput';
        $message['roles.required'] = 'role wajib dipilih';

        return Validator::make($request->all(), $required, $message);
    }

}
