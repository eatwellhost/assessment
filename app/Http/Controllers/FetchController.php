<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Master\Brand;
use App\Models\Master\Outlet;
use App\Models\Master\Checklist\JenisAudit;
use App\Models\Master\Maintenance\Teknisi;
use App\Models\Master\Wilayah\Provinsi;
use App\Models\Master\Wilayah\Kota;
use App\Models\Master\Wilayah\Area;
use App\Models\Master\Wilayah\Negara;
use App\Models\Setting\Department;
use App\Models\Setting\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
class FetchController extends Controller
{
    public function getprovinsi(Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Provinsi::getprovinsi($search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->nama
			        	];
			        })
			        ->toArray();    

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Provinsi - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

    public function getkota($id_provinsi = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Kota::getkota($id_provinsi,$search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->nama
			        	];
			        })
			        ->toArray();    

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Kota - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

	public function getarea($brand_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Area::getarea($brand_id,$search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->nama
			        	];
			        })
			        ->toArray();    

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Area - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

	public function getam($brand_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = User::whereHas("roles", function($q){ $q->whereIn("name", ['Area Manager']); })
					->where('brand_id',$brand_id)
					->where('name','like','%'.$search.'%')
					->get()->take(5)->map(function($item, $key){
								return [
									'id' => $item->id,
									'text' => $item->name
								];
							})->toArray();    
			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Area - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

	public function getDept($division_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Department::where('division_id',$division_id)
					->where('nama','like','%'.$search.'%')
					->get()->take(5)->map(function($item, $key){
								return [
									'id' => $item->id,
									'text' => $item->nama
								];
							})->toArray();    
			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Department - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}
	public function getSect($department_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Section::where('department_id',$department_id)
					->where('nama','like','%'.$search.'%')
					->get()->take(5)->map(function($item, $key){
								return [
									'id' => $item->id,
									'text' => $item->nama
								];
							})->toArray();    
			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Section - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

	public function getteknisi($brand_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Teknisi::where('brand_id','like','%'.$brand_id.'%')
					->where('nama','like','%'.$search.'%')
					->where('status','active')
					->get()->take(5)->map(function($item, $key){
								return [
									'id' => $item->id,
									'text' => $item->nama
								];
							})->toArray();    
			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Teknisi - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

	public function getBrand(Request $request){
        $result = Brand::select('*')->orderby('id','asc')->get();
        $data = [];
        for ($i = 0; $i < count($result); $i++) { 
            $data[$i]=[ 'id'=> $result[$i]->id,
                        'text' => $result[$i]->name
                ];
        }
        return response()->json(['item' => $data]);
    }

	public function getJenisAudit(Request $request){
        $result = JenisAudit::select('*')->orderby('id','asc')->get();
        $data = [];
        for ($i = 0; $i < count($result); $i++) { 
            $data[$i]=[ 'id'=> $result[$i]->id,
                        'text' => $result[$i]->name
                ];
        }
        return response()->json(['item' => $data]);
    }

	public function getOutletByArea($area_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Outlet::getoutletbyarea($area_id,$search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->nama
			        	];
			        })
			        ->toArray();    

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Outlet - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}

	public function getOutletByBrand($brand_id = null,Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = Outlet::getoutletbybrand($brand_id,$search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->nama
			        	];
			        })
			        ->toArray();    

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Outlet - ]'
            ];
            return response()->json(compact('item'));			
		}		
	}
    


}
