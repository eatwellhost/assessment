<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GeneralModel;
use App\Models\KategoriUser;
use App\Models\Master\Brand;
use App\Models\Master\Outlet;
use Carbon\Carbon;
use Exception;

class GeneralController extends Controller
{
	protected $gm;

	public function __construct()
	{
		$this->gm = new GeneralModel();
	}

	public function fetchparentmenu(Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = $this->gm->getparentmenu($search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->label
			        	];
			        })
			        ->toArray();

            array_unshift($item, ['id' => 0, 'text' => 'Sebagai Root Menu']);        

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Parent Menu - ]'
            ];
            return response()->json(compact('item'));			
		}			
	}

    public function fetchparentquestion(Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = $this->gm->getparentquestion($search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->question
			        	];
			        })
			        ->toArray();

            array_unshift($item, ['id' => 0, 'text' => 'Sebagai Root Pertanyaan']);        

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Parent Pertanyaan - ]'
            ];
            return response()->json(compact('item'));			
		}			
	}

	public function fetchparentmenuuser(Request $request)
	{
		try{
			$search = $request->has('q')? $request->input('q') : null;

			$item = $this->gm->getparentmenuuser($search)
			        ->map(function($item, $key){
			        	return [
			        		'id' => $item->id,
			        		'text' => $item->label
			        	];
			        })
			        ->toArray();

            array_unshift($item, ['id' => 0, 'text' => 'Sebagai Root Menu']);        

			return response()->json(compact('item'));			        
		}catch(Exception $e){
            $item[] = [
                'id' => 0,
                'text' => '[ - Pilih Parent Menu - ]'
            ];
            return response()->json(compact('item'));			
		}			
	}

	/**
     * Format tanggal DB ke Indonesia
     * DB : 2017-12-31
     * @return format 1 = 31 Desember 2017
     *                2 = Minggu, 31 Desember 2017
     *                3 = 31-12-2017
     */
	public static function tglFormat($tgl, $format = 1)
    {
        $dt = @date_parse($tgl);
        if (!$dt['year'] || !$dt['month'] || !$dt['day']) return $tgl;

        if ($format == 1) {

            # 31 Desember 2017
            $hari   = $dt['day'];
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];

            return $hari . ' ' . self::bulan($bulan) . ' ' . $tahun;
        } else if ($format == 2) {

            # Minggu, 31 Desember 2017
            $hari   = $dt['day'];
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];
            $nama_hari  = Carbon::parse($tgl)->dayOfWeek;
            return self::hari($nama_hari) . ', ' . $hari . ' ' . self::bulan($bulan) . ' ' . $tahun;
        } else if ($format == 3) {

            # 31-12-2017
            $hari       = date('d', strtotime($tgl));
            $bulan      = date('m', strtotime($tgl));
            $tahun      = date('Y', strtotime($tgl));

            return $hari . '-' . $bulan . '-' . $tahun;
        } else if ($format == 4) {

            # 31 Des 2017
            $hari   = $dt['day'];
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];

            return $hari . ' ' . substr(self::bulan($bulan), 0, 3) . ' ' . $tahun;
        } else if ($format == 5) {

            # 01 Des 2017
            $hari   = 01;
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];

            return $hari . ' ' . self::bulan($bulan) . ' ' . $tahun;
        } else if ($format == 7) {

            # 01 Des 2017
            $hari   = 01;
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];

            return self::bulan($bulan) . ' ' . $tahun;
        }
        if ($format == 6) {

            # 31 Desember 2017
            $hari   = $dt['day'];
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];

            return $hari . ' ' . self::bulanSingkat($bulan) . ' ' . $tahun;
        }
        if($format == 8){
            $hari   = $dt['day'];
            $bulan  = $dt['month'];
            $tahun  = $dt['year'];

            return $hari . ' ' . self::bulan($bulan);
        }
    }

    public static function hari($hari)
    {
        $aHari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

        if ($hari == 0) {
            $hari = 7;
        }

        if (@$aHari[$hari - 1]) {
            return $aHari[$hari - 1];
        }

        return false;
    }


	public static function bulan($bulan)
    {
        $aBulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        //kalau $bulan lebih dari 12 return error
        if ($bulan > count($aBulan)) {
            return 'error';
        }
        return $aBulan[$bulan - 1];
    }

    public static function bulanroma($bulan)
    {
        $aBulan = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII'];

        //kalau $bulan lebih dari 12 return error
        if ($bulan > count($aBulan)) {
            return 'error';
        }
        return $aBulan[$bulan - 1];
    }

	public static function bulanSingkat($bulan)
    {
        $aBulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

        return $aBulan[$bulan - 1];
    }

    

}
