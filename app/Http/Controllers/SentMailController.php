<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Mail\NotifikasiAssessment;
use App\Mail\NotifikasiReminderAssessment;
use App\Models\Assessment;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Mail;
use App\Models\Menu;
use App\Models\Pegawai;
use App\Models\Setting\Company;
use App\Models\Setting\Jenis;
use App\Models\Setting\Periode;
use App\Models\Setting\PraAssessment;
use App\Models\Setting\SendMail;
use App\Models\User;
use Carbon\Carbon;
use DB;

class SentMailController extends Controller
{
    protected $__route;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','sent_mail.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ','_',Menu::where('route_name','sent_mail.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ','_',Menu::where('route_name','sent_mail.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ','_',Menu::where('route_name','sent_mail.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));

        $this->__route = 'sent_mail';
        $this->pagetitle = 'Sent Mail';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {
        return view($this->__route.'.index',[
            'pagetitle' => 'Sent Mail',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
        ]);

    }

    public function datatable(Request $request)
    {
        try{
            return datatables()->of(Periode::orderby('id','desc')->get())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('brand'))) {
                    $query->where('name', 'like', "%{$request->input('brand')}%");
                }
            })
            ->addColumn('action', function ($row){
                $button = '<div align="center">';

                $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm btn-icon cls-button-sent" data-id="'.$row->id.'" title="Kirim Email">
                <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Communication/Mail.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"/>
                    </g>
                </svg><!--end::Svg Icon--></span>
                </button>';
                 
                $button .= '</div>';
                return $button;
            })
            ->rawColumns(['action'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {
        return view($this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
            'actionform' => 'insert'
        ]);
    }

    public function store(Request $request)
    {
        $result = [
            'flag' => 'error',
            'msg' => 'Error System',
            'title' => 'Error'
        ];      

        $validator = $this->validateform($request);   

        if (!$validator->fails()) {
            $param['nama'] = $request->input('nama');
            
            switch ($request->input('actionform')) {
                case 'insert': DB::beginTransaction();
                    try{     
                        
                        $brand = Company::create((array)$param);
                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses tambah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;
                
                case 'update': DB::beginTransaction();
                    try{
                        $Company = Company::find((int)$request->input('id'));
                        
                        $Company->update((array)$param);

                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses ubah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;
            }
        }else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'warning',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];                      
        }

        return response()->json($result);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {

        try{
            $Company = Company::find((int)$request->input('id'));
            return view($this->__route.'.form',[
                'actionform' => 'update',
                'pagetitle' => $this->pagetitle,
                'data' => $Company
            ]);
        }catch(Exception $e){}

    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try{
            $data = Company::find((int)$request->input('id'));
            $data->delete();

            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal hapus data',
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);       
    }

    public function SentMail(Request $request){
        try{
            $data1 = DB::table('new_view_pra_assessment')->where('periode_id', $request->id);
            $data2 = DB::table('view_pra_assessment')->union($data1)->where('periode_id', $request->id);
            $masterData = $data2->get();
            
            foreach($masterData as $key => $val){
                $atasan_id = explode(',', $val->atasan_id);
                $rekan_id = explode(',', $val->rekan_id);
                $tim_id = explode(',', $val->tim_id);
                $pegawai = User::Where(function ($query) use($val,$atasan_id,$rekan_id,$tim_id) {
                    $query->orwhereIn('pegawai_id', [$val->pegawai_id])
                        ->orWhereIn('pegawai_id', $atasan_id)
                        ->orWhereIn('pegawai_id', $rekan_id)
                        ->orWhereIn('pegawai_id', $tim_id);
                })->whereNotNUll('email')->get();

                $param['pra_assessment_id'] = $val->id;
                foreach($pegawai as $keys => $value){
                    $param['user_id'] = $value->id;
                    $param['status'] = false;
                    $param['versi'] = 'awal';

                    $exist = SendMail::where('user_id', $value->id)->where('pra_assessment_id', $val->id)->first();
                    if(!$exist){
                        SendMail::create($param);
                    }
                }
            }
            $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses Mengirim Email',
                        'title' => 'Sukses'
                    ];
        }catch(\Exception $e){
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal Mengirim Email',
                'title' => 'Gagal'
            ];
        }
       return response()->json($result);
    }

    public function QueueSendMail(){
        DB::beginTransaction();
        $mail = SendMail::where('status', 0)->limit('4')->get();
        if($mail->count() != 0){
            foreach($mail as $key => $val){
                $PraAssessment = PraAssessment::where('id', $val->pra_assessment_id)->first();
                $data['jenis'] = Jenis::where('id',$PraAssessment->jenis_id)->pluck('nama')->first();
                $data['periode'] = Periode::where('id', $PraAssessment->periode_id)->first();
                $data['url'] = route('assessment.inputassessment',['id' => $PraAssessment->id]);
                $data['user'] = User::where('id', $val->user_id)->first();
                $data['pegawai'] = Pegawai::where('id',$PraAssessment->pegawai_id)->first();
                $data['akhir'] = GeneralController::tglFormat($data['periode']->tgl_akhir,2);

                $to = $data['user']->email;
                if($val->versi == 'awal'){
                    $success = Mail::to($to)->send(new NotifikasiAssessment($data));
                }else{
                    $success = Mail::to($to)->send(new  NotifikasiReminderAssessment($data));
                }
                SendMail::where('user_id', $val->user_id)->where('pra_assessment_id', $val->pra_assessment_id)->update(['status'=>'1']);
            }
            if($success){
                DB::commit();
                return 'sukses kirim email';
            }else{
                DB::rollback();
                return 'Gagal Kirim Email';
            }
        }else{
            DB::rollback();
            return 'tidak ada data';
        }
    }

    public function ReminderSendMail(){
        try{
            DB::beginTransaction();
            $H = Carbon::now()->format('Y-m-d');
            $H1 = Carbon::now()->addDays(1)->format('Y-m-d');
            $H2 = Carbon::now()->addDays(2)->format('Y-m-d');
            $H3 = Carbon::now()->addDays(3)->format('Y-m-d');
            $H4 = Carbon::now()->addDays(4)->format('Y-m-d');
            $H5 = Carbon::now()->addDays(5)->format('Y-m-d');
            $H6 = Carbon::now()->addDays(6)->format('Y-m-d');
            $H7 = Carbon::now()->addDays(7)->format('Y-m-d');
            $periode_id = Periode::whereIn('tgl_akhir', [$H,$H1,$H2,$H3,$H4,$H5,$H6,$H7])->pluck('id','id')->all();
            if($periode_id){ 
                $data1 = DB::table('new_view_pra_assessment')->whereIn('periode_id', $periode_id);
                $data2 = DB::table('view_pra_assessment')->union($data1)->whereIn('periode_id', $periode_id);
                $PraAssessment = $data2->get();
                foreach($PraAssessment as $key => $val){
                    $atasan_id = explode(',', $val->atasan_id);
                    $rekan_id = explode(',', $val->rekan_id);
                    $tim_id = explode(',', $val->tim_id);

                    $param['pra_assessment_id'] = $val->id;
                    $param['versi'] = 'reminder';
                    $pegawai = Assessment::where('praassessment_id', $val->id)->where('penilai_id', $val->pegawai_id)->first();
                    if($pegawai == NULL){
                        $user = User::where('pegawai_id', $val->pegawai_id)->first();
                        $param['user_id'] = $user->id;
                        $param['status'] = false;
                        SendMail::create($param);
                    }
                    foreach($atasan_id as $key => $atasan){
                        if($atasan){
                            $dataAtasan = Assessment::where('praassessment_id', $val->id)->where('penilai_id', $atasan)->first();
                            if($dataAtasan == NULL){
                                $user = User::where('pegawai_id', $atasan)->first();
                                $param['user_id'] = $user->id;
                                $param['status'] = false;
                                SendMail::create($param);
                            }
                        }
                    }

                    foreach($rekan_id as $key => $rekan){
                        if($rekan){
                            $dataRekan = Assessment::where('praassessment_id', $val->id)->where('penilai_id', $rekan)->first();
                            if($dataRekan == NULL){
                                $user = User::where('pegawai_id', $rekan)->first();
                                $param['user_id'] = $user->id;
                                $param['status'] = false;
                                SendMail::create($param);
                            }
                        }
                    }
                    foreach($tim_id as $key => $tim){
                        if($tim){
                            $dataTim = Assessment::where('praassessment_id', $val->id)->where('penilai_id', $tim)->first();
                            if($dataTim == NULL){
                                $user = User::where('pegawai_id', $tim)->first();
                                $param['user_id'] = $user->id;
                                $param['status'] = false;
                                SendMail::create($param);
                            }
                        }
                    }
                }
            }else{
                DB::rollback();
                return 'Gagal Kirim Email,tidak ada periode yang akan berakhir dalam waktu dekat ' .Carbon::now();
            }
            DB::commit();
            return 'sukses kirim email '.Carbon::now();
        }catch(\Exception $e){
            DB::rollback();
            return 'Gagal Kirim Email';
        }
    }

    protected function validateform($request)
    {
        $required['nama'] = 'required';
        //$required['guard_name'] = 'required';

        $message['nama.required'] = 'Nama Periode wajib diinput';
        //s$message['guard_name.required'] = 'guard name wajib dipilih';

        return Validator::make($request->all(), $required, $message);       
    }


}