<?php

namespace App\Http\Controllers;

use App\Models\Assessment;

use App\Models\Setting\Periode;
use App\Models\Setting\PraAssessment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        if (( Auth::user()->password_changed_at == null)) {
            return view('pages.auth.changepassword',['user_id'=>Auth::user()->id]);
         }else{
            $praAssessment='';
            $mulai ='';
            $selesai ='';
            $sudahisi ='';
            $komplit='';
            $nonkomplit ='';
            $komplit = $nonkomplit = 0;
            $param=[];

            $now = Carbon::now()->format('Y-m-d');
            $periode = Periode::where('tgl_mulai','<=', $now)->where('tgl_akhir','>=',$now)->orderby('tgl_mulai','asc')->pluck('id')->all();
            if($periode){
                $data1 = DB::table('new_view_pra_assessment');
                $data2 = DB::table('view_pra_assessment')->union($data1);

                $praAssessment = DB::table(DB::raw("({$data2->toSql()}) as master"));
                $praAssessment->select('master.*','assessments.praassessment_id','assessments.is_submit')
                
                ->leftJoin('assessments', function ($join) {
                    $join->on('master.id', '=', 'assessments.praassessment_id')
                     ->where('assessments.penilai_id', '=', Auth::user()->pegawai_id);
                })
                ->orWhere(function ($query) {
                        $query->orWhere('master.pegawai_id', Auth::user()->pegawai_id)
                            ->orWhereRaw('FIND_IN_SET(?, master.atasan_id)', [Auth::user()->pegawai_id])
                            ->orWhereRaw('FIND_IN_SET(?, master.rekan_id)', [Auth::user()->pegawai_id])
                            ->orWhereRaw('FIND_IN_SET(?, master.tim_id)', [Auth::user()->pegawai_id]);
                    })
                ->whereNull('praassessment_id')
                ->whereIn('master.periode_id', $periode)->orderby('id','desc');
                $praAssessment = $praAssessment->get()->count();
                

                //$mulai = GeneralController::tglFormat($periode->tgl_mulai,2);
                //$selesai = GeneralController::tglFormat($periode->tgl_akhir,2);
                $sudahisi = Assessment::whereIn('periode_id',$periode)->get()->count();

                $master = PraAssessment::whereIn('periode_id', $periode)->get();
                    foreach($master as $key => $vals){
                        $nilai =  collect(DB::select("call procedure_new_pasca_assessment($vals->id)"))->whereNull('penilai_id')->count();
                        if($nilai == '0'){
                            $komplit = $komplit+1;
                        }else{
                            $nonkomplit = $nonkomplit+1;
                        }

                    }
                }
                $periode = Periode::where('tgl_mulai','<=', $now)->where('tgl_akhir','>=',$now)->orderby('tgl_mulai','asc')->get();
                foreach($periode as $key => $val){
                    $param[] = "Periode Pengisian untuk ".$val->nama." adalah ".GeneralController::tglFormat($val->tgl_mulai)." S/d ".GeneralController::tglFormat($val->tgl_akhir);
                }
            // $UserHasOutlet = UserHasOutlet::where('user_id', Auth()->user()->id)->pluck('outlet_id','outlet_id')->all();
            // $UserHasBrand = UserHasOutlet::where('user_id',Auth()->user()->id)->pluck('brand_id','brand_id')->all();
            // $UserHasAm = UserHasAm::where('user_id', Auth()->user()->id)->pluck('am_id','am_id')->all();
            // $Outlets = UserHasOutlet::whereIn('user_id',$UserHasAm)->pluck('outlet_id','outlet_id')->all();
            // $Outlet = Outlet::where('brand_id',Auth()->user()->brand_id)->pluck('id','id')->all();
            // $areas = UserHasArea::where('user_id',Auth()->user()->id)->where('brand_id', Auth()->user()->brand_id)->pluck('area_id','area_id')->all();
            // $canSeeAudit = true;
            // $canSeeChecklist = true;
            // $canSeeMaintenance = false;
            // $totalAudit = '';
            // $totalChecklist = '';
            // $totalAuditActive = '';
            // $totalChecklistActive = '';
            // $totalAuditNonActive = '';
            // $totalChecklistNonActive = '';
            // $totalAuditToNonActive = '';
            // $totalChecklistToNonActive = '';
            // $totalMaintenancePemesanan = '';
            // $totalMaintenancePemesananBaru = '';
            // $totalMaintenancePemesananProses = '';
            // $totalMaintenancePemesananSelesai = '';

            // if(Auth()->user()->hasRole('Super Admin')){
            //     $totalAudit = Checklist::where('jenis_id','4')->get()->count();
            //     $totalChecklist = Checklist::where('jenis_id','!=','4')->get()->count();
            //     $totalAuditActive = Checklist::where('jenis_id','4')->where('is_active','1')->get()->count();
            //     $totalChecklistActive = Checklist::where('jenis_id','!=','4')->where('is_active','1')->get()->count();
            //     $totalAuditNonActive = Checklist::where('jenis_id','4')->where('is_active','0')->get()->count();
            //     $totalChecklistNonActive = Checklist::where('jenis_id','!=','4')->where('is_active','0')->get()->count();
            //     $totalAuditToNonActive = Checklist::where('jenis_id','4')->where('is_active','2')->get()->count();
            //     $totalChecklistToNonActive = Checklist::where('jenis_id','!=','4')->where('is_active','2')->get()->count();
            //     $totalMaintenancePemesanan = MaintenanceItemPemesanan::get()->count();
            //     $totalMaintenancePemesananBaru = MaintenanceItemPemesanan::where('status','pemesanan')->get()->count();
            //     $totalMaintenancePemesananProses = MaintenanceItemPemesanan::where('status','proses')->get()->count();
            //     $totalMaintenancePemesananSelesai = MaintenanceItemPemesanan::where('status','selesai')->get()->count();
            //     $canSeeMaintenance = true;
            // }else if(Auth()->user()->hasRole('Admin Brand') ||Auth()->user()->hasRole('Operational Manager')||Auth()->user()->hasRole('General Manager')||Auth()->user()->hasRole('Brand Manager')){
            //     $totalAudit = Checklist::where('jenis_id','4')->whereIn('outlet_id',$Outlet)->get()->count();
            //     $totalChecklist = Checklist::where('jenis_id','!=','4')->whereIn('outlet_id',$Outlet)->get()->count();
            //     $totalAuditActive = Checklist::where('jenis_id','4')->whereIn('outlet_id',$Outlet)->where('is_active','1')->get()->count();
            //     $totalChecklistActive = Checklist::where('jenis_id','!=','4')->whereIn('outlet_id',$Outlet)->where('is_active','1')->get()->count();
            //     $totalAuditNonActive = Checklist::where('jenis_id','4')->whereIn('outlet_id',$Outlet)->where('is_active','0')->get()->count();
            //     $totalChecklistNonActive = Checklist::where('jenis_id','!=','4')->whereIn('outlet_id',$Outlet)->where('is_active','0')->get()->count();
            //     $totalAuditToNonActive = Checklist::where('jenis_id','4')->whereIn('outlet_id',$Outlet)->where('is_active','2')->get()->count();
            //     $totalChecklistToNonActive = Checklist::where('jenis_id','!=','4')->whereIn('outlet_id',$Outlet)->where('is_active','2')->get()->count();
            // }else if(Auth()->user()->hasRole('Area Manager')){
            //     $totalChecklist = Checklist::whereIn('jenis_id',[1,2])->whereIn('outlet_id',$UserHasOutlet)->get()->count();
            //     $totalChecklistActive = Checklist::whereIn('jenis_id',[1,2])->whereIn('outlet_id',$UserHasOutlet)->where('is_active','1')->get()->count();
            //     $totalChecklistNonActive = Checklist::whereIn('jenis_id',[1,2])->whereIn('outlet_id',$UserHasOutlet)->where('is_active','0')->get()->count();
            //     $totalChecklistToNonActive = Checklist::where('jenis_id',[1,2])->whereIn('outlet_id',$UserHasOutlet)->where('is_active','2')->get()->count();
            //     $canSeeAudit =false;
            // }else if(Auth()->user()->hasRole('Trainer Brand')){
            //     $outlet = Outlet::whereIn('area_id', $areas)->pluck('id','id')->all();
            //     $totalChecklist = Checklist::whereIn('jenis_id',[3])->whereIn('outlet_id',$outlet)->get()->count();
            //     $totalChecklistActive = Checklist::whereIn('jenis_id',[3])->whereIn('outlet_id',$outlet)->where('is_active','1')->get()->count();
            //     $totalChecklistNonActive = Checklist::whereIn('jenis_id',[3])->whereIn('outlet_id',$outlet)->where('is_active','0')->get()->count();
            //     $totalChecklistToNonActive = Checklist::where('jenis_id',[3])->whereIn('outlet_id',$outlet)->where('is_active','2')->get()->count();
            //     $canSeeAudit =false;
            // }else if(Auth()->user()->hasRole('Auditor') || Auth()->user()->hasRole('Audit Manager')){
            //     $totalAudit = Checklist::where('jenis_id','4')->get()->count();
            //     $totalAuditActive = Checklist::where('jenis_id','4')->where('is_active','1')->get()->count();
            //     $totalAuditNonActive = Checklist::where('jenis_id','4')->where('is_active','0')->get()->count();
            //     $totalAuditToNonActive = Checklist::where('jenis_id','4')->where('is_active','2')->get()->count();
            //     $canSeeChecklist=false;
            // }else if(Auth()->user()->hasRole('Outlet')){
            //     $totalAudit = Checklist::where('jenis_id','4')->where('outlet_id',Auth()->user()->outlet_id)->get()->count();
            //     $totalChecklist = Checklist::where('jenis_id','!=','4')->where('outlet_id',Auth()->user()->outlet_id)->get()->count();
            //     $totalAuditActive = Checklist::where('jenis_id','4')->where('outlet_id',Auth()->user()->outlet_id)->where('is_active','1')->get()->count();
            //     $totalChecklistActive = Checklist::where('jenis_id','!=','4')->where('outlet_id',Auth()->user()->outlet_id)->where('is_active','1')->get()->count();
            //     $totalAuditNonActive = Checklist::where('jenis_id','4')->where('outlet_id',Auth()->user()->outlet_id)->where('is_active','0')->get()->count();
            //     $totalChecklistNonActive = Checklist::where('jenis_id','!=','4')->where('outlet_id',Auth()->user()->outlet_id)->where('is_active','0')->get()->count();
            //     $totalAuditToNonActive = Checklist::where('jenis_id','4')->where('outlet_id',Auth()->user()->outlet_id)->where('is_active','2')->get()->count();
            //     $totalChecklistToNonActive = Checklist::where('jenis_id','!=','4')->where('outlet_id',Auth()->user()->outlet_id)->where('is_active','2')->get()->count();
                
            // }else if(Auth()->user()->hasRole('Admin Maintenance') || Auth()->user()->hasRole('Maintenance Manager') || Auth()->user()->hasRole('Teknisi Maintenance')){
            //     $totalMaintenancePemesanan = MaintenanceItemPemesanan::get()->count();
            //     $totalMaintenancePemesananBaru = MaintenanceItemPemesanan::where('status','pemesanan')->get()->count();
            //     $totalMaintenancePemesananProses = MaintenanceItemPemesanan::where('status','proses')->get()->count();
            //     $totalMaintenancePemesananSelesai = MaintenanceItemPemesanan::where('status','selesai')->get()->count();
            //     $canSeeMaintenance = true;
            //     $canSeeAudit = $canSeeChecklist = false;
            // }else{
            //     $totalAudit = '';
            //     $totalChecklist = '';
            //     $totalAuditActive = '';
            //     $totalChecklistActive = '';
            //     $totalAuditNonActive = '';
            //     $totalChecklistNonActive = '';
            //     $canSeeAudit = false;
            //     $canSeeChecklist = false;
            // }
            return view('dashboard.index',[
                'periode' => $periode,
                'param' => $param,
                'praAssessment' => $praAssessment ? $praAssessment : '0',
                'sudahisi' => $sudahisi ?  $sudahisi : '0',
                'komplit' => $komplit ? $komplit : '0',
                'nonkomplit' => $nonkomplit ? $nonkomplit : '0',
                // 'totalAudit' => $totalAudit ? $totalAudit : 0,
                // 'totalAuditActive'=> $totalAuditActive ? $totalAuditActive : 0,
                // 'totalAuditNonActive' => $totalAuditNonActive ? $totalAuditNonActive : 0,
                // 'totalChecklist' => $totalChecklist ? $totalChecklist : 0,
                // 'totalChecklistActive' => $totalChecklistActive ? $totalChecklistActive : 0,
                // 'totalChecklistNonActive' => $totalChecklistNonActive ? $totalChecklistNonActive : 0,
                // 'totalAuditToNonActive' => $totalAuditToNonActive ?$totalAuditToNonActive : 0,
                // 'totalChecklistToNonActive' => $totalChecklistToNonActive ? $totalChecklistToNonActive : 0,
                // 'totalMaintenancePemesanan' => $totalMaintenancePemesanan ? $totalMaintenancePemesanan : 0,
                // 'totalMaintenancePemesananBaru' => $totalMaintenancePemesananBaru ? $totalMaintenancePemesananBaru : 0,
                // 'totalMaintenancePemesananProses' => $totalMaintenancePemesananProses ? $totalMaintenancePemesananProses : 0,
                // 'totalMaintenancePemesananSelesai' => $totalMaintenancePemesananSelesai ? $totalMaintenancePemesananSelesai : 0,
                // 'canSeeAudit' => $canSeeAudit,
                // 'canSeeChecklist' => $canSeeChecklist,
                // 'canSeeMaintenance' => $canSeeMaintenance,
                'user' => Auth()->user()->name,
            ]);
        }
    }
}
