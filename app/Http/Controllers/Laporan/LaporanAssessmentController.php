<?php


namespace App\Http\Controllers\Laporan;

use App\Exports\AssessmentExport;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Models\Pegawai;
use App\Models\Assessment;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\User;
use App\Models\Menu;
use App\Models\GeneralModel;
use App\Models\DetailChecklist;
use App\Models\Master\Brand;
use App\Models\Master\Checklist\Question;
use App\Models\Setting\PoinPenilaian;
use App\Models\Setting\Point;
use App\Models\Setting\PraAssessment;
use App\Models\Setting\SesiPenilai;
use App\Models\Setting\StandarPenilaian;
use Illuminate\Support\Facades\Auth;
class LaporanAssessmentController extends Controller
{
    protected $__route;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','laporan.assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ','_',Menu::where('route_name','laporan.assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ','_',Menu::where('route_name','laporan.assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ','_',Menu::where('route_name','laporan.assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));
        $this->__route = 'laporan.assessment';
        $this->pagetitle = 'Lapotan Assessment';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {
        return view($this->__route.'.index',[
            'pagetitle' => 'Laporan Assessment',
            'breadcrumb' => $this->pagetitle,
            'route' => $this->__route,
            'pegawai' => Pegawai::get(),
            'breadcrumb_arr' => [
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
        ]);

    }

	public function create(Request $request)
	{
		return view($this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
			'actionform' => 'insert',
            'brand' => Brand::get(),
		]);
	}	

	public function edit(Request $request)
	{
		try{
			$data = Question::find((int)$request->input('id'));
            $hasParent = $data->pluck('parent_id','parent_id')->all();
            $hisParent = $data->parent_id != 0 ? Question::select('question')->where('id',$data->parent_id)->first()->question : 'Sebagai Root Pertanyaan';
			return view($this->__route.'.form',[
				'actionform' => 'update',
                'pagetitle' => $this->pagetitle,
				'data' => $data,
                'brand' => Brand::get(),
                'hasParent' => $hasParent,
                'hisParent' => $hisParent,
				'parent' => json_encode($this->getparentmenu((int)$data->parent_id))
			]);
		}catch(Exception $e){}
	}

    public function getpicture(Request $request)
	{
		try{
            return view($this->__route.'.getpicture',[
				'actionform' => 'insert',
                'pagetitle' => $this->pagetitle,
                'outlet_id' => $request->outlet_id,
                'brand_id' => $request->brand_id,
                'question_id' => $request->question_id,
			]);
		}catch(Exception $e){}
	}

    public function showpicture(Request $request)
	{
		try{
            $data = DetailChecklist::where('question_id',$request->question_id)->where('checklist_id',$request->checklist_id)->first();
            return view($this->__route.'.showpicture',[
				'actionform' => 'insert',
                'pagetitle' => $this->pagetitle,
                'data' => $data,
                'type' => $request->jenis_gambar
			]);
		}catch(Exception $e){}
	}
    
	public function store(Request $request)
	{
		$result = [
			'flag' => 'error',
			'msg' => 'Error System',
			'title' => 'Error'
		];	

		$validator = $this->validateform($request);   

		if (!$validator->fails()) {
			$param['brand_id'] = (int)$request->input('brand_id');
			$param['parent_id'] = (int)$request->input('parent_id');
			$param['question'] = !empty($request->input('question'))?$request->input('question'):'';
			$param['status'] = '1';

            $parent = Question::where('parent_id', (int)$request->input('parent_id'))->orderBy('order', 'desc')->first();

			switch ($request->input('actionform')) {
				case 'insert': DB::beginTransaction();
                    try{
                        $param['order'] = (isset($parent)) ? ($parent->order + 1) : 1;      						
                        Question::create((array)$param);

                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses tambah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }
				break;
				
				case 'update': DB::beginTransaction();
                    try{
                        Question::find((int)$request->input('id'))->update((array)$param);

                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses ubah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => 'Gagal ubah data',
                        'title' => 'Gagal'
                        ];
                    }
				break;
			}
		}else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'warning',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];   			
		}

		return response()->json($result);			
	}	

	public function delete(Request $request)
	{
        DB::beginTransaction();
        try{
            Question::find((int)$request->input('id'))->delete();

            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal hapus data',
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);		
	}	
    
    public function showassessment(Request $request,$id,$pegawai_id,$penilai_id)
    {
        $data = DB::table('assessments')->select('*')->where('id',$id)->where('pegawai_id', $pegawai_id)->where('penilai_id', $penilai_id)->first();
        if(!$data){
            return view($this->__route.'.showassessment',['pagetitle' => 'Laporan Assessment Pegawai',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
            'can' => false
            ]);
        }
        $praassessment = PraAssessment::where('id',$id)->first();
        $sesi = SesiPenilai::where('id', $data->sesi_id)->get();
        $pemberi_nilai = Pegawai::where('id', $data->penilai_id)->pluck('nama')->first();
        $pegawai = DB::table('view_detail_pegawai')->select('*')->where('id',$data->pegawai_id)->first();
        $point = "call procedure_assessment('$data->pegawai_id','$data->penilai_id','$data->sesi_id','$data->periode_id','$data->level_id')";
        $points = collect(DB::select($point));
        
        $dataCollection = new Collection;  
            foreach ($points->where('parent_id',0) as $key => $induk) {
                $dataCollection->push($induk);
                $prevInduk = null;
                foreach ($points->where('parent_id',$induk->id) as $key => $sub1) {
                    if ($prevInduk === $sub1->id) {
                        continue;
                    }
                    $dataCollection->push($sub1);
                    $prevSub1 = null;
                    
                    foreach ($points->where('parent_id',$sub1->id) as $key => $sub2) {
                        if ($prevSub1 === $sub2->id) {
                            continue;
                        }
                        $dataCollection->push($sub2);
                        $prevSub2 = null;
                        foreach ($points->where('parent_id',$sub2->id) as $key => $sub3) {
                            if ($prevSub2 === $sub3->id) {
                                continue;
                            }
                            $dataCollection->push($sub3);
                            $prevSub3 = null;
                            foreach ($points->where('parent_id',$sub3->id) as $key => $sub4) {
                                if ($prevSub3 === $sub4->id) {
                                    continue;
                                }
                                $prevSub3 = $sub4->id;
                            }
                            $prevSub2 = $sub3->id;
                        }
                        $prevSub1 = $sub2->id;
                    }
                    $prevInduk = $sub1->id;
                }
            }
        return view($this->__route.'.showassessment',[
            'pagetitle' => 'Laporan Assessment Pegawai',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
            'can' => true,
            'data' => $data,
            'pegawai' => $pegawai,
            'point' =>$dataCollection,
            'sesi' => $sesi,
            'penilai' => $pemberi_nilai,
            'praassessment' => $praassessment,
            'actionform'=> 'insert'
        ]);
    }

	protected function validateform($request)
	{
        $required['question'] = 'required|max:256';
        $message['question.required'] = 'wajib diinput';
        $message['question.max'] = 'Pertanyaan maksimal 256 karakter';

        $message['parent_id.required'] = 'Parent wajib dipilih';
        $message['brand_id.required'] = 'Brand wajib dipilih';

        return Validator::make($request->all(), $required, $message); 		
	}

    public function datatable(Request $request)
    {
        try{
            $data1 = DB::table('new_view_pra_assessment')->select('*')->orderby('id','desc');
            $data2 = DB::table('view_pra_assessment')->select('*')->union($data1)->orderby('id','desc');

            $result = DB::table(DB::raw("({$data2->toSql()}) as combined_result"))
                    ->mergeBindings($data2); // Merge bindings from the unionQuery
            return datatables()->of($result)
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('nik'))) {
                    $query->where('nik', 'like', "%{$request->input('nik')}%");
                }
                if (!empty($request->input('nama'))) {
                    $query->whereIn('pegawai_id',[$request->input('nama')])
                    ->orwhereIn('atasan_id',[$request->input('nama')])
                    ->orwhereIn('rekan_id',[$request->input('nama')])
                    ->orwhereIn('tim_id',[$request->input('nama')]);
                }
            })
            ->editColumn('nama',function($row){
                $assessment = Assessment::where('pegawai_id',$row->pegawai_id)->where('praassessment_id', $row->id)->where('penilai_id', $row->pegawai_id)->first();
                if($assessment != NULL){
                $btn ='<div align="left">';
                $btn .= '<a href="'.route('laporan.assessment.showassessment',['id' => $assessment->id,'pegawai_id'=>$row->pegawai_id,'penilai_id'=>$row->pegawai_id]).'"> <span class="badge badge-warning cls-show-data" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$row->pegawai_id.'>'.$row->nik.' - '.$row->nama.'</span></a>';
                $btn .= '</div>';
                }else{
                    $btn ='<div align="left">';
                $btn .= '<a href="#"> <span class="badge badge-success cls-show-data" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$row->pegawai_id.'>'.$row->nik.' - '.$row->nama.'</span></a>';
                $btn .= '</div>';
                }
                return $btn;
            })
            ->editColumn('atasan',function($row){
                $atasan_id = explode(',', $row->atasan_id);
                $atasan = Pegawai::whereIn('id', $atasan_id)->get();
                if($atasan){
                    $btn ='<div align="left">';
                    foreach($atasan as $key => $valueAtasan){
                        $assessment = Assessment::where('pegawai_id',$row->pegawai_id)->where('praassessment_id', $row->id)->where('penilai_id', $valueAtasan->id)->first();
                        if($assessment != NULL){
                            $btn .= '<a href="'.route('laporan.assessment.showassessment',['id' => $assessment->id,'pegawai_id'=>$row->pegawai_id,'penilai_id'=>$valueAtasan->id]).'"> <span class="badge badge-warning cls-show-data m-1" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$valueAtasan->id.'>'.$valueAtasan->nama.'</span></a>';
                            $btn .= '<br>';
                        }else{
                            $btn .= '<a href="#"><span class="badge badge-success cls-show-data m-1" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$valueAtasan->id.'>'.$valueAtasan->nama.'</span></a>';
                            $btn .= '<br>';
                        }
                    }
                    $btn .= '</div>';
                }else{
                    $btn = '';
                }
                return $btn;
            })
            ->editColumn('rekan',function($row){
                $rekan_id = explode(',', $row->rekan_id);
                $rekan = Pegawai::whereIn('id',$rekan_id)->get();
                if($rekan){
                    $btn ='<div align="left">';
                    foreach($rekan as $key => $valueRekan){
                        $assessment = Assessment::where('pegawai_id',$row->pegawai_id)->where('praassessment_id', $row->id)->where('penilai_id', $valueRekan->id)->first();
                        if($assessment != NULL){
                            $btn .= '<a href="'.route('laporan.assessment.showassessment',['id' => $assessment->id,'pegawai_id'=>$row->pegawai_id,'penilai_id'=>$valueRekan->id]).'"> <span class="badge badge-warning cls-show-data m-1" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$valueRekan->id.'>'.$valueRekan->nama.'</span></a>';
                            $btn .= '<br>';
                        }else{
                            $btn .= '<a href="#"><span class="badge badge-success cls-show-data m-1" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$valueRekan->id.'>'.$valueRekan->nama.'</span></a>';
                            $btn .= '<br>';
                        }
                    }
                    $btn .= '</div>';
                }else{
                    $btn = '';
                }
                return $btn;
            })
            ->editColumn('tim',function($row){
                $tim_id = explode(',', $row->tim_id);
                $tim = Pegawai::whereIn('id', $tim_id)->get();
                if($tim ){
                    $btn ='<div align="left">';
                    foreach($tim as $key => $valueTim){
                        $assessment = Assessment::where('pegawai_id',$row->pegawai_id)->where('praassessment_id', $row->id)->where('penilai_id', $valueTim->id)->first();
                        if($assessment != NULL){
                            $btn .= '<a href="'.route('laporan.assessment.showassessment',['id' => $assessment->id,'pegawai_id'=>$row->pegawai_id,'penilai_id'=>$valueTim->id]).'"> <span class="badge badge-warning cls-show-data m-1" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$valueTim->id.'>'.$valueTim->nama.'</span></a>';
                            $btn .= '<br>';
                        }else{
                            $btn .= '<a href="#"><span class="badge badge-success cls-show-data m-1" data-id='.$row->id.' data-pegawai_id='.$row->pegawai_id.' data-penilai = '.$valueTim->id.'>'.$valueTim->nama.'</span></a>';
                            $btn .= '<br>';
                        }
                    }
                    $btn .= '</div>';
                }else{
                    $btn = '';
                }
                return $btn;
            })
            ->addColumn('action', function ($row){
                $assessment = Assessment::where('praassessment_id', $row->id)->first();
                if($assessment){
                $button = '<div align="center">';
                $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm btn-icon cls-button-cetakassessment" data-id="'.$row->id.'" data-pegawai_id="'.$row->pegawai_id.'" data-nama="'.$row->nama.'" data-level_id="'.$row->level_id.'" data-toggle="tooltip" title="Cetak Assessment '.$row->nama.'">
                            <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Devices/Printer.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M16,17 L16,21 C16,21.5522847 15.5522847,22 15,22 L9,22 C8.44771525,22 8,21.5522847 8,21 L8,17 L5,17 C3.8954305,17 3,16.1045695 3,15 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,15 C21,16.1045695 20.1045695,17 19,17 L16,17 Z M17.5,11 C18.3284271,11 19,10.3284271 19,9.5 C19,8.67157288 18.3284271,8 17.5,8 C16.6715729,8 16,8.67157288 16,9.5 C16,10.3284271 16.6715729,11 17.5,11 Z M10,14 L10,20 L14,20 L14,14 L10,14 Z" fill="#000000"/>
                                    <rect fill="#000000" opacity="0.3" x="8" y="2" width="8" height="2" rx="1"/>
                                </g>
                            </svg><!--end::Svg Icon--></span>
                </button>';

                $button .= '</div>';
                }else{
                    $button = '';
                }
                return $button;
            })
            ->editColumn('periode', function($row){
                return $row->periode.'<br/>'.GeneralController::tglFormat($row->tgl_mulai,1).' s/d '.GeneralController::tglFormat($row->tgl_akhir,1);
            })
            ->rawColumns(['action','nama','atasan','rekan','tim','periode'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }

    public function cetakassessment(Request $request)
	{
		try{
            $data1 = DB::table('new_view_pra_assessment')->where('id', $request->id)->orderby('id','desc');
            $data2 = DB::table('view_pra_assessment')->union($data1)->where('id', $request->id)->orderby('id','desc');

            $pegawai = DB::table('view_detail_pegawai')->select('*')->where('id',$request->pegawai_id)->first();
            $praassessment = $data2->select('*')->first();
            $standar = StandarPenilaian::orderby('batas_bawah')->get();
            $poin = PoinPenilaian::orderby('poin')->get();
            $joindate = GeneralController::tglFormat($pegawai->join_date,1);
            $datas[][] =$hasils[][]='';
            $sesi=['3','4'];
            if($praassessment->periode_id <= 4){
                $assessment = collect(DB::select("call procedure_pasca_assessment($request->id)"));//Assessment::where('praassessment_id', $praassessment->id)->get();
                foreach($assessment as $key => $val){
                    $dinilai = $val->pegawai_id ? $val->pegawai_id:'NULL';
                    $penilai_id = $val->penilai_id ? $val->penilai_id:'NULL';
                    $getdatas = "call procedure_assessment($dinilai,$penilai_id,'$val->sesi_id','$val->periode_id','$val->level_id')";
                    $data = collect(DB::select($getdatas));
                    foreach($data as $keys => $vals){
                        $datas[$vals->id]['id'] = $vals->id;
                        $datas[$vals->id]['question'] = $vals->question;
                        $datas[$vals->id]['parent_id'] = $vals->parent_id;
                        $datas[$vals->id]['pegawai_id'] = $vals->pegawai_id;
                        $datas[$vals->id]['praassessment_id'] = $vals->praassessment_id;
                        $datas[$vals->id]['sesi_id'] = $vals->sesi_id;

                        if($val->sesi_id == 1){
                            $datas[$vals->id]['dirisendiri'] = $vals->penilai_id;
                            $datas[$vals->id]['jawabansendiri'] = $vals->jawaban;
                            $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                        }elseif($val->sesi_id == 2){
                            $datas[$vals->id]['atasansendiri'] = $vals->penilai_id;
                            $datas[$vals->id]['jawabanatasan'] = $vals->jawaban;
                            $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                        }elseif($val->sesi_id == 3){
                            $datas[$vals->id]['rekansendiri'] = $vals->penilai_id;
                            $datas[$vals->id]['jawabanrekan'] = $vals->jawaban;
                            $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                        }elseif($val->sesi_id == 4){
                            $datas[$vals->id]['timsendiri'] = $vals->penilai_id;
                            $datas[$vals->id]['jawabantim'] = $vals->jawaban;
                            $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                        }
                        $assessment_id = $val->assessment_id ? $val->assessment_id : 'NULL';
                        $getsummary = "call procedure_summary('$praassessment->id',$assessment_id)";
                        $datasummary = collect(DB::select($getsummary));
                        foreach($datasummary as $key => $valb){
                            if($vals->parent_id == 0 && $vals->id == $valb->parent_id){
                                if($val->sesi_id == 1){
                                    $hasils[$valb->parent_id]['avgsendiri'] = ($valb->ratarata*0.2) ;
                                    $hasils[$valb->parent_id]['totalsendiri'] = $valb->total;
                                    $hasils[$valb->parent_id]['jumlahsendiri'] = $valb->jumlah;
                                }elseif($val->sesi_id == 2){
                                    $hasils[$valb->parent_id]['avgatasan'] = ($valb->ratarata*0.5);
                                    $hasils[$valb->parent_id]['totalatasan'] = $valb->total;
                                    $hasils[$valb->parent_id]['jumlahatasn'] = $valb->jumlah; 
                                }
                                elseif($val->sesi_id == 3 ){
                                    $hasils[$valb->parent_id]['avgrekan'] = $valb->ratarata;
                                    $hasils[$valb->parent_id]['totalrekan'] = $valb->total;
                                    $hasils[$valb->parent_id]['jumlahrekan'] = $valb->jumlah; 
                                }elseif($val->sesi_id == 4){
                                    $hasils[$valb->parent_id]['avgtim'] = $valb->ratarata;
                                    $hasils[$valb->parent_id]['totaltim'] = $valb->total;
                                    $hasils[$valb->parent_id]['jumlahtim'] = $valb->jumlah;
                                }
                            }
                            $result[$valb->parent_id]['avgsendiri'] = isset($hasils[$valb->parent_id]['avgsendiri']) ? $hasils[$valb->parent_id]['avgsendiri'] : null;
                            $result[$valb->parent_id]['avgatasan'] = isset($hasils[$valb->parent_id]['avgatasan']) ? $hasils[$valb->parent_id]['avgatasan'] : null;
                            $result[$valb->parent_id]['avgrekan'] = isset($hasils[$valb->parent_id]['avgrekan']) ? $hasils[$valb->parent_id]['avgrekan'] : null;
                            $result[$valb->parent_id]['avgtim'] = isset($hasils[$valb->parent_id]['avgtim']) ? $hasils[$valb->parent_id]['avgtim'] : null;
                            $result[$valb->parent_id]['totalsendiri'] = isset($hasils[$valb->parent_id]['totalsendiri']) ? $hasils[$valb->parent_id]['totalsendiri'] : null;
                            $result[$valb->parent_id]['totalatasan'] = isset($hasils[$valb->parent_id]['totalatasan']) ? $hasils[$valb->parent_id]['totalatasan'] : null;
                            $result[$valb->parent_id]['totalrekan'] = isset($hasils[$valb->parent_id]['totalrekan']) ? $hasils[$valb->parent_id]['totalrekan'] : null;
                            $result[$valb->parent_id]['totaltim'] = isset($hasils[$valb->parent_id]['totaltim']) ? $hasils[$valb->parent_id]['totaltim'] : null;
                            $result[$valb->parent_id]['jumlahsendiri'] = isset($hasils[$valb->parent_id]['jumlahsendiri']) ? $hasils[$valb->parent_id]['jumlahsendiri'] : null;
                            $result[$valb->parent_id]['jumlahatasn'] = isset($hasils[$valb->parent_id]['jumlahatasn']) ? $hasils[$valb->parent_id]['jumlahatasn'] : null;
                            $result[$valb->parent_id]['jumlahrekan'] = isset($hasils[$valb->parent_id]['jumlahrekan']) ? $hasils[$valb->parent_id]['jumlahrekan'] : null;
                            $result[$valb->parent_id]['jumlahtim'] = isset($hasils[$valb->parent_id]['jumlahtim']) ? $hasils[$valb->parent_id]['jumlahtim'] : null;
                        }
                    }
                }
                unset($hasils[0]);
                unset($datas[0]);
                foreach($result as $key => $valc){
                    $result[$key]['rekantim'] = $praassessment->tim_id != NULL ? number_format(((($valc['avgrekan']+$valc['avgtim'])/2)*0.3),1) : number_format(($valc['avgrekan']*0.3),1);
                    $result[$key]['hasilakhir'] = $praassessment->tim_id != NULL ? number_format((($valc['avgsendiri'])+($valc['avgatasan'])+((($valc['avgrekan']+$valc['avgtim'])*0.3)/2)),1) : number_format((($valc['avgsendiri'])+($valc['avgatasan'])+($valc['avgrekan']*0.3)),1);
                    $result[$key]['hasiltext'] = StandarPenilaian::where('batas_bawah','<=',$result[$key]['hasilakhir'])->where('batas_atas','>=',$result[$key]['hasilakhir'])->pluck('nama')->first();
                }

                $sum = 0;
                foreach($result as $kuy => $vald){
                    $sum += $vald['hasilakhir'];
                }
                $summary=[];
                $summary['total'] = round($sum / count($hasils),1);
                $summary['totaltext'] = StandarPenilaian::where('batas_bawah','<=',$summary['total'])->where('batas_atas','>=',$summary['total'])->pluck('nama')->first();
            }else{
                $assessment = collect(DB::select("call procedure_new_pasca_assessment($request->id)"));
                $roots = DB::table('view_hierarchy_points')->select('*')->orderby('point_id')->get();
                $hasCalculated = [];
                foreach($assessment as $key => $val){
                    $dinilai = $val->pegawai_id ? $val->pegawai_id:'NULL';
                    $penilai_id = $val->as_penilai_id ? $val->as_penilai_id:'NULL';

                    if($penilai_id != 'NULL'){
                        $getdatas = "call procedure_assessment('$dinilai','$penilai_id','$val->sesi_id','$val->periode_id','$val->level_id')";
                        $data = collect(DB::select($getdatas));
                        foreach($data as $keys => $vals){
                            $datas[$vals->id]['id'] = $vals->id;
                            $datas[$vals->id]['question'] = $vals->question;
                            $datas[$vals->id]['pegawai_id'] = $vals->pegawai_id;
                            $datas[$vals->id]['praassessment_id'] = $vals->praassessment_id;
                            // $datas[$vals->id]['sesi_id'] = $vals->sesi_id;
                            foreach($roots as $root){
                                if($vals->id == $root->point_id){
                                    $parent_id = ($vals->id == $root->root_id) ? '0' : ($root->orders == 1 ? $root->parent_id : $root->root_id);
                                    $root_id  = ($vals->id == $root->root_id) ? '0' : $root->orders;
                                    $kepala = $root->parent_id;
                                    
                                    $datas[$vals->id]['parent_id'] = $parent_id;
                                    $datas[$vals->id]['root_id'] = $root_id;
                                    $datas[$vals->id]['kepala'] = $kepala;
                                }
                            }
                            if($val->sesi_id == 1){
                                $datas[$vals->id]['dirisendiri'] = $vals->penilai_id;
                                $datas[$vals->id]['jawabansendiri'] = $vals->jawaban;
                                $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                            }elseif($val->sesi_id == 2){
                                $gethasilatasan = "call procedure_hitung_assessment($val->pegawai_id,'$val->sesi_id','$val->periode_id','$request->id','$vals->id')";
                                $hasilhitungatasan = DB::select($gethasilatasan);
                                $datas[$vals->id]['jawabanatasan'] = $hasilhitungatasan != null ? $hasilhitungatasan[0]->hasil : null;
                                $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                            }elseif($val->sesi_id == 3 or $val->sesi_id == 4){
                                $gethasilrekan = "call procedure_hitung_assessment($val->pegawai_id,'$val->sesi_id','$val->periode_id','$request->id','$vals->id')";
                                $hasilhitungrekan = DB::select($gethasilrekan);
                                $datas[$vals->id]['jawabanrekan'] = $hasilhitungrekan != null ? $hasilhitungrekan[0]->hasil: null;
                                $datas[$vals->id]['detail'][$vals->penilai_id] = $vals->detail;
                            }

                            $assessment_id = $val->assessment_id ? $val->assessment_id : 'NULL';
                            $getsummary = "call procedure_new_summary('$praassessment->id','$val->sesi_id')";
                            $datasummary = collect(DB::select($getsummary));
                            $rata[]=0;
                            $totalrata[] =0;
                            $totalpoint[] = 0;
                            foreach($datasummary as $key => $valb){
                                if($vals->parent_id == 0 && $vals->id == $valb->parent_id){
                                    if($val->sesi_id == 1){
                                        $hasils[$valb->parent_id]['avgsendiri'] = ($valb->hasil_akhir*0.2) ;
                                        $hasils[$valb->parent_id]['totalsendiri'] = $valb->total;
                                        $hasils[$valb->parent_id]['jumlahsendiri'] = $valb->jumlah_point;
                                    }elseif($val->sesi_id == 2){
                                        $hasils[$valb->parent_id]['avgatasan'] = ($valb->hasil_akhir*0.5);
                                        $hasils[$valb->parent_id]['totalatasan'] = $valb->total;
                                        $hasils[$valb->parent_id]['jumlahatasn'] = $valb->jumlah_point; 
                                    }else if ($val->sesi_id == 3 || $val->sesi_id == 4) {
                                        // Only process for a parent_id if it's not already processed
                                        if (!isset($hasCalculated[$valb->parent_id][$valb->sesi_id])) {
                                            // Hitung hanya sekali untuk sesi_id 3 atau 4 per parent_id
                                            $rata[$valb->parent_id] += $valb->hasil_akhir;
                                            $totalrata[$valb->parent_id] += $valb->total;
                                            $totalpoint[$valb->parent_id] += $valb->jumlah_point;
                                            
                                            $hasils[$valb->parent_id]['avgrekan'] = ($rata[$valb->parent_id] * 0.3);
                                            $hasils[$valb->parent_id]['totalrekan'] = $totalrata[$valb->parent_id];
                                            $hasils[$valb->parent_id]['jumlahrekan'] = $totalpoint[$valb->parent_id];
                                            
                                            // Tandai bahwa perhitungan untuk parent_id dan sesi_id ini sudah dilakukan
                                            $hasCalculated[$valb->parent_id][$valb->sesi_id] = true;
                                        }
                                    }
                                }
                                
                                $result[$valb->parent_id]['avgsendiri'] = isset($hasils[$valb->parent_id]['avgsendiri']) ? number_format($hasils[$valb->parent_id]['avgsendiri'],2) : null;
                                $result[$valb->parent_id]['avgatasan'] = isset($hasils[$valb->parent_id]['avgatasan']) ? number_format($hasils[$valb->parent_id]['avgatasan'],2) : null;
                                $result[$valb->parent_id]['avgrekan'] = isset($hasils[$valb->parent_id]['avgrekan']) ? number_format($hasils[$valb->parent_id]['avgrekan'],2) : null;
                                $result[$valb->parent_id]['totalsendiri'] = isset($hasils[$valb->parent_id]['totalsendiri']) ? $hasils[$valb->parent_id]['totalsendiri'] : null;
                                $result[$valb->parent_id]['totalatasan'] = isset($hasils[$valb->parent_id]['totalatasan']) ? $hasils[$valb->parent_id]['totalatasan'] : null;
                                $result[$valb->parent_id]['totalrekan'] = isset($hasils[$valb->parent_id]['totalrekan']) ? $hasils[$valb->parent_id]['totalrekan'] : null;
                                $result[$valb->parent_id]['jumlahsendiri'] = isset($hasils[$valb->parent_id]['jumlahsendiri']) ? $hasils[$valb->parent_id]['jumlahsendiri'] : null;
                                $result[$valb->parent_id]['jumlahatasn'] = isset($hasils[$valb->parent_id]['jumlahatasn']) ? $hasils[$valb->parent_id]['jumlahatasn'] : null;
                                $result[$valb->parent_id]['jumlahrekan'] = isset($hasils[$valb->parent_id]['jumlahrekan']) ? $hasils[$valb->parent_id]['jumlahrekan'] : null;
                            }
                        }
                    }
                }
                unset($hasils[0]);
                unset($datas[0]);
                foreach($result as $key => $valc){
                    $result[$key]['rekantim'] = number_format($valc['avgrekan'],2);
                    $result[$key]['hasilakhir'] = number_format(($valc['avgsendiri'])+($valc['avgatasan'])+($valc['avgrekan']),2);
                    $result[$key]['hasiltext'] = StandarPenilaian::where('batas_bawah','<=',number_format($result[$key]['hasilakhir'],2))->where('batas_atas','>=',number_format($result[$key]['hasilakhir'],2))->pluck('nama')->first();
                }
                
                $sum = 0;
                foreach($result as $kuy => $vald){
                    $sum += $vald['hasilakhir'];
                }
                
                $summary=[];
                $summary['total'] = round($sum / count($hasils),2);
                $summary['totaltext'] = StandarPenilaian::where('batas_bawah','<=',$summary['total'])->where('batas_atas','>=',$summary['total'])->pluck('nama')->first();
            }

            $dataCollection = new Collection;  
            $a= collect($datas);
            foreach ($a->where('kepala',0) as $key => $induk) {
                $dataCollection->push($induk);
                $prevInduk = null;
                foreach ($a->where('kepala',$induk['id']) as $key => $sub1) {
                    if ($prevInduk === $sub1['id']) {
                        continue;
                    }
                    $dataCollection->push($sub1);
                    $prevSub1 = null;
                    
                    foreach ($a->where('kepala',$sub1['id']) as $key => $sub2) {
                        if ($prevSub1 === $sub2['id']) {
                            continue;
                        }
                        $dataCollection->push($sub2);
                        $prevSub2 = null;
                        foreach ($a->where('kepala',$sub2['id']) as $key => $sub3) {
                            if ($prevSub2 === $sub3['id']) {
                                continue;
                            }
                            $dataCollection->push($sub3);
                            $prevSub3 = null;
                            foreach ($a->where('kepala',$sub3['id']) as $key => $sub4) {
                                if ($prevSub3 === $sub4['id']) {
                                    continue;
                                }
                                $prevSub3 = $sub4['id'];
                            }
                            $prevSub2 = $sub3['id'];
                        }
                        $prevSub1 = $sub2['id'];
                    }
                    $prevInduk = $sub1['id'];
                }
            }
            $induks = $dataCollection->where('parent_id',0);
            $namaFile = "Laporan Assessment an ".$pegawai->nama.".xlsx";
            $export = new AssessmentExport($pegawai, $joindate,$praassessment,$assessment, $dataCollection,$induks, $result,$standar,$poin,$summary);
            return Excel::download($export, $namaFile);
		}catch(Exception $e){}
	}


}