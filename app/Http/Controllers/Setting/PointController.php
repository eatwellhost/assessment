<?php


namespace App\Http\Controllers\Setting;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use App\Models\Master\Brand;
use App\Models\Master\Checklist\Question;
use App\Models\Master\Checklist\JenisAudit;
use App\Models\Menu;
use App\Models\Setting\Jenis;
use App\Models\Setting\LevelAssessment;
use App\Models\Setting\Point;
use DB;

 
class PointController extends Controller
{
    protected $__route;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','setting.point_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ','_',Menu::where('route_name','setting.point_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ','_',Menu::where('route_name','setting.point_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ','_',Menu::where('route_name','setting.point_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));

        $this->__route = 'setting.point_assessment';
        $this->pagetitle = 'Point Assessment';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {
        return view($this->__route.'.index',[
            'pagetitle' => 'Point Assessment',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Master',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
        ]);

    }

    public function gettreequestion(Request $request)
	{
        $brand_id = $request->brand_id ? $request->brand_id : 0  ;
        $jenis_id = $request->jenis_id ? $request->jenis_id : 0  ;

		$data = Point::orderBy('order')->get();
		$html = '<ol class="dd-list">';
		$html .= $this->recursiveMenu($data, 0);
		$html .= '</ol>';
		return response()->json(compact('html'));
	}

	private function recursiveMenu($data, $parent_id)
	{
		$html = '';
		$result = $data->where('parent_id', (int)$parent_id)->sortBy('order');

		foreach($result as $val){
			$html .= '<li class="dd-item dd3-item" data-id="'.(int)$val->id.'">';
			$html .= '<div class="dd-handle dd3-handle"> </div>';
            if($val->parent_id == 0){
            $html .= '<div class="dd3-content"><strong>'.$val->question.'</strong>';
            }else{
                $html .= '<div class="dd3-content"><strong>'.$val->question.'</strong>';
            }
            $html .= '<span class="pull-right" style="float:right">';
            /*if (Gate::allows($this->__permission.'-edit')) {
                $html .= '<a class="text-primary cls-button-edit" href="javascript:;" data-id="'.(int)$val->id.'" data-toggle="tooltip" title="Ubah Menu '.$val->label.'"><i class="flaticon-edit-1"></i></a>';
            }*/
            $html .= '<a class="text-primary btn-light-warning w-30px h-30px  cls-button-edit" href="javascript:;" data-id="'.(int)$val->id.'" data-toggle="tooltip" title="Ubah Menu '.$val->question.'">			             
			<!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"/>
                <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"/>
                <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"/>
                </svg>
                </span>            
			</button>
			</a>';

            $child = $data->where('parent_id', (int)$val->id)->sortBy('order');

            if(! $child->isEmpty() ){
                /*if (Gate::allows($this->__permission.'-delete')) {
            	    $html .= '&nbsp;';
                    $html .= '<a style="color: #CCCCCC; cursor: not-allowed;" class="nounderline" href="javascript:;" data-id="'.(int)$val->id.'" data-label="'.$val->label.'" data-toggle="tooltip" title="Hapus Menu '.$val->label.'"><i class="flaticon2-trash"></i></a>';
                }*/
                $html .= '&nbsp;';
                $html .= '<a style="color: #CCCCCC; cursor: not-allowed;" class="nounderline" href="javascript:;" data-id="'.(int)$val->id.'" data-label="'.$val->question.'" data-toggle="tooltip" title="Hapus Menu '.$val->question.'">
				<!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"/>
                <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="currentColor"/>
                <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="currentColor"/>
                </svg>
                </span>
                <!--end::Svg Icon-->
				</a>';
            }else{
                /*if (Gate::allows($this->__permission.'-delete')) {
            	    $html .= '&nbsp;';
                    $html .= '<a class="text-danger cls-button-delete nounderline" href="javascript:;" data-id="'.(int)$val->id.'" data-label="'.$val->label.'" data-toggle="tooltip" title="Hapus Menu '.$val->label.'"><i class="flaticon2-trash"></i></a>';
                }*/
                $html .= '&nbsp;';
                $html .= '<a class="text-danger cls-button-delete nounderline" href="javascript:;" data-id="'.(int)$val->id.'" data-label="'.$val->question.'" data-toggle="tooltip" title="Hapus Menu '.$val->question.'">
				<!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"/>
                <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="currentColor"/>
                <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="currentColor"/>
                </svg>
                </span>
                <!--end::Svg Icon-->
				</a>';
            }

            $html .= '</div>';


            if((bool)$child->count()){
               $html .= '<ol class="dd-list">';
               $html .= $this->recursiveMenu($data, (int)$val->id);
               $html .= '</ol>';
            }
            $html .= '</li>';
        }
        return $html;
    }

	public function create(Request $request)
	{
		return view($this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
			'actionform' => 'insert',
            'level' => LevelAssessment::get(),
		]);
	}	

	private function getparentmenu($id)
	{
		try{
			$data = Point::find((int)$id);
            if($data){
            return [
				'id' => (int)$data->id,
				'text' => $data->question
			];
            }else{
                return [
                    'id' => 0,
                    'text' => 'Sebagai Root Pertanyaan'
                ];
            }
		}catch(Exception $e){
			return [
				'id' => 0,
				'text' => 'Sebagai Root Pertanyaan'
			];
		}
	}

	public function edit(Request $request)
	{
		try{
			$data = Point::find((int)$request->input('id'));
            $hasParent = $data->pluck('parent_id','parent_id')->all();
            $hisParent = $data->parent_id != 0 ? Point::select('question')->where('id',$data->parent_id)->first()->question : 'Sebagai Root Pertanyaan';
			return view($this->__route.'.form',[
				'actionform' => 'update',
                'pagetitle' => $this->pagetitle,
				'data' => $data,
                'hasParent' => $hasParent,
                'hisParent' => $hisParent,
                'hislevel' => explode(',',$data->level),
                'level' => LevelAssessment::get(),
				'parent' => json_encode($this->getparentmenu((int)$data->parent_id))
			]);
		}catch(Exception $e){}
	}	

	public function store(Request $request)
	{
		$result = [
			'flag' => 'error',
			'msg' => 'Error System',
			'title' => 'Error'
		];	

		$validator = $this->validateform($request);   
        // dd($request->all());
		if (!$validator->fails()) {
			$param['parent_id'] = (int)$request->input('parent_id');
			$param['question'] = !empty($request->input('question'))?$request->input('question'):'';
			$param['status'] = '1';
            $param['level'] = implode(',',$request->level);
            $parent = Point::where('parent_id', (int)$request->input('parent_id'))->orderBy('order', 'desc')->first();

			switch ($request->input('actionform')) {
				case 'insert': DB::beginTransaction();
                               try{
                               	  $param['order'] = (isset($parent)) ? ($parent->order + 1) : 1;
                               	  Point::create((array)$param);

                                  DB::commit();
                                  $result = [
                                    'flag'  => 'success',
                                    'msg' => 'Sukses tambah data',
                                    'title' => 'Sukses'
                                  ];
                               }catch(\Exception $e){
                                  DB::rollback();
                                  $result = [
                                    'flag'  => 'warning',
                                    'msg' => $e->getMessage(),
                                    'title' => 'Gagal'
                                  ];
                               }

				break;
				
				case 'update': DB::beginTransaction();
                               try{
                                Point::find((int)$request->input('id'))->update((array)$param);

                                  DB::commit();
                                  $result = [
                                    'flag'  => 'success',
                                    'msg' => 'Sukses ubah data',
                                    'title' => 'Sukses'
                                  ];
                               }catch(\Exception $e){
                                  DB::rollback();
                                  $result = [
                                    'flag'  => 'warning',
                                    'msg' => 'Gagal ubah data',
                                    'title' => 'Gagal'
                                  ];
                               }

				break;
			}

		}else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'warning',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];   			
		}

		return response()->json($result);			
	}	

	public function delete(Request $request)
	{
        DB::beginTransaction();
        try{
            Point::find((int)$request->input('id'))->delete();

            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal hapus data',
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);		
	}		

	public function submitchangestructure(Request $request)
	{
		$this->saveChangeMenu($request->input('serialized'));
		return response()->json(true);		
	}

	private function saveChangeMenu($children, $rootId = 0)
	{
		try{
			foreach($children as $key => $val){
				$parent = Point::where('id', $rootId)->first();

				Point::where('id', $val['id'])
				->update([
					'parent_id' => $rootId,
					'order' => $key
				]);
				if(isset($val['children'])){
					$this->saveChangeMenu($val['children'], $val['id']);
				}
			}
		}catch(Exception $e){}
    }	

	protected function validateform($request)
	{
        // $required['question'] = 'required|max:256';
        $message['question.required'] = 'wajib diinput';
        // $message['question.max'] = 'Pertanyaan maksimal 256 karakter';

        $message['parent_id.required'] = 'Parent wajib dipilih';

        return Validator::make($request->all(), $message); 		
	}


}