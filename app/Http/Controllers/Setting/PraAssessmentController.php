<?php


namespace App\Http\Controllers\Setting;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Mail\NotifikasiAssessment;
use App\Models\Assessment;
use App\Models\AssessmentHasAtasan;
use App\Models\AssessmentHasRekan;
use App\Models\AssessmentHasTim;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Models\Menu;
use App\Models\Pegawai;
use App\Models\Setting\Jenis;
use App\Models\Setting\Level;
use App\Models\Setting\LevelAssessment;
use App\Models\Setting\Periode;
use App\Models\Setting\PraAssessment;
use App\Models\User;
use Carbon\Carbon;
use DB;

 
class PraAssessmentController extends Controller
{
    protected $__route;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','setting.mapping_pra_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ','_',Menu::where('route_name','setting.mapping_pra_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ','_',Menu::where('route_name','setting.mapping_pra_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ','_',Menu::where('route_name','setting.mapping_pra_assessment.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));
        $this->__route = 'setting.mapping_pra_assessment';
        $this->pagetitle = 'Mapping Pra Assessment Pegawai';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {
        return view($this->__route.'.index',[
            'pagetitle' => 'Mapping Pra Assessment Pegawai',
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Setting',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
            'periode' => Periode::orderby('id', 'desc')->get(),
            'jenis' => Jenis::get(),
            'pegawai' => Pegawai::get(),
            'level_assessment' => LevelAssessment::get(),
        ]);

    }

    public function datatable(Request $request)
    {
        try{
            $data1 = DB::table('new_view_pra_assessment')->select('*')->orderby('id','desc');
            $data2 = DB::table('view_pra_assessment')->select('*')->union($data1)->orderby('id','desc');

            $result = DB::table(DB::raw("({$data2->toSql()}) as combined_result"))
                    ->mergeBindings($data2); // Merge bindings from the unionQuery
             return datatables()->of($result)
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('nama'))) {
                    $query->whereIn('pegawai_id',[$request->input('nama')])
                        ->orwhereIn('atasan_id',[$request->input('nama')])
                        ->orwhereIn('rekan_id',[$request->input('nama')])
                        ->orwhereIn('tim_id',[$request->input('nama')]);
                }

                if (!empty($request->input('periode_id'))) {
                    $query->where('periode_id',$request->input('periode_id'));
                }

                if (!empty($request->input('jenis_id'))) {
                    $query->where('jenis_id',$request->input('jenis_id'));
                }

                if (!empty($request->input('level_assessment_id'))) {
                    $query->where('level_id',$request->input('level_assessment_id'));
                }
                
            })
            ->editColumn('nama', function($row){
                return $row->nik.' - '.$row->nama;
            })

            ->editColumn('atasan', function($row){
                if($row->periode_id <= '4'){
                    $pegawai = Pegawai::whereIn('id', [$row->atasan_id])->get();
                    
                    $show = '';
                    foreach($pegawai as $key => $val){
                        $show .= '<span style="color:black;">&#x2022</span> '.$val->nama. '<br>';
                    }
                    $show .='';
                    return $show;
                }else{
                    $data = AssessmentHasAtasan::where('pra_assessment_id', $row->id)->pluck('atasan_id')->all();
                    $pegawai = Pegawai::whereIn('id', $data)->get();

                    $show = '';
                    foreach($pegawai as $key => $val){
                        $show .= '<span style="color:black;">&#x2022</span> '.$val->nama. '<br>';
                    }
                    $show .='';
                    return $show;
                }
            })

            ->editColumn('rekan', function($row){
                if($row->periode_id <= 4){
                    $pegawai = Pegawai::whereIn('id', [$row->rekan_id])->get();
                    $show = '';
                    foreach($pegawai as $key => $val){
                        $show .= '<span style="color:black;">&#x2022</span> '.$val->nama. '<br>';
                    }                    $show .='';
                    return $show;
                }else{
                    $data = AssessmentHasRekan::where('pra_assessment_id', $row->id)->pluck('rekan_id')->all();
                    $pegawai = Pegawai::whereIn('id', $data)->get();

                    $show = '';
                    foreach($pegawai as $key => $val){
                        $show .= '<span style="color:black;">&#x2022</span> '.$val->nama. '<br>';
                    }
                    $show .='';
                    return $show;
                }
            })

            ->editColumn('tim', function($row){
                if($row->periode_id <= 4){
                    $pegawai = Pegawai::where('id', [$row->tim_id])->get();
                    $show = '';
                    foreach($pegawai as $key => $val){
                        $show .= '<span style="color:black;">&#x2022</span> '.$val->nama. '<br>';
                    }                    $show .='';
                    return $show;
                }else{
                    $data = AssessmentHasTim::where('pra_assessment_id', $row->id)->pluck('tim_id')->all();
                    $pegawai = Pegawai::whereIn('id', $data)->get();

                    $show = '';
                    foreach($pegawai as $key => $val){
                        $show .= '<span style="color:black;">&#x2022</span> '.$val->nama. '<br>';
                    }
                    $show .='';
                    return $show;
                }
            })
            
            ->addColumn('action', function ($row){
                $periode = Periode::where('tgl_akhir','>=', Carbon::now()->format('Y-m-d'))->pluck('id')->all();
                $id = (int)$row->id;
                $button = '<div align="center">';
                if(in_array($row->periode_id,$periode)){
                    $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm btn-icon cls-button-sent" data-id="'.$row->id.'" title="Kirim Email">
                    <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Communication/Mail.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"/>
                        </g>
                    </svg><!--end::Svg Icon--></span>
                    </button>';
                    $button .= '&nbsp;';
                    $button .= '<button type="button" class="btn btn-light-warning w-30px h-30px btn-sm btn-icon cls-button-edit" data-id="'.$id.'" data-toggle="tooltip" title="Ubah data '.$row->nama.'">
                    <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                    <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"/>
                    <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"/>
                    <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"/>
                    </svg>
                    </span>
                    <!--end::Svg Icon-->
                    </button>';

                    $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm btn-icon cls-button-delete" data-id="'.$id.'" data-nama="'.$row->nama.'" data-toggle="tooltip" title="Hapus data '.$row->nama.'">
                    <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                    <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"/>
                    <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="currentColor"/>
                    <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="currentColor"/>
                    </svg>
                    </span>
                    <!--end::Svg Icon-->
                    </button>';
                }
                $button .= '</div>';
                return $button;
            })
            ->rawColumns(['action','atasan','rekan','tim'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $now = Carbon::now()->format('Y-m-d');
        return view($this->__route.'.create',[
            'pagetitle' => $this->pagetitle,
            'actionform' => 'insert',
            'pegawai' => Pegawai::whereNotNull('email')->get(),
            'levelAssessment' => LevelAssessment::get(),
            'periode' => Periode::where('tgl_akhir','>', $now)->get(),
            'jenisAssesment' => Jenis::get(),
        ]);
    }

    public function store(Request $request)
    {
        $result = [
            'flag' => 'error',
            'msg' => 'Error System',
            'title' => 'Error'
        ]; 

        $validator = $this->validateform($request);   

        if (!$validator->fails()) {
            $param['pegawai_id'] = $request->input('pegawai_id');
            $param['periode_id'] = $request->input('periode_id');
            $param['jenis_id'] = $request->input('jenis_id');
            $param['level_id'] = $request->input('level_id');
            
            switch ($request->input('actionform')) {
                case 'insert': DB::beginTransaction();
                    try{     
                        $PraAssessment = PraAssessment::create((array)$param);
                        
                        if($request->atasan_id){
                            foreach($request->atasan_id as $key => $value){
                                if($value != NULL){
                                    $atasan['pra_assessment_id'] = $PraAssessment->id;
                                    $atasan['atasan_id'] = $value;
                                    AssessmentHasAtasan::create($atasan);
                                }
                            }
                        }

                        if($request->rekan_id){
                            foreach($request->rekan_id as $key => $value){
                                if($value != NULL){
                                    $rekan['pra_assessment_id'] = $PraAssessment->id;
                                    $rekan['rekan_id'] = $value;
                                    AssessmentHasRekan::create($rekan);
                                }
                            }
                        }

                        if($request->tim_id){
                            foreach($request->tim_id as $key => $value){
                                if($value != NULL){
                                    $tim['pra_assessment_id'] = $PraAssessment->id;
                                    $tim['tim_id'] = $value;
                                    AssessmentHasTim::create($tim);
                                }
                            }
                        }

                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses tambah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;
                
                case 'update': DB::beginTransaction();
                    try{
                        $PraAssessment = PraAssessment::find((int)$request->input('id'));
                        $assessment = Assessment::where('praassessment_id',(int)$request->input('id'))->where('periode_id', $PraAssessment->periode_id)->pluck('penilai_id')->all();
                        
                        if($PraAssessment->pegawai_id != $request->pegawai_id){
                            $assessment  = Assessment::where('praassessment_id',(int)$request->input('id'))
                            ->where('penilai_id', $PraAssessment->pegawai_id)
                            ->where('periode_id', $PraAssessment->periode_id)->first();
                            if($assessment){
                                DB::rollback();
                                $result = [
                                    'flag'  => 'warning',
                                    'msg' => 'tidak dapat diubah, pegawai sudah mengisi assessment',
                                    'title' => 'Gagal'
                                ];
                            return response()->json($result);
                            }
                        }
                        if($request->periode_id <= 4){
                            if($PraAssessment->atasan_id != $request->atasan_id){
                                $assessment  = Assessment::where('praassessment_id',(int)$request->input('id'))
                                ->where('penilai_id', $PraAssessment->atasan_id)
                                ->where('periode_id', $PraAssessment->periode_id)->first();
                                if($assessment){
                                    DB::rollback();
                                    $result = [
                                        'flag'  => 'warning',
                                        'msg' => 'tidak dapat diubah, pegawai sudah mengisi assessment',
                                        'title' => 'Gagal'
                                    ];
                                return response()->json($result);
                                }
                            }
                            if($PraAssessment->rekan_id != $request->rekan_id){
                                $assessment  = Assessment::where('praassessment_id',(int)$request->input('id'))
                                ->where('penilai_id', $PraAssessment->rekan_id)
                                ->where('periode_id', $PraAssessment->periode_id)->first();
                                if($assessment){
                                    DB::rollback();
                                    $result = [
                                        'flag'  => 'warning',
                                        'msg' => 'tidak dapat diubah, pegawai sudah mengisi assessment',
                                        'title' => 'Gagal'
                                    ];
                                return response()->json($result);
                                }
                            }
                            if($PraAssessment->tim_id != $request->tim_id){
                                $assessment  = Assessment::where('praassessment_id',(int)$request->input('id'))
                                ->where('penilai_id', $PraAssessment->tim_id)
                                ->where('periode_id', $PraAssessment->periode_id)->first();
                                if($assessment){
                                    DB::rollback();
                                    $result = [
                                        'flag'  => 'warning',
                                        'msg' => 'tidak dapat diubah, pegawai sudah mengisi assessment',
                                        'title' => 'Gagal'
                                    ];
                                return response()->json($result);
                                }
                            }
                            $param['atasan_id'] = $request->atasan_id;
                            $param['rekan_id'] = $request->rekan_id;
                            $param['tim_id'] = $request->tim_id;
                        }else{
                            if($request->atasan_id){
                                $arrayExistAtasan = AssessmentHasAtasan::where('pra_assessment_id',(int)$request->input('id'))->pluck('atasan_id','atasan_id')->all();
                                foreach($request->atasan_id as $key => $valueAtasan){
                                    $atasan['pra_assessment_id'] = (int)$request->input('id');
                                    $atasan['atasan_id'] = $valueAtasan;
                                    if (array_key_exists($valueAtasan, $arrayExistAtasan)) {
                                        AssessmentHasAtasan::where('pra_assessment_id',(int)$request->input('id'))->where('atasan_id', $valueAtasan)->update($atasan);
                                    } else {
                                        AssessmentHasAtasan::create($atasan); 
                                    }

                                    $atasanIdDelete = array_diff(array_keys($arrayExistAtasan), $request->atasan_id);
                                    foreach($atasanIdDelete as $key => $valueAtasanIdDelete){
                                        $checkSubmitAssessmentAtasan = Assessment::where('praassessment_id',(int)$request->input('id'))->where('penilai_id', $valueAtasanIdDelete)->where('periode_id', $PraAssessment->periode_id)->first();
                                        if(!$checkSubmitAssessmentAtasan){
                                            AssessmentHasAtasan::where('pra_assessment_id',(int)$request->input('id'))->where('atasan_id', $valueAtasanIdDelete)->delete();
                                        }
                                    }
                                }
                            }
                            if($request->rekan_id){
                                $arrayExistRekan = AssessmentHasRekan::where('pra_assessment_id',(int)$request->input('id'))->pluck('rekan_id','rekan_id')->all();
                                foreach($request->rekan_id as $key => $valueRekan){
                                    $rekan['pra_assessment_id'] = (int)$request->input('id');
                                    $rekan['rekan_id'] = $valueRekan;
                                    if (array_key_exists($valueRekan, $arrayExistRekan)) {
                                        AssessmentHasRekan::where('pra_assessment_id',(int)$request->input('id'))->where('rekan_id', $valueRekan)->update($rekan);
                                    } else {
                                        AssessmentHasRekan::create($rekan); 
                                    }

                                    $rekanIdDelete = array_diff(array_keys($arrayExistRekan), $request->rekan_id);
                                    foreach($rekanIdDelete as $key => $valueRekanIdDelete){
                                        $checkSubmitAssessmentRekan = Assessment::where('praassessment_id',(int)$request->input('id'))->where('penilai_id', $valueRekanIdDelete)->where('periode_id', $PraAssessment->periode_id)->first();
                                        if(!$checkSubmitAssessmentRekan){
                                            AssessmentHasRekan::where('pra_assessment_id',(int)$request->input('id'))->where('rekan_id', $valueRekanIdDelete)->delete();
                                        }
                                    }
                                }
                            }   
                           if($request->tim_id){
                                $arrayExistTim = AssessmentHasTim::where('pra_assessment_id',(int)$request->input('id'))->pluck('tim_id','tim_id')->all();
                                foreach($request->tim_id as $key => $valueTim){
                                    $tim['pra_assessment_id'] = (int)$request->input('id');
                                    $tim['tim_id'] = $valueTim;
                                    if (array_key_exists($valueTim, $arrayExistTim)) {
                                        AssessmentHasTim::where('pra_assessment_id',(int)$request->input('id'))->where('tim_id', $valueTim)->update($tim);
                                    } else {
                                        AssessmentHasTim::create($tim); 
                                    }

                                    $timIdDelete = array_diff(array_keys($arrayExistTim), $request->tim_id);
                                    foreach($timIdDelete as $key => $valueTimIdDelete){
                                        $checkSubmitAssessmentTim = Assessment::where('praassessment_id',(int)$request->input('id'))->where('penilai_id', $valueTimIdDelete)->where('periode_id', $PraAssessment->periode_id)->first();
                                        if(!$checkSubmitAssessmentTim){
                                            AssessmentHasTim::where('pra_assessment_id',(int)$request->input('id'))->where('tim_id', $valueTimIdDelete)->delete();
                                        }
                                    }
                                }
                            }
                        }
                    
                        $PraAssessment->update((array)$param);

                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses ubah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;
            }
        }else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'warning',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];                      
        }

        return response()->json($result);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {

        try{
            $data = PraAssessment::find((int)$request->input('id'));
            return view($this->__route.'.edit',[
                'actionform' => 'update',
                'pagetitle' => $this->pagetitle,
                'data' => $data,
                'pegawai' => Pegawai::whereNotNull('email')->get(),
                'levelAssessment' => LevelAssessment::get(),
                'jenisAssesment' => Jenis::get(),
                'periode' => Periode::get(),
                'AssessmentHasAtasan' => AssessmentHasAtasan::where('pra_assessment_id',(int)$request->input('id'))->get(),
                'AssessmentHasRekan' => AssessmentHasRekan::where('pra_assessment_id',(int)$request->input('id'))->get(),
                'AssessmentHasTim' => AssessmentHasTim::where('pra_assessment_id',(int)$request->input('id'))->get(),
            ]);
        }catch(Exception $e){}

    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try{
            $data = PraAssessment::where('id',(int)$request->input('id'))->delete();
            $assessmentHasAtasan = AssessmentHasAtasan::where('pra_assessment_id', (int)$request->input('id'))->delete();
            $assessmentHasTim = AssessmentHasTim::where('pra_assessment_id', (int)$request->input('id'))->delete();
            $assessmentHasRekan = AssessmentHasRekan::where('pra_assessment_id', (int)$request->input('id'))->delete();
            
            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal hapus data',
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);       
    }

    public function SentMail(Request $request){
        try{
            $data1 = DB::table('new_view_pra_assessment')->where('id', $request->id);
            $data2 = DB::table('view_pra_assessment')->union($data1)->where('id', $request->id);
            $masterData = $data2->first();
            //$PraAssessment = PraAssessment::where('id', $request->id)->first();
            $atasan_id = explode(',', $masterData->atasan_id);
            $rekan_id = explode(',', $masterData->rekan_id);
            $tim_id = explode(',', $masterData->tim_id);
            $pegawai = User::Where(function ($query) use($masterData,$atasan_id,$rekan_id,$tim_id) {
                $query->orwhereIn('pegawai_id', [$masterData->pegawai_id])
                      ->orWhereIn('pegawai_id', $atasan_id)
                      ->orWhereIn('pegawai_id', $rekan_id)
                      ->orWhereIn('pegawai_id', $tim_id);
            })->get();
            $data['jenis'] = Jenis::where('id',$masterData->jenis_id)->pluck('nama')->first();
            $data['periode'] = Periode::where('id', $masterData->periode_id)->first();
            $data['pegawai'] = Pegawai::where('id',$masterData->pegawai_id)->first();
            $data['akhir'] = GeneralController::tglFormat($data['periode']->tgl_akhir,2);

            foreach($pegawai as $key => $value){
                $to = $value->email;
                $data['user'] = $value;
                $data['url'] = route('assessment.inputassessment',['id' => $masterData->id]);
                $success = Mail::to($to)->send(new NotifikasiAssessment($data));
                // $success = Mail::to('mohamad.adi@eatwell.co.id')->queue(new NotifikasiAssessment($data));
            }
            if($success){
                $result = [
                    'flag'  => 'success',
                    'msg' => 'Sukses Mengirim Email',
                    'title' => 'Sukses'
                ];
            }else{
                $result = [
                    'flag'  => 'warning',
                    'msg' => 'Gagal Mengirim Email',
                    'title' => 'Gagal'
                ];
            }
        }catch(\Exception $e){
            $result = [
                'flag'  => 'warning',
                'msg' => 'Gagal Mengirim Email',
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);
    }

    protected function validateform($request)
    {
        $required['pegawai_id'] = 'required';
        $required['atasan_id'] = 'required';
        $required['rekan_id'] = 'required';
        $required['periode_id'] = 'required';
        $required['level_id'] = 'required';
        $required['jenis_id'] = 'required';
        //$required['guard_name'] = 'required';

        $message['pegawai_id.required'] = 'Pegawai Wajib Di Pilih';
        $message['atasan_id.required'] = 'Atasan Wajib Di Pilih';
        $message['rekan_id.required'] = 'Rekan Wajib Di Pilih';
        $message['periode_id.required'] = 'Periode Assessment Wajib Di Pilih';
        $message['level_id.required'] = 'Level Assessment Wajib Di Pilih';
        $message['jenis_id.required'] = 'Jenis Assessment Wajib Di Pilih';

        //s$message['guard_name.required'] = 'guard name wajib dipilih';

        return Validator::make($request->all(), $required, $message);       
    }


}