<?php

namespace App\Http\Controllers\Manajemen;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Config;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Datatables;

use App\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Menu;

class RoleController extends Controller
{

    public function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.role.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
        $create = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.role.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
        $edit = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.role.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
        $delete = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.role.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));

        $this->__pagetype = 'manajemen';
		$this->__route = 'role';
        $this->pagetitle = 'Role';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }

    public function index()
    {
        $role = Role::get();
        $main_menu = DB::table('roles')->select('*')
                    ->leftjoin('role_menu','role_menu.role_id','roles.id')
                    ->leftjoin('menus','menus.id','role_menu.menu_id')
                    ->where('menus.parent_id', '=' ,0)
                    ->get();                    
        $role_parent_menu = DB::table('roles')->select('*')
                    ->leftjoin('role_menu','role_menu.role_id','roles.id')
                    ->leftjoin('menus','menus.id','role_menu.menu_id')->orderby('menus.id','asc')
                    //->where('menus.parent_id', '>' ,0)
                    ->get();                    

        $role_permission = DB::table('roles')->select('*')
                    ->leftjoin('role_has_permissions','role_has_permissions.role_id','roles.id')
                    ->leftjoin('permissions','permissions.id','role_has_permissions.permission_id')
                    ->get();                        

        return view($this->__pagetype.'.'.$this->__route.'.index',[
            'pagetitle' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Management User'
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle
                ],
            ],
            'parent_menu' =>$role_parent_menu,
            'role' => $role
        ]);
    }
    public function datatable(Request $request)
    {
        try{
            return datatables()->of(Role::query())
            ->addColumn('action', function ($row){
                $id = (int)$row->id;
                $button = '<div align="center">';

                $button .= '<button type="button" class="btn btn-sm btn-outline btn-outline-flush btn-outline-info btn-active-light-info btn-icon cls-button-show" data-id="'.$id.'" data-toggle="tooltip" title="Lihat data '.$row->name.'"><i class="bi bi-eye fs-3 text-info"></i></button>';

                $button .= '&nbsp;';
                $button .= '<button type="button" class="btn btn-sm btn-outline btn-outline-flush btn-outline-success btn-active-light-success btn-icon cls-button-edit" data-id="'.$id.'" data-toggle="tooltip" title="Ubah data '.$row->name.'"><i class="bi bi-pencil fs-3 text-success"></i></button>';

                $button .= '&nbsp;';

                $button .= '<button type="button" class="btn btn-sm btn-outline btn-outline-flush btn-outline-danger btn-active-light-danger btn-icon cls-button-delete" data-id="'.$id.'" data-nama="'.$row->name.'" data-toggle="tooltip" title="Hapus data '.$row->name.'"><i class="bi bi-trash fs-3 text-danger"></i></button>';
                $button .= '</div>';
                return $button;
            })
            ->rawColumns(['nama','keterangan','action'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $permission = Permission::get();
        $menu = Menu::orderby('order','asc')->get();
        $dataCollection = new Collection;  
        foreach ($menu->where('parent_id',0) as $key => $induk) {
            $dataCollection->push($induk);
            $prevInduk = null;
            foreach ($menu->where('parent_id',$induk->id) as $key => $sub1) {
                if ($prevInduk === $sub1->id) {
                    continue;
                }
                $dataCollection->push($sub1);
                $prevSub1 = null;
                
                foreach ($menu->where('parent_id',$sub1->id) as $key => $sub2) {
                    if ($prevSub1 === $sub2->id) {
                        continue;
                    }
                    $dataCollection->push($sub2);
                    $prevSub2 = null;
                    foreach ($menu->where('parent_id',$sub2->id) as $key => $sub3) {
                        if ($prevSub2 === $sub3->id) {
                            continue;
                        }
                        $dataCollection->push($sub3);
                        $prevSub3 = null;
                        foreach ($menu->where('parent_id',$sub3->id) as $key => $sub4) {
                            if ($prevSub3 === $sub4->id) {
                                continue;
                            }
                            $dataCollection->push($sub4);
                            $prevSub4 = null;
                            foreach ($menu->where('parent_id',$sub4->id) as $key => $sub5) {
                                if ($prevSub4 === $sub4->id) {
                                    continue;
                                }
                                $prevSub4 = $sub5->id;
                            }
                            
                            $prevSub3 = $sub4->id;
                        }
                        $prevSub2 = $sub3->id;
                    }
                    $prevSub1 = $sub2->id;
                }
                $prevInduk = $sub1->id;
            }
        }
        $main_menu = DB::table('menus')->select('*')
        ->where('menus.parent_id', '=' ,0)
        ->get();                    

        return view($this->__pagetype.'.'.$this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Management User'
                ],
                [
                    'url' => 'manajemen/role',
                    'menu' => $this->pagetitle
                ],
                [
                    'url' => '',
                    'menu' => 'Create'
                ],
            ],
            'actionform' => 'insert',
            'permission' => $permission,
            'role' => [],
            'menu' => $dataCollection,
            'main_menu' => $main_menu
        ]);

    }


    public function store(Request $request)
    {
        
        $result = [
            'flag' => 'error',
            'msg' => 'Error System',
            'title' => 'Error'
        ];

        switch ($request->input('actionform')) {
            case 'insert': 
                    $param['name'] = $request->input('name');
                    $permission = $request->input('permission');            
                    $arr_menu = [];
                    $menu = [];

                    if($request->input('permission')){
                        foreach($request->input('permission') as $menus){

                            $listmenu = explode(",",$menus);
                            if($listmenu[0]){
                                $arr_menu[] = (int)$listmenu[0];
                            }
                        }

                        $menu = array_unique($arr_menu);    
                    }

                    $param['guard_name'] = 'web';
                    $role = Role::create((array)$param);
                    //   $role->syncPermissions($request->input('permission'));
                    $role->menus()->sync($menu);

                    $getrolename = [];
                    if($permission){
                        foreach($permission as $item){

                            $listitem = explode(",",$item);
                            $getrolename[] = [
                                            'permission_id' =>(int)$listitem[1],
                                            'menu_id' =>(int)$listitem[0],
                                            'role_id' => (int)$role->id,
                                            'label'=> strtolower(str_replace(' ','_',Menu::where('id',(int)$listitem[0])->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('id',(int)$listitem[1])->pluck('name')->first())),
                                            // 'tag'=> strtolower(str_replace(' ','_',Menu::where('id',(int)$listitem[0])->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('id',(int)$listitem[1])->pluck('name')->first()))
                                        ];
                        }    
                        
                        DB::table('role_has_permissions')->insert($getrolename);
                    }

                return redirect()->route('manajemen.role.index');

            break;
                
            case 'update':   
                    $param['name'] = $request->input('name');
                    $permission = $request->input('permission');            
                    $arr_menu = [];
                    $menu = [];
                    //update role 
                    $role_exists = Role::find((int)$request->role_id);
                    if($role_exists){
                        $role_exists->update([
                            'name'=>$request->name
                        ]);
                    }

                    $permission_exists = DB::table('role_has_permissions')->where('role_id',(int)$request->role_id);

                    if(!empty($permission_exists->get())){
                        $permission_exists = $permission_exists->delete();
                    }   

                    if($request->input('permission')){
                        foreach($request->input('permission') as $menus){
                            $listmenu = explode(",",$menus);
                            if($listmenu[0]){
                                $arr_menu[] = (int)$listmenu[0];
                            }
                        }
                        $menu = array_unique($arr_menu);    
                    }

                    $role_menus = DB::table('role_menu')->where('role_id',(int)$request->role_id);

                    if(!empty($role_menus->get())){
                        $role_menus = $role_menus->delete();
                    }
                    $role_exists->menus()->sync($menu);

                    $getrolename = [];
                    if($permission){
                        foreach($permission as $item){

                            $listitem = explode(",",$item);
                            $getrolename[] = [
                                            'permission_id' =>(int)$listitem[1],
                                            'menu_id' =>(int)$listitem[0],
                                            'role_id' => (int)$role_exists->id,
                                            'label'=> strtolower(str_replace(' ','_',Menu::where('id',(int)$listitem[0])->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('id',(int)$listitem[1])->pluck('name')->first()))
                                        ];

                        }                        
                        DB::table('role_has_permissions')->insert($getrolename);
                    }

                return redirect()->route('manajemen.role.index');
            break;      
        }
    }

    public function edit($id)
    {
        $role = Role::find((int)decrypt($id));
        if($role){
            $role_permission = DB::table('role_has_permissions')->where('role_id',$role->id)->get();
            $arr_role_per = [];
            foreach($role_permission as $k=>$r){
                array_push($arr_role_per, $r->menu_id.','.$r->permission_id);
            }
        }

        $permission = Permission::get();
    
        $menu = Menu::orderby('order','asc')->get();
        $dataCollection = new Collection;  
        foreach ($menu->where('parent_id',0) as $key => $induk) {
            $dataCollection->push($induk);
            $prevInduk = null;
            foreach ($menu->where('parent_id',$induk->id) as $key => $sub1) {
                if ($prevInduk === $sub1->id) {
                    continue;
                }
                $dataCollection->push($sub1);
                $prevSub1 = null;
                
                foreach ($menu->where('parent_id',$sub1->id) as $key => $sub2) {
                    if ($prevSub1 === $sub2->id) {
                        continue;
                    }
                    $dataCollection->push($sub2);
                    $prevSub2 = null;
                    foreach ($menu->where('parent_id',$sub2->id) as $key => $sub3) {
                        if ($prevSub2 === $sub3->id) {
                            continue;
                        }
                        $dataCollection->push($sub3);
                        $prevSub3 = null;
                        foreach ($menu->where('parent_id',$sub3->id) as $key => $sub4) {
                            if ($prevSub3 === $sub4->id) {
                                continue;
                            }
                            $dataCollection->push($sub4);
                            $prevSub4 = null;
                            foreach ($menu->where('parent_id',$sub4->id) as $key => $sub5) {
                                if ($prevSub4 === $sub4->id) {
                                    continue;
                                }
                                $prevSub4 = $sub5->id;
                            }
                            
                            $prevSub3 = $sub4->id;
                        }
                        $prevSub2 = $sub3->id;
                    }
                    $prevSub1 = $sub2->id;
                }
                $prevInduk = $sub1->id;
            }
        }

        $main_menu = DB::table('menus')->select('*')
        ->where('menus.parent_id', '=' ,0)
        ->get();    

        return view($this->__pagetype.'.'.$this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Management User'
                ],
                [
                    'url' => '/manajemen/role',
                    'menu' => $this->pagetitle
                ],
                [
                    'url' => '',
                    'menu' => 'Edit'
                ],
            ],
            'actionform' => 'update',
            'permission' => $permission,
            'menu' => $dataCollection,
            'role' => $role,
            'role_permission' => $role_permission? $role_permission : [],
            'arr_role_per'=>$arr_role_per
        ]);

    }


    public function detail($id)
    {
        $role = Role::find((int)decrypt($id));
        
        $main_menu = DB::table('menus')->select('*')
                    ->where('menus.parent_id', '=' ,0)
                    ->get();                    
        $role_parent_menu = DB::table('roles')->select('*')
                    ->leftjoin('role_menu','role_menu.role_id','roles.id')
                    ->leftjoin('menus','menus.id','role_menu.menu_id')
                    ->where('menus.parent_id', '>' ,0)
                    ->where('roles.id', (int)decrypt($id))
                    ->get();                    

        $role_permission = DB::table('roles')->select('*')
                    ->leftjoin('role_has_permissions','role_has_permissions.role_id','roles.id')
                    ->leftjoin('permissions','permissions.id','role_has_permissions.permission_id')
                    ->where('roles.id', (int)decrypt($id))
                    ->get();  
                    
        $role_user = Role::select('users.*')
                    ->leftjoin('model_has_roles','model_has_roles.role_id','roles.id')
                    ->leftjoin('users','model_has_roles.model_id','users.id')
                    ->where('roles.id',(int)decrypt($id))
                    ->get();


        return view($this->__pagetype.'.'.$this->__route.'.detail',[
            'pagetitle' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'Management User'
                ],
                [
                    'url' => '/manajemen/role',
                    'menu' => $this->pagetitle
                ],
                [
                    'url' => '',
                    'menu' => 'Detail'
                ],
            ],
            'parent_menu' =>$role_parent_menu,
            'roles' => $role,
            'role_user' => $role_user
        ]);

    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try{
                $data = Role::find((int)decrypt($request->id));

                if($data){
                    $rolehasmenu = DB::table('role_menu')->where('role_id',(int)$data->id);
                    if(!empty($rolehasmenu->get())){
                        $rolehasmenu = $rolehasmenu->delete();
                    }
                    $rolehaspermission = DB::table('role_has_permissions')->where('role_id',(int)$data->id);
                    if(!empty($rolehaspermission->get())){
                        $rolehaspermission = $rolehaspermission->delete();
                    }
                    $data->delete();
                }
                DB::commit();
                $result = [
                    'flag'  => 'success',
                    'msg' => 'Sukses hapus data',
                    'title' => 'Sukses'
                ];

            }catch(\Exception $e){
                DB::rollback();
                $result = [
                    'flag'  => 'warning',
                    'msg' => 'Gagal hapus data',
                    'title' => 'Gagal'
                ];
            }
            return response()->json($result);  
    }

    protected function validateform($request)
    {
        $required['name'] = 'required';

        $message['name.required'] = 'Nama Role wajib diinput';

        return Validator::make($request->all(), $required, $message);
    }
    
    public function gettreemenubyrole($id=null)
    {
      try{
        $result = $this->getarrayrolebymenu((int)$id);
        return response()->json($result);
      }catch(Exception $e){
        return response()->json([]);
      }
    }

    private function getarrayrolebymenu($id)
    {
      $data = Menu::where('status',true)->orderBy('order')->get();
      $menurole = [];
      if((bool)$id){
        //jika id ada artinya ini bagian edit lakukan pengambilan data referensi
        $row = Role::find($id);
        $menurole = $row->menus()->get()->pluck('id')->toArray();
      }
      return $this->recursivemenu($data, 0, $menurole);
    }

    private function recursivemenu($data, $parent_id, $menurole)
    {
      $array = [];
        $result = $data->where('parent_id', (int)$parent_id)->sortBy('order');
        foreach ($result as $val) {
          $child = $data->where('parent_id', (int)$val->id)->sortBy('order');

          $array[] = [
            'id' => (int)$val->id,
            'text' => $val->label,
            'state' => [
              'opened' => (bool)$child->count()? true : false,
              'selected' => $val->id == 1? true : ((bool)count($menurole)? (in_array($val->id, $menurole)? true : false) : false),
              'disabled' => $val->id == 1? true : false
            ],
            'children' => (bool)$child->count()? $this->recursivemenu($data, (int)$val->id, $menurole) : []
          ];
        }
        return $array;    
    }
}
