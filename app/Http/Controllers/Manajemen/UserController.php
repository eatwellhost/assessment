<?php

namespace App\Http\Controllers\Manajemen;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Config;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Datatables;

use App\Models\User;
use App\Models\KlasterIndustri;
use Spatie\Permission\Models\Permission;
use App\Models\Menu;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Models\Master\Brand;
use App\Models\Master\Outlet;
use App\Models\Master\Wilayah\Area;
use App\Models\Pegawai;
use App\Models\UserHasOutlet;
use App\Models\UserHasArea;
use App\Models\UserHasAm;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $view = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.user.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','view')->pluck('name')->first()));
		$create = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.user.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','create')->pluck('name')->first()));
		$edit = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.user.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','edit')->pluck('name')->first()));
		$delete = strtolower(str_replace(' ','_',Menu::where('route_name','manajemen.user.index')->pluck('label')->first())).':'.strtolower(str_replace(' ','_',Permission::where('name','delete')->pluck('name')->first()));
        $this->__route = 'manajemen.user';
        $this->pagetitle = 'User';
        $this->middleware('rolehaspermission:'.$view);
        $this->middleware('rolehaspermission:'.$create, ['only' => ['create','store']]);
        $this->middleware('rolehaspermission:'.$edit, ['only' => ['edit','update']]);
        $this->middleware('rolehaspermission:'.$delete, ['only' => ['delete']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view($this->__route.'.index',[
            'pagetitle' => $this->pagetitle,
            'breadcrumb' => $this->pagetitle,
            'breadcrumb_arr' => [
                [
                    'url' => '#',
                    'menu' => 'User Management',
                ],
                [
                    'url' => '',
                    'menu' => $this->pagetitle,
                ],
            ],
        ]);
    }

    
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        try{
            return datatables()->of(User::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('name'))) {
                    $query->where('name', 'like', "%{$request->input('name')}%");
                }
                if (!empty($request->input('email'))) {
                    $query->where('email', 'like', "%{$request->input('email')}%");
                }
                // if (!empty($request->input('role'))) {
                //     $key = $request->input('role');
                //     $query->whereHas('roles',function($query) use($key){
                //         $query->where('name','like',"%{$key}%");
                //     });
                // }
            })->addColumn('aktif',function ($row){
                $badge = '';
                if($row->password_changed_at != null){
                    $badge='<span class="badge badge-primary mr-2">Active</span>';
                }else{
                    $badge='<span class="badge badge-warning mr-2">InActive</span>';
                }
                return $badge;
            })
            ->addColumn('action', function ($row){
                $id = (int)$row->id;
                $button = '<div align="center">';

                $button .= '<button type="button" class="btn btn-light-warning w-30px h-30px btn-sm btn-icon cls-button-edit" data-id="'.$id.'" data-toggle="tooltip" title="Ubah data '.$row->name.'">
                <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2023-03-24-172858/core/html/src/media/icons/duotune/general/gen055.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"/>
                <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"/>
                <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"/>
                </svg>
                </span>
                <!--end::Svg Icon-->
                </button>';

                $button .= '&nbsp;';

                $button .= '<button type="button" class="btn btn-light-danger w-30px h-30px btn-sm  btn-icon cls-button-delete" data-id="'.$id.'" data-nama="'.$row->name.'" data-toggle="tooltip" title="Hapus data '.$row->name.'">
                <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-2"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"/>
                <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="currentColor"/>
                <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="currentColor"/>
                </svg>
                </span>
                <!--end::Svg Icon-->
                </button>';

                if($row->password_changed_at != null){
                    $button .= '&nbsp;';

                $button .= '<button type="button" class="btn btn-light-success w-30px h-30px btn-sm  btn-icon cls-button-reset" data-id="'.$id.'" data-nama="'.$row->name.'" data-toggle="tooltip" title="Reset Password '.$row->name.'">
                    <!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen034.svg-->
                    <span class="svg-icon svg-icon-muted svg-icon-2">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="0" y="0" width="24" height="24"></rect>
                    <path d="M12,8 L8,8 C5.790861,8 4,9.790861 4,12 L4,13 C4,14.6568542 5.34314575,16 7,16 L7,18 C4.23857625,18 2,15.7614237 2,13 L2,12 C2,8.6862915 4.6862915,6 8,6 L12,6 L12,4.72799742 C12,4.62015048 12.0348702,4.51519416 12.0994077,4.42878885 C12.264656,4.2075478 12.5779675,4.16215674 12.7992086,4.32740507 L15.656242,6.46136716 C15.6951359,6.49041758 15.7295917,6.52497737 15.7585249,6.56395854 C15.9231063,6.78569617 15.876772,7.09886961 15.6550344,7.263451 L12.798001,9.3840407 C12.7118152,9.44801079 12.607332,9.48254921 12.5,9.48254921 C12.2238576,9.48254921 12,9.25869158 12,8.98254921 L12,8 Z" fill="#000000"></path>
                    <path d="M12.0583175,16 L16,16 C18.209139,16 20,14.209139 20,12 L20,11 C20,9.34314575 18.6568542,8 17,8 L17,6 C19.7614237,6 22,8.23857625 22,11 L22,12 C22,15.3137085 19.3137085,18 16,18 L12.0583175,18 L12.0583175,18.9825492 C12.0583175,19.2586916 11.8344599,19.4825492 11.5583175,19.4825492 C11.4509855,19.4825492 11.3465023,19.4480108 11.2603165,19.3840407 L8.40328311,17.263451 C8.18154548,17.0988696 8.13521119,16.7856962 8.29979258,16.5639585 C8.32872576,16.5249774 8.36318164,16.4904176 8.40207551,16.4613672 L11.2591089,14.3274051 C11.48035,14.1621567 11.7936615,14.2075478 11.9589099,14.4287888 C12.0234473,14.5151942 12.0583175,14.6201505 12.0583175,14.7279974 L12.0583175,16 Z" fill="#000000" opacity="0.3"></path>
                    </svg>
                    </span>
                    <!--end::Svg Icon-->
                    </button>';
                }

                $button .= '</div>';
                return $button;
            })
            
            ->editColumn('roles', function ($row){
                $data = '';
                if(!empty($row->getRoleNames())){
                    foreach ($row->getRoleNames() as $v) {
                        $data .= '<span class="badge badge-primary mr-2">'.$v.'</span>';
                        $data .= '&nbsp;';
                    }
                }
                return $data;
            })
            ->rawColumns(['nama','action','roles','aktif'])
            ->toJson();
        }catch(Exception $e){
            return response([
                'draw'            => 0,
                'recordsTotal'    => 0,
                'recordsFiltered' => 0,
                'data'            => []
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $user = User::get();
        
        return view($this->__route.'.form',[
            'pagetitle' => $this->pagetitle,
            'actionform' => 'insert',
            'role' => Role::get(),
            'User' => $user,
            'pegawai'=> Pegawai::get(),
            'random_password' => 'E4tw3ll!',
        ]);
    }

    public function reset(Request $request)
    {
        $user = User::find((int)$request->input('id'));;
        return view($this->__route.'.reset',[
            'pagetitle' => $this->pagetitle,
            'actionform' => 'reset',
            'data' => $user,
            'pegawai'=> Pegawai::get(),
            'random_password' => 'E4tw3ll!',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $result = [
            'flag' => 'error',
            'msg' => 'Error System',
            'title' => 'Error'
        ];

        $validator = $this->validateform($request);
        if (!$validator->fails()) {
            $param = $request->except('actionform','id','roles','outlet_id');
            switch ($request->input('actionform')) {
                case 'insert': DB::beginTransaction();
                    try{
                        $userExist = User::where('username',$request->username)->first();
                        if ($userExist) {
                            $result = [
                                'flag'  => 'warning',
                                'msg' => 'Username telah terdaftar !',
                                'title' => 'Gagal'
                                ];
                                return response()->json($result);
                        }
                        $param['password'] = Hash::make($request->random_password);
                        $param['old_password'] = $request->random_password;
                        $param['email_verified_at'] = null;
                        $param['remember_token'] = Str::random(64);
                        $user = User::create((array)$param);
                        $user->assignRole($request->input('roles'));
                        

                        DB::commit();
                        $result = [
                            'flag'  => 'success',
                            'msg' => 'Sukses tambah data',
                            'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;

                case 'update': DB::beginTransaction();
                    try{
                        $user = User::find((int)$request->input('id'));
                        if($user->password_changed_at != null){
                            $param['random_password'] = 'change';
                            $param['old_password'] = $request->random_password;
                        }
                        $user->update((array)$param);
                        DB::table('model_has_roles')->where('model_id',(int)$request->input('id'))->delete();
                        $user->assignRole($request->input('roles'));

                        

                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses ubah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;

                case 'reset': DB::beginTransaction();
                    try{
                        $user = User::find((int)$request->input('id'));
                        $param['password'] = Hash::make($request->random_password);
                        $param['old_password'] = $request->random_password;
                        $param['email_verified_at'] = null;
                        $param['password_changed_at'] = null;
                        $param['remember_token'] = Str::random(64);
                        $user->update((array)$param);
                        
                        DB::commit();
                        $result = [
                        'flag'  => 'success',
                        'msg' => 'Sukses ubah data',
                        'title' => 'Sukses'
                        ];
                    }catch(\Exception $e){
                        DB::rollback();
                        $result = [
                        'flag'  => 'warning',
                        'msg' => $e->getMessage(),
                        'title' => 'Gagal'
                        ];
                    }

                break;
            }
        }else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'warning',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];
        }

        return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {

        try{

            $user = User::find((int)$request->input('id'));
            $userRole = $user->roles->pluck('name','name')->all();
            
            $data = ([
            'pagetitle' => $this->pagetitle,
            'actionform' => 'update',
            'data' => $user,
            'userRole' => $userRole,
            'pegawai' => Pegawai::get(),
            'role' => Role::get(),
            'user' => $user,
            'random_password' => null,
            ]);
                return view($this->__route.'.form',$data);
        }catch(Exception $e){}

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        DB::beginTransaction();
        try{
            $user = User::find((int)$request->input('id'));

            $user->roles()->detach();
            $user->delete();

            DB::commit();
            $result = [
                'flag'  => 'success',
                'msg' => 'Sukses hapus data',
                'title' => 'Sukses'
            ];
        }catch(\Exception $e){
            DB::rollback();
            $result = [
                'flag'  => 'warning',
                'msg' => $e->getMessage(),
                'title' => 'Gagal'
            ];
        }
        return response()->json($result);
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateform($request)
    {
        $required['name'] = 'required';

        $message['name.required'] = 'Nama User wajib diinput';

        return Validator::make($request->all(), $required, $message);
    }

    public function getString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
     
        for ($i = 0; $i < 10; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
     
        return $randomString;
    }
}
