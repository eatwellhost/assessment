<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentHasAtasan extends Model
{
    protected $table = 'pra_assessment_has_atasans';
    protected $guarded = [];
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Master\Brand', 'brand_id');
    }

    public function am()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}

