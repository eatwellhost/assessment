<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;
use Illuminate\Support\Facades\Auth;
use App\Models\Setting\Point;

class AssessmentDetail extends Model
{
    use LogsActivity;
    protected $table = 'assessment_details';
    protected $guarded = [];
    protected static $logName = 'detail assessment';
    protected static $logFillable = true;
    protected static $logUnguarded = true;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['*'])->logOnlyDirty();
    }

    // public function tapActivity(Activity $activity,string $eventName)
    // {   
    //     $detailchecklist = DetailChecklist::where('id',$activity->subject_id)->first();
    //     $question = Question::where('id',$detailchecklist->question_id)->pluck('question')->first();
    //     $activity->description   = $this->name . " {$eventName} Oleh: " . Auth::user()->name. ' untuk pertanyaan : '.$question;
    //     $activity->log_name      = 'detail checklist';
    // }
    // public function getDescriptionForEvent(string $eventName): string
    // {
    //     return $this->name . " {$eventName} Oleh: " . Auth::user()->name;
    // }
    

}
