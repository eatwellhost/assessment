<?php

namespace App\Models;

use App\Models\Checklist as ModelsChecklist;
use App\Models\Master\Checklist\JenisAudit;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use App\Models\Master\Outlet;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Assessment extends Model
{
    use LogsActivity;
    protected $guarded = [];
    protected static $logName = 'assessments';
    protected static $logFillable = true;
    protected static $logUnguarded = true;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['*'])->logOnlyDirty();
    }

    // public function tapActivity(Activity $activity,string $eventName)
    // {  
    //     $checklist = Checklist::where('id',$activity->subject_id)->first();
    //     $jenis = JenisAudit::where('id', $checklist->jenis_id)->pluck('nama')->first();
    //     $nama_outlet = Outlet::where('id',$checklist->outlet_id)->pluck('nama')->first();

    //     $activity->description   = $this->name . " {$eventName} Oleh : " .Auth::user()->name. ' Untuk Outlet: ' .$nama_outlet.', Dengan Jenis Audit : '.$jenis.', Pada Tanggal : ' .$checklist->tgl_input;
    //     $activity->log_name      = 'checklist';
    // }

    // public function getuser()
    // {
    // 	return $this->hasMany(User::class,'id','user_id');
    // }
}
