<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Models\Pegawai;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'random_password',
        'password_changed_at',
        'pegawai_id',
        'old_password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAuthPassword() {
        return $this->password;
    }

    public function getmenuaccess()
    {
        try{
            return Menu::whereHas('roles', function($query){
                $query->whereIn('id', explode(',', $this->roles()->get()->implode('id', ',')));
            })->where('status', (bool)true)->orderBy('order')->get();
        }catch(Exception $e){
            return collect([]);
        }
    }

    public function getpermissionku($label)
    {
        $roles_id = $this->roles()->get()->implode('id', ',');
        $cek_permission = DB::table('role_has_permissions')->where('role_id',(int)$roles_id)->where('label',$label)->first();

        if($cek_permission){
            return true;
        }
        return false;
    }

    public function pegawai()
    {
    	return $this->hasOne(Pegawai::class,'id','pegawai_id');
    }

}
