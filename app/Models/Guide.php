<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Guide extends Model
{
    protected $table = 'guides';
    protected $guarded = [];
}
