<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Location extends Model
{
    protected $table = 'locations';
    protected $guarded = [];
}
