<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class StandarPenilaian extends Model
{
    protected $table = 'standar_penilaians';
    protected $guarded = [];
}
