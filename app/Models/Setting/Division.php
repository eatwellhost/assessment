<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Division extends Model
{
    protected $table = 'divisions';
    protected $guarded = [];
}
