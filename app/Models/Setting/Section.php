<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Section extends Model
{
    protected $table = 'sections';
    protected $guarded = [];
}
