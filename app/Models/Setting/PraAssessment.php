<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class PraAssessment extends Model
{
    protected $table = 'pra_assessments';
    protected $guarded = [];
}
