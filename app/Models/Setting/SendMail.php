<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class SendMail extends Model
{
    protected $table = 'send_mails';
    protected $guarded = [];
}
