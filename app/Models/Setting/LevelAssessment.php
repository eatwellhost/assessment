<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class LevelAssessment extends Model
{
    protected $table = 'level_assessments';
    protected $guarded = [];
}
