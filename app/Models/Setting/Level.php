<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Level extends Model
{
    protected $table = 'level_pegawais';
    protected $guarded = [];
}
