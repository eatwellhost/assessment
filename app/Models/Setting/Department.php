<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Department extends Model
{
    protected $table = 'departments';
    protected $guarded = [];
}
