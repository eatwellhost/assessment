<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class EmployeeStatus extends Model
{
    protected $table = 'employee_statuss';
    protected $guarded = [];
}
