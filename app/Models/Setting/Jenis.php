<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Jenis extends Model
{
    protected $table = 'jenis_assessments';
    protected $guarded = [];
}
