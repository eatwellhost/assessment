<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentHasTim extends Model
{
    protected $table = 'pra_assessment_has_tims';
    protected $guarded = [];
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Master\Outlet', 'outlet_id');
    }
    

}

