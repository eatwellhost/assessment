<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;


class Pegawai extends Model
{
    protected $table = 'pegawais';
    protected $guarded = [];
}
