<?php

use App\Http\Controllers\AssessmentController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FetchController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\GuideController;
use App\Http\Controllers\Laporan\LaporanAssessmentController;
use App\Http\Controllers\Manajemen\RoleController;
use App\Http\Controllers\Manajemen\MenuController;
use App\Http\Controllers\Manajemen\UserController;
use App\Http\Controllers\Manajemen\PermissionController;
use App\Http\Controllers\Manajemen\PegawaiController;
use App\Http\Controllers\Laporan\LaporanChecklistController;
use App\Http\Controllers\SentMailController;
use App\Http\Controllers\Setting\Master\CompanyController;
use App\Http\Controllers\Setting\Master\DepartmentController;
use App\Http\Controllers\Setting\Master\DivisionController;
use App\Http\Controllers\Setting\Master\LevelAssessmentController;
use App\Http\Controllers\Setting\Master\LevelPegawaiController;
use App\Http\Controllers\Setting\Master\LocationController;
use App\Http\Controllers\Setting\Master\SectionController;
use App\Http\Controllers\Setting\PeriodeController;
use App\Http\Controllers\Setting\PointController;
use App\Http\Controllers\Setting\PraAssessmentController;
use App\Http\Middleware\FrameHeadersMiddleware;
use App\Http\Middleware\XSS;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware([XSS::class, FrameHeadersMiddleware::class])->group(function () {
    Route::get('login',function(){return view('pages.auth.login');})->name('login');
    Route::get('changepassword',function(){return view('pages.auth.changepassword');})->name('changepassword');
    Route::post('changepassword','App\Http\Controllers\Auth\LoginController@change')->name('auth.changepassword.store');


    Route::prefix('auth')->group(function(){
        Route::post('login','App\Http\Controllers\Auth\LoginController@login')->name('auth.login');
        Route::get('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('auth.logout');  
    });
    Route::middleware(['auth', 'verified'])->group(function () {
        Route::get('/', 'App\Http\Controllers\DashboardController@index')->middleware(['auth', 'verified']);
        Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->middleware(['auth', 'verified'])->name('dashboard.index');
        Route::prefix('assessment')->group(function(){
            Route::get('/', [AssessmentController::class, 'index'])->name('assessment.index');
            Route::post('create', [AssessmentController::class, 'create'])->name('assessment.create');
            Route::post('edit', [AssessmentController::class, 'edit'])->name('assessment.edit');
            Route::post('getpicture', [AssessmentController::class, 'getpicture'])->name('assessment.getpicture');
            Route::post('getupload', [AssessmentController::class, 'getupload'])->name('assessment.getupload');
            Route::post('showpicture', [AssessmentController::class, 'showpicture'])->name('assessment.showpicture');
            Route::post('storepicture', [AssessmentController::class, 'storepicture'])->name('assessment.storepicture');
            Route::post('store', [AssessmentController::class, 'store'])->name('assessment.store');
            Route::post('sentmail', [AssessmentController::class, 'sentmail'])->name('assessment.sentmail');
            Route::post('status', [AssessmentController::class, 'status'])->name('assessment.status');
            Route::get('inputassessment/{id?}', [AssessmentController::class, 'inputassessment'])->name('assessment.inputassessment');
            Route::post('delete', [AssessmentController::class, 'delete'])->name('assessment.delete');
            Route::get('datatable', [AssessmentController::class, 'datatable'])->name('assessment.datatable');
            Route::get('table', [AssessmentController::class, 'datatable'])->name('assessment.table');
            Route::post('gettreequestion', [AssessmentController::class, 'gettreequestion'])->name('assessment.gettreequestion');
        });

        Route::prefix('laporan')->namespace('Laporan')->group(function(){
            Route::prefix('assessment')->group(function(){
                Route::get('/', [LaporanAssessmentController::class, 'index'])->name('laporan.assessment.index');
                Route::post('create', [LaporanAssessmentController::class, 'create'])->name('laporan.assessment.create');
                Route::post('edit', [LaporanAssessmentController::class, 'edit'])->name('laporan.assessment.edit');
                Route::post('getpicture', [LaporanAssessmentController::class, 'getpicture'])->name('laporan.assessment.getpicture');
                Route::post('showpicture', [LaporanAssessmentController::class, 'showpicture'])->name('laporan.assessment.showpicture');
                Route::post('store', [LaporanAssessmentController::class, 'store'])->name('laporan.assessment.store');
                Route::post('cetakassessment',[LaporanAssessmentController::class, 'cetakassessment'])->name('laporan.assessment.cetakassessment');
                Route::get('showassessment/{id?}/{pegawai_id?}/{penilai_id?}', [LaporanAssessmentController::class, 'showassessment'])->name('laporan.assessment.showassessment');
                Route::post('delete', [LaporanAssessmentController::class, 'delete'])->name('laporan.assessment.delete');
                Route::get('datatable', [LaporanAssessmentController::class, 'datatable'])->name('laporan.assessment.datatable');
                Route::get('table', [LaporanAssessmentController::class, 'datatable'])->name('laporan.assessment.table');
                Route::post('gettreequestion', [LaporanAssessmentController::class, 'gettreequestion'])->name('laporan.assessment.gettreequestion');
            });
        });
        Route::prefix('setting')->namespace('Setting')->group(function(){
            Route::prefix('periode')->group(function(){
                Route::get('/', [PeriodeController::class, 'index'])->name('setting.periode.index');
                Route::post('create', [PeriodeController::class, 'create'])->name('setting.periode.create');
                Route::post('edit', [PeriodeController::class, 'edit'])->name('setting.periode.edit');
                Route::post('store', [PeriodeController::class, 'store'])->name('setting.periode.store');
                Route::post('delete', [PeriodeController::class, 'delete'])->name('setting.periode.delete');
                Route::get('datatable', [PeriodeController::class, 'datatable'])->name('setting.periode.datatable');
            });
            
            Route::prefix('mapping_pra_assessment')->group(function(){
                Route::get('/', [PraAssessmentController::class, 'index'])->name('setting.mapping_pra_assessment.index');
                Route::post('create', [PraAssessmentController::class, 'create'])->name('setting.mapping_pra_assessment.create');
                Route::post('edit', [PraAssessmentController::class, 'edit'])->name('setting.mapping_pra_assessment.edit');
                Route::post('store', [PraAssessmentController::class, 'store'])->name('setting.mapping_pra_assessment.store');
                Route::post('delete', [PraAssessmentController::class, 'delete'])->name('setting.mapping_pra_assessment.delete');
                Route::get('datatable', [PraAssessmentController::class, 'datatable'])->name('setting.mapping_pra_assessment.datatable');
                Route::post('sentmail', [PraAssessmentController::class, 'sentmail'])->name('setting.mapping_pra_assessment.sentmail');
            });

            Route::prefix('point_assessment')->group(function(){
                Route::get('/', [PointController::class, 'index'])->name('setting.point_assessment.index');
                Route::post('create', [PointController::class, 'create'])->name('setting.point_assessment.create');
                Route::post('edit', [PointController::class, 'edit'])->name('setting.point_assessment.edit');
                Route::post('store', [PointController::class, 'store'])->name('setting.point_assessment.store');
                Route::post('delete', [PointController::class, 'delete'])->name('setting.point_assessment.delete');
                Route::get('datatable', [PointController::class, 'datatable'])->name('setting.point_assessment.datatable');
                Route::post('gettreequestion', [PointController::class, 'gettreequestion'])->name('setting.point_assessment.gettreequestion');
                Route::post('submitchangestructure', [PointController::class, 'submitchangestructure'])->name('setting.point_assessment.submitchangestructure');
            });
            Route::prefix('master')->namespace('Master')->group(function(){
                Route::prefix('company')->group(function(){
                    Route::get('/', [CompanyController::class, 'index'])->name('setting.master.company.index');
                    Route::post('create', [CompanyController::class, 'create'])->name('setting.master.company.create');
                    Route::post('edit', [CompanyController::class, 'edit'])->name('setting.master.company.edit');
                    Route::post('store', [CompanyController::class, 'store'])->name('setting.master.company.store');
                    Route::post('delete', [CompanyController::class, 'delete'])->name('setting.master.company.delete');
                    Route::get('datatable', [CompanyController::class, 'datatable'])->name('setting.master.company.datatable');
                });

                Route::prefix('level_pegawai')->group(function(){
                    Route::get('/', [LevelPegawaiController::class, 'index'])->name('setting.master.level_pegawai.index');
                    Route::post('create', [LevelPegawaiController::class, 'create'])->name('setting.master.level_pegawai.create');
                    Route::post('edit', [LevelPegawaiController::class, 'edit'])->name('setting.master.level_pegawai.edit');
                    Route::post('store', [LevelPegawaiController::class, 'store'])->name('setting.master.level_pegawai.store');
                    Route::post('delete', [LevelPegawaiController::class, 'delete'])->name('setting.master.level_pegawai.delete');
                    Route::get('datatable', [LevelPegawaiController::class, 'datatable'])->name('setting.master.level_pegawai.datatable');
                });

                Route::prefix('level_assessment')->group(function(){
                    Route::get('/', [LevelAssessmentController::class, 'index'])->name('setting.master.level_assessment.index');
                    Route::post('create', [LevelAssessmentController::class, 'create'])->name('setting.master.level_assessment.create');
                    Route::post('edit', [LevelAssessmentController::class, 'edit'])->name('setting.master.level_assessment.edit');
                    Route::post('store', [LevelAssessmentController::class, 'store'])->name('setting.master.level_assessment.store');
                    Route::post('delete', [LevelAssessmentController::class, 'delete'])->name('setting.master.level_assessment.delete');
                    Route::get('datatable', [LevelAssessmentController::class, 'datatable'])->name('setting.master.level_assessment.datatable');
                });

                Route::prefix('division')->group(function(){
                    Route::get('/', [DivisionController::class, 'index'])->name('setting.master.division.index');
                    Route::post('create', [DivisionController::class, 'create'])->name('setting.master.division.create');
                    Route::post('edit', [DivisionController::class, 'edit'])->name('setting.master.division.edit');
                    Route::post('store', [DivisionController::class, 'store'])->name('setting.master.division.store');
                    Route::post('delete', [DivisionController::class, 'delete'])->name('setting.master.division.delete');
                    Route::get('datatable', [DivisionController::class, 'datatable'])->name('setting.master.division.datatable');
                });

                Route::prefix('department')->group(function(){
                    Route::get('/', [DepartmentController::class, 'index'])->name('setting.master.department.index');
                    Route::post('create', [DepartmentController::class, 'create'])->name('setting.master.department.create');
                    Route::post('edit', [DepartmentController::class, 'edit'])->name('setting.master.department.edit');
                    Route::post('store', [DepartmentController::class, 'store'])->name('setting.master.department.store');
                    Route::post('delete', [DepartmentController::class, 'delete'])->name('setting.master.department.delete');
                    Route::get('datatable', [DepartmentController::class, 'datatable'])->name('setting.master.department.datatable');
                });

                Route::prefix('section')->group(function(){
                    Route::get('/', [SectionController::class, 'index'])->name('setting.master.section.index');
                    Route::post('create', [SectionController::class, 'create'])->name('setting.master.section.create');
                    Route::post('edit', [SectionController::class, 'edit'])->name('setting.master.section.edit');
                    Route::post('store', [SectionController::class, 'store'])->name('setting.master.section.store');
                    Route::post('delete', [SectionController::class, 'delete'])->name('setting.master.section.delete');
                    Route::get('datatable', [SectionController::class, 'datatable'])->name('setting.master.section.datatable');
                });

                Route::prefix('location')->group(function(){
                    Route::get('/', [LocationController::class, 'index'])->name('setting.master.location.index');
                    Route::post('create', [LocationController::class, 'create'])->name('setting.master.location.create');
                    Route::post('edit', [LocationController::class, 'edit'])->name('setting.master.location.edit');
                    Route::post('store', [LocationController::class, 'store'])->name('setting.master.location.store');
                    Route::post('delete', [LocationController::class, 'delete'])->name('setting.master.location.delete');
                    Route::get('datatable', [LocationController::class, 'datatable'])->name('setting.master.location.datatable');
                });
            });
        });
        Route::prefix('manajemen')->namespace('Manajemen')->group(function(){

            Route::prefix('menu')->group(function(){
                Route::get('/', [MenuController::class, 'index'])->name('manajemen.menu.index');
                Route::post('create', [MenuController::class, 'create'])->name('manajemen.menu.create');
                Route::post('edit', [MenuController::class, 'edit'])->name('manajemen.menu.edit');
                Route::post('store', [MenuController::class, 'store'])->name('manajemen.menu.store');
                Route::post('delete', [MenuController::class, 'delete'])->name('manajemen.menu.delete');
                Route::post('gettreemenu', [MenuController::class, 'gettreemenu'])->name('manajemen.menu.gettreemenu');
                Route::post('submitchangestructure', [MenuController::class, 'submitchangestructure'])->name('manajemen.menu.submitchangestructure');
                Route::get('datatable', [MenuController::class, 'datatable'])->name('manajemen.menu.datatable');
            });      
            // user management
            Route::prefix('user')->group(function(){
                Route::get('/', [UserController::class, 'index'])->name('manajemen.user.index');
                Route::post('create', [UserController::class, 'create'])->name('manajemen.user.create');
                Route::post('edit', [UserController::class, 'edit'])->name('manajemen.user.edit');
                Route::post('reset', [UserController::class, 'reset'])->name('manajemen.user.reset');
                Route::post('store', [UserController::class, 'store'])->name('manajemen.user.store');
                Route::post('delete', [UserController::class, 'delete'])->name('manajemen.user.delete');
                Route::post('checkuser', [UserController::class, 'checkuser'])->name('manajemen.user.checkuser');
                Route::get('datatable', [UserController::class, 'datatable'])->name('manajemen.user.datatable');
            });

            Route::prefix('permission')->group(function(){
                Route::get('/', [PermissionController::class, 'index'])->name('manajemen.permission.index');
                Route::post('create', [PermissionController::class, 'create'])->name('manajemen.permission.create');
                Route::post('edit', [PermissionController::class, 'edit'])->name('manajemen.permission.edit');
                Route::post('store', [PermissionController::class, 'store'])->name('manajemen.permission.store');
                Route::post('delete', [PermissionController::class, 'delete'])->name('manajemen.permission.delete');
                Route::get('datatable', [PermissionController::class, 'datatable'])->name('manajemen.permission.datatable');
            });

            Route::prefix('role')->group(function(){
                Route::get('/', [RoleController::class , 'index'])->name('manajemen.role.index');
                Route::get('create', [RoleController::class , 'create'])->name('manajemen.role.create');
                Route::get('edit/{id?}', [RoleController::class , 'edit'])->name('manajemen.role.edit');
                Route::get('detail/{id?}', [RoleController::class , 'detail'])->name('manajemen.role.detail');
                Route::post('store', [RoleController::class , 'store'])->name('manajemen.role.store');
                Route::post('delete', [RoleController::class , 'delete'])->name('manajemen.role.delete');
                Route::get('datatable', [RoleController::class , 'datatable'])->name('manajemen.role.datatable');
                Route::get('gettreemenubyrole/{id?}', [RoleController::class , 'gettreemenubyrole'])->name('manajemen.role.gettreemenubyrole');
            });

            Route::prefix('pegawai')->group(function(){
                Route::get('/', [ PegawaiController::class, 'index'])->name('manajemen.pegawai.index');
                Route::post('create', [PegawaiController::class, 'create'])->name('manajemen.pegawai.create');
                Route::post('edit', [PegawaiController::class, 'edit'])->name('manajemen.pegawai.edit');
                Route::post('store', [PegawaiController::class, 'store'])->name('manajemen.pegawai.store');
                Route::post('delete', [PegawaiController::class, 'delete'])->name('manajemen.pegawai.delete');
                Route::post('getpegawai', [PegawaiController::class, 'getPegawai'])->name('manajemen.pegawai.getpegawai');
                Route::get('datatable', [PegawaiController::class, 'datatable'])->name('manajemen.pegawai.datatable');
            });
        });

        Route::prefix('log')->namespace('Log')->group(function(){
            Route::get('/', [LogController::class, 'index'])->name('log.index');
            Route::get('datatable', [LogController::class, 'datatable'])->name('log.datatable');
        });

        Route::prefix('sent_mail')->group(function(){
            Route::get('/', [SentMailController::class, 'index'])->name('sent_mail.index');
            Route::post('create', [SentMailController::class, 'create'])->name('sent_mail.create');
            Route::post('edit', [SentMailController::class, 'edit'])->name('sent_mail.edit');
            Route::post('store', [SentMailController::class, 'store'])->name('sent_mail.store');
            Route::post('delete', [SentMailController::class, 'delete'])->name('sent_mail.delete');
            Route::get('datatable', [SentMailController::class, 'datatable'])->name('sent_mail.datatable');
            Route::post('sentmail', [SentMailController::class, 'sentMail'])->name('sent_mail.sentmail');

        });

        // Route::prefix('guide')->group(function(){
        //     Route::get('/', [GuideController::class, 'index'])->name('guide.index');
        //     Route::post('create', [GuideController::class, 'create'])->name('guide.create');
        //     Route::post('edit', [GuideController::class, 'edit'])->name('guide.edit');
        //     Route::post('store', [GuideController::class, 'store'])->name('guide.store');
        //     Route::post('delete', [GuideController::class, 'delete'])->name('guide.delete');
        //     Route::get('datatable', [GuideController::class, 'datatable'])->name('guide.datatable');
        // });

        Route::prefix('general')->group(function(){
            Route::get('fetchparentmenu', [GeneralController::class, 'fetchparentmenu'])->name('general.fetchparentmenu');
            Route::get('fetchparentquestion', [GeneralController::class, 'fetchparentquestion'])->name('general.fetchparentquestion');
            Route::get('fetchrole', [GeneralController::class, 'fetchrole'])->name('general.fetchrole');
            Route::get('getbrand',[FetchController::class,'getBrand'])->name('general.getbrand');
            Route::get('getjenisaudit',[FetchController::class,'getJenisAudit'])->name('general.getjenisaudit');
            Route::get('{area_id?}/getoutletbyarea',[FetchController::class,'getOutletByArea'])->name('general.getoutletbyarea');
            Route::get('{brand_id?}/getoutletbybrand',[FetchController::class,'getOutletByBrand'])->name('general.getoutletbybrand');
            Route::get('getprovinsi', [FetchController::class, 'getprovinsi'])->name('general.getprovinsi');
            Route::get('{brand_id?}/getarea', [FetchController::class, 'getarea'])->name('general.getarea');
            Route::get('{brand_id?}/getam', [FetchController::class, 'getam'])->name('general.getam');
            Route::get('{id_provinsi}/getkota', [FetchController::class, 'getkota'])->name('general.getkota');
            Route::get('{brand_id}/getteknisi', [FetchController::class, 'getteknisi'])->name('general.getteknisi');

            Route::get('{pegawai_id?}/getpegawai', [FetchController::class, 'getpegawai'])->name('general.getpegawai');
            Route::get('{division_id?}/getdept', [FetchController::class, 'getDept'])->name('general.getdept');
            Route::get('{department_id?}/getsect', [FetchController::class, 'getSect'])->name('general.getsect');
            // Route::get('getinstansi', [FetchController::class, 'getinstansi'])->name('general.getinstansi');
            // Route::get('{id_instansi}/getjabatan', [FetchController::class, 'getjabatan'])->name('general.getjabatan');
            // Route::get('getstatus', [FetchController::class, 'getStatus'])->name('general.getstatus');
        }); 
    }); 
});


Route::get('/error', function () {
    abort(500);
});

require __DIR__.'/auth.php';
