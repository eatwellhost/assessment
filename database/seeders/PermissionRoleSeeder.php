<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Menu;
use App\Models\User;
use DB;


class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name','Super Admin')->first();

        DB::table('model_has_roles')->updateOrInsert([
            'role_id'=>$role->id,
            'model_type'=>'App\Models\User',
            'model_id'=>(int)User::where('username','mohamad.adi')->pluck('id')->first()
        ],[
            'role_id'=>$role->id,
            'model_type'=>'App\Models\User',
            'model_id'=>(int)User::where('username','mohamad.adi')->pluck('id')->first()
        ]);
        $menu = Menu::get();
        
        foreach($menu as $m){
            DB::table('role_menu')->updateOrInsert([
                'role_id' => $role->id,
                'menu_id' => $m->id                
            ],[
                'role_id' => $role->id,
                'menu_id' => $m->id
            ]);
        }
        $role_menu = DB::table('role_menu')->leftjoin('menus','menus.id','role_menu.menu_id')->get();

        $permission = Permission::get();

        $role_p = [];
        foreach($role_menu as $rm){
            foreach($permission as $pm){
                $role_p[] = [
                    'permission_id'=>$pm->id,
                    'menu_id'=>$rm->menu_id,
                    'role_id'=>$rm->role_id,
                    'label'=>strtolower($rm->label).':'.strtolower($pm->name)
                ];
            }
        }

        foreach($role_p as $rolepm){
            DB::table('role_has_permissions')->updateOrInsert([
                'permission_id'=>$rolepm['permission_id'],
                'menu_id'=>$rolepm['menu_id'],
                'role_id'=>$rolepm['role_id']
            ],[
                'permission_id'=>$rolepm['permission_id'],
                'menu_id'=>$rolepm['menu_id'],
                'role_id'=>$rolepm['role_id'],
                'label'=>$rolepm['label']
            ]);
        }
    }

}
