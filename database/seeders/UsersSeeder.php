<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserInfo;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        User::updateOrInsert([
            'id' => 1,
            'username' => 'mohamad.adi',
        ],
        [
            'id' => 1,
            'username' => 'mohamad.adi',
            'email' => 'mohamadadi@eatwell.co.id',
            'name' => 'Mohamad Adi',
            'password' => Hash::make('12345678'),
            'random_password' => '12345678',
            'email_verified_at' => now(),
            'remember_token' => Str::random(64)
        ]);

        User::updateOrInsert([
            'id' => 2,
            'username' => 'ria.damayanti',
        ],
        [
            'id' => 2,
            'username' => 'ria.damayanti',
            'email' => 'ria.damayanti@eatwell.co.id',
            'name' => 'Ria Damayanti',
            'password' => Hash::make('E4tw3ll!'),
            'random_password' => 'E4tw3ll!',
            'email_verified_at' => now(),
            'remember_token' => Str::random(64)
        ]);
    }
}
