<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $data = $this->crudActions();

        foreach ($data as $value) {
            Permission::create([
                'name' => $value,
            ]);
        }
    }

    public function crudActions()
    {
        $actions = [];
        // list of default permission actions
        $crud = ['create', 'view', 'edit', 'delete'];

        foreach ($crud as $value) {
            $actions[] = $value;
        }

        return $actions;
    }
}
