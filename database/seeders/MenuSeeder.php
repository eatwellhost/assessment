<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\RoleHasMenus;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Menu::updateorInsert(
            [
            'id' => 1
            ],
            [
            'id'=>1,
            'parent_id'=>0,
            'label'=>"Dashboard",
            'link'=>null,
            'order'=>1,
            'icon'=>'fas fa-home',
            'status'=>true,
            'route_name'=>'dashboard.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 2
            ],
            [
            'id'=>2,
            'parent_id'=>0,
            'label'=>"Assessment",
            'link'=>null,
            'order'=>2,
            'icon'=>'fas fa-clipboard-list',
            'status'=>true,
            'route_name'=>'assessment.index',

        ]);
        Menu::updateorInsert(
            [
            'id' => 3
            ],
            [
            'id'=>3,
            'parent_id'=>0,
            'label'=>"Setting",
            'link'=>null,
            'order'=>3,
            'icon'=>'fa fa-cog',
            'status'=>true,
            'route_name'=>'',

        ]);
        Menu::updateorInsert(
            [
            'id' => 31
            ],
            [
            'id'=>31,
            'parent_id'=>3,
            'label'=>"Periode",
            'link'=>null,
            'order'=>1,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.periode.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 32
            ],
            [
            'id'=>32,
            'parent_id'=>3,
            'label'=>"Point Assessment",
            'link'=>null,
            'order'=>2,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.point_assessment.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 33
            ],
            [
            'id'=>33,
            'parent_id'=>3,
            'label'=>"Mapping Pra Assessment",
            'link'=>null,
            'order'=>3,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.mapping_pra_assessment.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 34
            ],
            [
            'id'=>34,
            'parent_id'=>3,
            'label'=>"Master",
            'link'=>null,
            'order'=>100,
            'icon'=>'fa fa-genderless',
            'status'=>true,
            'route_name'=>'',
        ]);
        Menu::updateorInsert(
            [
            'id' => 341
            ],
            [
            'id'=>341,
            'parent_id'=>34,
            'label'=>"Company",
            'link'=>null,
            'order'=>1,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.company.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 342
            ],
            [
            'id'=>342,
            'parent_id'=>34,
            'label'=>"Level Pegawai",
            'link'=>null,
            'order'=>10,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.level_pegawai.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 343
            ],
            [
            'id'=>343,
            'parent_id'=>34,
            'label'=>"Level Assessment",
            'link'=>null,
            'order'=>11,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.level_assessment.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 344
            ],
            [
            'id'=>344,
            'parent_id'=>34,
            'label'=>"Division",
            'link'=>null,
            'order'=>2,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.division.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 345
            ],
            [
            'id'=>345,
            'parent_id'=>34,
            'label'=>"Department",
            'link'=>null,
            'order'=>3,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.department.index',
        ]);Menu::updateorInsert(
            [
            'id' => 346
            ],
            [
            'id'=>346,
            'parent_id'=>34,
            'label'=>"Section",
            'link'=>null,
            'order'=>4,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.section.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 347
            ],
            [
            'id'=>347,
            'parent_id'=>34,
            'label'=>"Location",
            'link'=>null,
            'order'=>5,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'setting.master.location.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 4
            ],
            [
            'id'=>4,
            'parent_id'=>0,
            'label'=>"User Management",
            'link'=>null,
            'order'=>5,
            'icon'=>'fas fa-users',
            'status'=>true,
            'route_name'=>'',

        ]);
        Menu::updateorInsert(
            [
            'id' => 41
            ],
            [
            'id'=>41,
            'parent_id'=>4,
            'label'=>"User",
            'link'=>null,
            'order'=>1,
            'icon'=>null,
            'status'=>true,
            'route_name'=>'manajemen.user.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 42
            ],   
            [
            'id'=>42,
            'parent_id'=>4,
            'label'=>"Menu",
            'link'=>null,
            'order'=>2,
            'icon'=>null,
            'status'=>true,
            'route_name'=>'manajemen.menu.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 43
            ],
            [
            'id'=>43,
            'parent_id'=>4,
            'label'=>"Role",
            'link'=>null,
            'order'=>3,
            'icon'=>null,
            'status'=>true,
            'route_name'=>'manajemen.role.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 44
            ],
            [
            'id'=>44,
            'parent_id'=>4,
            'label'=>"Permission",
            'link'=>null,
            'order'=>4,
            'icon'=>null,
            'status'=>true,
            'route_name'=>'manajemen.permission.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 45
            ],
            [
            'id'=>45,
            'parent_id'=>4,
            'label'=>"Pegawai",
            'link'=>null,
            'order'=>5,
            'icon'=>null,
            'status'=>true,
            'route_name'=>'manajemen.pegawai.index',
        ]);
        Menu::updateorInsert(
            [
            'id' => 5
            ],
            [
            'id'=>5,
            'parent_id'=>0,
            'label'=>"Laporan",
            'link'=>null,
            'order'=>5,
            'icon'=>'fa fa-file-alt',
            'status'=>true,
            'route_name'=>'',
        ]);
        Menu::updateorInsert(
            [
            'id' => 51
            ],
            [
            'id'=>51,
            'parent_id'=>5,
            'label'=>"Assessment",
            'link'=>null,
            'order'=>1,
            'icon'=>'',
            'status'=>true,
            'route_name'=>'laporan.assessment.index',
        ]);

        Menu::updateorInsert(
            [
            'id' => 6
            ],
            [
            'id'=>6,
            'parent_id'=>0,
            'label'=>"Sent Mail",
            'link'=>null,
            'order'=>6,
            'icon'=>'fa fa-envelope-open-text',
            'status'=>true,
            'route_name'=>'sent_mail.index',
        ]);
        
        for ($i = 1; $i <= 51; $i++) {
            RoleHasMenus::insert(
                [
                    'menu_id' => $i,
                    'role_id' => 1,
                ]
            );
        }
        RoleHasMenus::insert(
            [
                'menu_id' => 1,
                'role_id' => 2,
            ]
        );

    }
}
