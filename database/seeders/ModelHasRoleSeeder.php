<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class ModelHasRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Assign roles to adinistrators users
        $admin = User::where('id',1)->first();
        $admin->assignRole('Super Admin');

        $admin = User::where('id',2)->first();
        $admin->assignRole('Human Capital');
    }
}
