<?php

namespace Database\Seeders;

use App\Models\Setting\Company;
use App\Models\Setting\Department;
use App\Models\Setting\Division;
use App\Models\Setting\EmployeeStatus;
use App\Models\Setting\Jenis;
use App\Models\Setting\Level;
use App\Models\Setting\LevelAssessment;
use App\Models\Setting\Location;
use App\Models\Setting\PoinPenilaian;
use App\Models\Setting\Section;
use App\Models\Setting\SesiPenilai;
use App\Models\Setting\StandarPenilaian;
use App\Models\User;
use App\Models\UserInfo;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ReferensiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        #start generate referensi Level
        Level::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Staff',
        ]);

        Level::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Assistant Supervisor',
        ]);
        Level::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Supervisor',
        ]);
        Level::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Assistant Manager',
        ]);
        Level::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Manager',
        ]);
        Level::updateOrInsert([
            'id' => 6,
        ],
        [
            'id' => 6,
            'nama' => 'General Manager',
        ]);
        #end generate referensi level
        #start generate referensi company
        Company::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'PT Panca Boga Paramita',
        ]);
        Company::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'PT Dapur Solo Mustika Nusantara',
        ]);
        #end generate referensi company
        #start generate referensi employee status
        EmployeeStatus::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Permanent',
        ]);
        EmployeeStatus::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Contract',
        ]);
        EmployeeStatus::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Probation',
        ]);
        #end generate referensi employee status
        #start generate referensi location
        Location::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Head Office',
        ]);
        Location::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Central Kitchen Cikupa',
        ]);
        Location::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Ichiban Sushi',
        ]);
        Location::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Ta Wan',
        ]);
        Location::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Dapur Solo Resto',
        ]);
        Location::updateOrInsert([
            'id' => 6,
        ],
        [
            'id' => 6,
            'nama' => 'Dapur Solo Lunch Box',
        ]);
        #end generate referensi location
        #start generate referensi division
        Division::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Audit And Business Process',
        ]);
        Division::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Business Development',
        ]);
        Division::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Finance Accounting And Tax',
        ]);
        Division::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'General Affair',
        ]);
        Division::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Human Capital',
        ]);
        Division::updateOrInsert([
            'id' => 6,
        ],
        [
            'id' => 6,
            'nama' => 'Information Technology',
        ]);
        Division::updateOrInsert([
            'id' => 7,
        ],
        [
            'id' => 7,
            'nama' => 'Legal',
        ]);
        Division::updateOrInsert([
            'id' => 8,
        ],
        [
            'id' => 8,
            'nama' => 'Maintenance',
        ]);
        Division::updateOrInsert([
            'id' => 9,
        ],
        [
            'id' => 9,
            'nama' => 'Marketing',
        ]);
        Division::updateOrInsert([
            'id' => 10,
        ],
        [
            'id' => 10,
            'nama' => 'Operational',
        ]);
        Division::updateOrInsert([
            'id' => 11,
        ],
        [
            'id' => 11,
            'nama' => 'Project',
        ]);
        Division::updateOrInsert([
            'id' => 12,
        ],
        [
            'id' => 12,
            'nama' => 'Purchasing',
        ]);
        Division::updateOrInsert([
            'id' => 13,
        ],
        [
            'id' => 13,
            'nama' => 'Quality Assurance',
        ]);
        Division::updateOrInsert([
            'id' => 14,
        ],
        [
            'id' => 14,
            'nama' => 'Central Kitchen',
        ]);
        Division::updateOrInsert([
            'id' => 15,
        ],
        [
            'id' => 15,
            'nama' => 'Warehouse',
        ]);
        Division::updateOrInsert([
            'id' => 16,
        ],
        [
            'id' => 16,
            'nama' => 'Management',
        ]);
        #end generate referensi division
        #start generate referensi department
        Department::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Account Payable',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Accounting',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Audit And Business Process',
            'division_id' => 1
        ]);
        Department::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Business Development',
            'division_id' => 2
        ]);
        Department::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Cost Control',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 6,
        ],
        [
            'id' => 6,
            'nama' => 'Finance',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 7,
        ],
        [
            'id' => 7,
            'nama' => 'General Affair',
            'division_id' => 4
        ]);
        Department::updateOrInsert([
            'id' => 8,
        ],
        [
            'id' => 8,
            'nama' => 'Human Capital',
            'division_id' => 5
        ]);
        Department::updateOrInsert([
            'id' => 9,
        ],
        [
            'id' => 9,
            'nama' => 'Information Technology',
            'division_id' => 6
        ]);
        Department::updateOrInsert([
            'id' => 10,
        ],
        [
            'id' => 10,
            'nama' => 'Legal',
            'division_id' => 7
        ]);
        Department::updateOrInsert([
            'id' => 11,
        ],
        [
            'id' => 11,
            'nama' => 'Maintenance',
            'division_id' => 8
        ]);
        Department::updateOrInsert([
            'id' => 12,
        ],
        [
            'id' => 12,
            'nama' => 'Marketing',
            'division_id' => 9
        ]);
        Department::updateOrInsert([
            'id' => 13,
        ],
        [
            'id' => 13,
            'nama' => 'Project',
            'division_id' => 11
        ]);
        Department::updateOrInsert([
            'id' => 14,
        ],
        [
            'id' => 14,
            'nama' => 'Purchasing',
            'division_id' => 12
        ]);
        Department::updateOrInsert([
            'id' => 15,
        ],
        [
            'id' => 15,
            'nama' => 'Quality Assurance',
            'division_id' => 13
        ]);
        Department::updateOrInsert([
            'id' => 16,
        ],
        [
            'id' => 16,
            'nama' => 'Reconcile',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 17,
        ],
        [
            'id' => 17,
            'nama' => 'Tax',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 18,
        ],
        [
            'id' => 18,
            'nama' => 'Treasury',
            'division_id' => 3
        ]);
        Department::updateOrInsert([
            'id' => 19,
        ],
        [
            'id' => 19,
            'nama' => 'Operational Ichiban Sushi',
            'division_id' => 10
        ]);
        Department::updateOrInsert([
            'id' => 20,
        ],
        [
            'id' => 20,
            'nama' => 'Operational Ta Wan',
            'division_id' => 10
        ]);
        Department::updateOrInsert([
            'id' => 21,
        ],
        [
            'id' => 21,
            'nama' => 'Operational Dapur Solo',
            'division_id' => 10
        ]);
        Department::updateOrInsert([
            'id' => 22,
        ],
        [
            'id' => 22,
            'nama' => 'Seles',
            'division_id' => 10
        ]);
        Department::updateOrInsert([
            'id' => 23,
        ],
        [
            'id' => 23,
            'nama' => 'Central Kitchen',
            'division_id' => 14
        ]);
        Department::updateOrInsert([
            'id' => 24,
        ],
        [
            'id' => 24,
            'nama' => 'Warehouse',
            'division_id' => 15
        ]);
        Department::updateOrInsert([
            'id' => 25,
        ],
        [
            'id' => 25,
            'nama' => 'Management',
            'division_id' => 16
        ]);
        #end generate referensi department
        #start generate referensi section
        Section::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Account Payable',
            'department_id'=> 1
        ]);
        Section::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Accounting',
            'department_id'=> 1
        ]);
        Section::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Audit And Business Process',
            'department_id'=> 3
        ]);
        Section::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Business Development',
            'department_id'=> 4
        ]);
        Section::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Cost Control',
            'department_id'=> 5
        ]);
        Section::updateOrInsert([
            'id' => 6,
        ],
        [
            'id' => 6,
            'nama' => 'Finance',
            'department_id'=> 6
        ]);
        Section::updateOrInsert([
            'id' => 7,
        ],
        [
            'id' => 7,
            'nama' => 'General Affair',
            'department_id'=> 7
        ]);
        Section::updateOrInsert([
            'id' => 8,
        ],
        [
            'id' => 8,
            'nama' => 'Human Capital',
            'department_id'=> 8
        ]);
        Section::updateOrInsert([
            'id' => 9,
        ],
        [
            'id' => 9,
            'nama' => 'Information Technology',
            'department_id'=> 9
        ]);
        Section::updateOrInsert([
            'id' => 10,
        ],
        [
            'id' => 10,
            'nama' => 'Legal',
            'department_id'=> 10
        ]);
        Section::updateOrInsert([
            'id' => 11,
        ],
        [
            'id' => 11,
            'nama' => 'Maintenance',
            'department_id'=> 11
        ]);
        Section::updateOrInsert([
            'id' => 12,
        ],
        [
            'id' => 12,
            'nama' => 'Marketing',
            'department_id'=> 12
        ]);
        Section::updateOrInsert([
            'id' => 13,
        ],
        [
            'id' => 13,
            'nama' => 'Project',
            'department_id'=> 13
        ]);
        Section::updateOrInsert([
            'id' => 14,
        ],
        [
            'id' => 14,
            'nama' => 'Purchasing',
            'department_id'=> 14
        ]);
        Section::updateOrInsert([
            'id' => 15,
        ],
        [
            'id' => 15,
            'nama' => 'Quality Assurance',
            'department_id'=> 15
        ]);
        Section::updateOrInsert([
            'id' => 16,
        ],
        [
            'id' => 16,
            'nama' => 'Reconcile',
            'department_id'=> 16
        ]);
        Section::updateOrInsert([
            'id' => 17,
        ],
        [
            'id' => 17,
            'nama' => 'Tax',
            'department_id'=> 17
        ]);
        Section::updateOrInsert([
            'id' => 18,
        ],
        [
            'id' => 18,
            'nama' => 'Treasury',
            'department_id'=> 18
        ]);
        Section::updateOrInsert([
            'id' => 19,
        ],
        [
            'id' => 19,
            'nama' => 'Team Manajemen Ichiban Sushi',
            'department_id'=> 19
        ]);
        Section::updateOrInsert([
            'id' => 20,
        ],
        [
            'id' => 20,
            'nama' => 'Team Manajemen Ta Wan',
            'department_id'=> 20
        ]);
        Section::updateOrInsert([
            'id' => 21,
        ],
        [
            'id' => 21,
            'nama' => 'Team Manajemen Dapur Solo',
            'department_id'=> 21
        ]);
        Section::updateOrInsert([
            'id' => 22,
        ],
        [
            'id' => 22,
            'nama' => 'Panca Boga Paramita - Warehouse',
            'department_id'=> 24
        ]);
        Section::updateOrInsert([
            'id' => 23,
        ],
        [
            'id' => 23,
            'nama' => 'Ichiban Sushi - Central Kitchen',
            'department_id'=> 23
        ]);
        Section::updateOrInsert([
            'id' => 24,
        ],
        [
            'id' => 24,
            'nama' => 'Ta Wan - Central Kitchen',
            'department_id'=> 23
        ]);
        Section::updateOrInsert([
            'id' => 25,
        ],
        [
            'id' => 25,
            'nama' => 'Dapur Solo - Central Kitchen',
            'department_id'=> 23
        ]);
        Section::updateOrInsert([
            'id' => 26,
        ],
        [
            'id' => 26,
            'nama' => 'Dapur Solo - Warehouse',
            'department_id'=> 24
        ]);
        Section::updateOrInsert([
            'id' => 27,
        ],
        [
            'id' => 27,
            'nama' => 'Central Kitchen',
            'department_id'=> 23
        ]);
        Section::updateOrInsert([
            'id' => 28,
        ],
        [
            'id' => 28,
            'nama' => 'Management',
            'department_id'=> 25
        ]);

        Jenis::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Penilaian Kerja Akhir Tahun',
        ]);
        Jenis::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Penilaian Kerja Karyawan Kontrak',
        ]);
        Jenis::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Penilaian Kerja Promosi Jabatan',
        ]);
        Jenis::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Penilaian Kerja Masa Percobaan',
        ]);
        Jenis::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Penilaian Evaluasi Kinerja',
        ]);

        LevelAssessment::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Manager',
        ]);
        LevelAssessment::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Supervisor',
        ]);
        LevelAssessment::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Staff',
        ]);

        SesiPenilai::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Penilaian Diri Sendiri (Diri sendiri yang memberi nilai)',
        ]);
        SesiPenilai::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Penilaian dari Atasan (Atasan yang memberi nilai)',
        ]);
        SesiPenilai::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Penilaian dari Rekan Kerja (Rekan Kerja yang memberi nilai)',
        ]);
        SesiPenilai::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Penilaian dari Tim/ Bawahan (Tim yang memberi nilai)',
        ]);

        StandarPenilaian::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'nama' => 'Tidak Memenuhi Harapan',
            'batas_bawah'=>0,
            'batas_atas'=>2.5
        ]);
        StandarPenilaian::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'nama' => 'Memenuhi Sebagian Kecil Harapan',
            'batas_bawah'=>2.6,
            'batas_atas'=>3.0
        ]);
        StandarPenilaian::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'nama' => 'Memenuhi Sebagian Besar Harapan',
            'batas_bawah'=>3.1,
            'batas_atas'=>3.5
        ]);
        StandarPenilaian::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'nama' => 'Memenuhi Seluruh Harapan',
            'batas_bawah'=>3.6,
            'batas_atas'=>4.5
        ]);
        StandarPenilaian::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'nama' => 'Melebihi Harapan',
            'batas_bawah'=>4.6,
            'batas_atas'=>5.0
        ]);

        PoinPenilaian::updateOrInsert([
            'id' => 1,
        ],
        [
            'id' => 1,
            'poin' => 1,
            'keterangan' => 'Jarang/tidak pernah menjalankan tanggung jawabnya',
        ]);
        PoinPenilaian::updateOrInsert([
            'id' => 2,
        ],
        [
            'id' => 2,
            'poin' => 2,
            'keterangan' => 'Kurang konsisten dalam menjalankan tanggung jawabnya',
        ]);
        PoinPenilaian::updateOrInsert([
            'id' => 3,
        ],
        [
            'id' => 3,
            'poin' => 3,
            'keterangan' => 'Menjalankan sebagian besar tanggung jawabnya',
        ]);
        PoinPenilaian::updateOrInsert([
            'id' => 4,
        ],
        [
            'id' => 4,
            'poin' => 4,
            'keterangan' => 'Menjalankan tanggung jawabnya sesuai dengan harapan dan kinerjanya baik',
        ]);
        PoinPenilaian::updateOrInsert([
            'id' => 5,
        ],
        [
            'id' => 5,
            'poin' => 5,
            'keterangan' => 'Menjalankan tanggung jawabnya melebihi ekspektasi saya dan sangat luar biasa',
        ]);
    }
}
