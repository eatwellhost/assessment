<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateReferensiNegarasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referensi_negaras', function (Blueprint $table) {
            $table->id();
            $table->string('negara')->nullable();
            $table->boolean('status')->default('1')->nullable();
            $table->timestamps();
        });
        DB::unprepared("
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (3, 'Afrika Selatan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (4, 'Afrika Tengah', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (5, 'Albania', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (6, 'Aljazair', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (7, 'Amerika Serikat', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (8, 'Andorra', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (9, 'Angola', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (10, 'Antigua dan Barbuda', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (11, 'Arab Saudi', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (12, 'Argentina', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (13, 'Armenia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (14, 'Australia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (15, 'Austria', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (16, 'Azerbaijan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (17, 'Bahama', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (18, 'Bahrain', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (19, 'Bangladesh', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (20, 'Barbados', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (21, 'Belanda', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (22, 'Belarus', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (23, 'Belgia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (24, 'Belize', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (25, 'Benin', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (26, 'Bhutan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (27, 'Bolivia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (28, 'Bosnia dan Herzegovina', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (29, 'Botswana', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (30, 'Brasil', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (31, 'Britania Raya', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (32, 'Brunei Darussalam', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (33, 'Bulgaria', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (34, 'Burkina Faso', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (35, 'Burundi', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (36, 'Ceko', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (37, 'Chad', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (38, 'Chili', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (39, 'China', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (40, 'Denmark', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (41, 'Djibouti', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (42, 'Dominika', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (43, 'Ekuador', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (44, 'El Salvador', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (45, 'Eritrea', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (46, 'Estonia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (47, 'Ethiopia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (48, 'Fiji', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (49, 'Filipina', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (50, 'Finlandia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (51, 'Gabon', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (52, 'Gambia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (53, 'Georgia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (54, 'Ghana', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (55, 'Grenada', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (56, 'Guatemala', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (57, 'Guinea', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (58, 'Guinea Bissau', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (59, 'Guinea Khatulistiwa', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (60, 'Guyana', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (61, 'Haiti', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (62, 'Honduras', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (63, 'Hongaria', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (64, 'India', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (65, 'Indonesia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (66, 'Irak', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (67, 'Iran', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (68, 'Irlandia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (69, 'Islandia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (70, 'Israel', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (71, 'Italia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (72, 'Jamaika', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (73, 'Jepang', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (74, 'Jerman', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (75, 'Kamboja', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (76, 'Kamerun', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (77, 'Kanada', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (78, 'Kazakhstan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (79, 'Kenya', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (80, 'Kirgizstan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (81, 'Kiribati', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (82, 'Kolombia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (83, 'Komoro', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (84, 'Republik Kongo', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (85, 'Korea Selatan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (86, 'Korea Utara', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (87, 'Kosta Rika', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (88, 'Kroasia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (89, 'Kuba', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (90, 'Kuwait', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (91, 'Laos', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (92, 'Latvia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (93, 'Lebanon', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (94, 'Lesotho', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (95, 'Liberia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (96, 'Libya', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (97, 'Liechtenstein', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (98, 'Lituania', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (99, 'Luksemburg', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (100, 'Madagaskar', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (101, 'Makedonia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (102, 'Maladewa', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (103, 'Malawi', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (104, 'Malaysia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (105, 'Mali', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (106, 'Malta', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (107, 'Maroko', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (108, 'Marshall', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (109, 'Mauritania', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (110, 'Mauritius', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (111, 'Meksiko', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (112, 'Mesir', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (113, 'Mikronesia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (114, 'Moldova', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (115, 'Monako', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (116, 'Mongolia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (117, 'Montenegro', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (118, 'Mozambik', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (119, 'Myanmar', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (120, 'Namibia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (121, 'Nauru', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (122, 'Nepal', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (123, 'Niger', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (124, 'Nigeria', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (125, 'Nikaragua', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (126, 'Norwegia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (127, 'Oman', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (128, 'Pakistan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (129, 'Palau', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (130, 'Panama', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (131, 'Pantai Gading', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (132, 'Papua Nugini', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (133, 'Paraguay', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (134, 'Perancis', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (135, 'Peru', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (136, 'Polandia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (137, 'Portugal', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (138, 'Qatar', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (139, 'Republik Demokratik Kongo', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (175, 'Togo', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (140, 'Republik Dominika', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (141, 'Rumania', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (142, 'Rusia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (143, 'Rwanda', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (144, 'Saint Kitts and Nevis', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (145, 'Saint Lucia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (146, 'Saint Vincent and the Grenadines', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (147, 'Samoa', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (148, 'San Marino', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (149, 'Sao Tome and Principe', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (150, 'Selandia Baru', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (151, 'Senegal', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (152, 'Serbia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (153, 'Seychelles', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (154, 'Sierra Leone', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (155, 'Singapura', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (156, 'Siprus', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (157, 'Slovenia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (158, 'Slowakia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (159, 'Solomon', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (160, 'Somalia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (161, 'Spanyol', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (162, 'Sri Lanka', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (163, 'Sudan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (164, 'Sudan Selatan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (165, 'Suriah', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (166, 'Suriname', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (167, 'Swaziland', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (168, 'Swedia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (169, 'Swiss', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (170, 'Tajikistan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (171, 'Tanjung Verde', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (172, 'Tanzania', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (173, 'Thailand', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (174, 'Timor Leste', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (176, 'Tonga', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (177, 'Trinidad and Tobago', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (178, 'Tunisia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (179, 'Turki', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (180, 'Turkmenistan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (181, 'Tuvalu', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (182, 'Uganda', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (183, 'Ukraina', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (184, 'Uni Emirat Arab', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (185, 'Uruguay', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (186, 'Uzbekistan', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (187, 'Vanuatu', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (188, 'Venezuela', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (189, 'Vietnam', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (190, 'Yaman', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (191, 'Yordania', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (192, 'Yunani', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (193, 'Zambia', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (194, 'Zimbabwe', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (195, 'Vatican City', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (196, 'Palestina', 1, NULL, NULL);
        INSERT INTO referensi_negaras (id, negara, status, created_at, updated_at) VALUES (2, 'Afganistan', 1, NULL, '2022-11-18 09:37:05');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referensi_negaras');
    }
}
