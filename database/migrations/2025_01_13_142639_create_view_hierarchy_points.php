<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewHierarchyPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
        CREATE VIEW view_hierarchy_points AS

        WITH recursive `recursivepoints` AS (
	SELECT
		`points`.`id` AS `point_id`,
		`points`.`id` AS `root_id`,
		`points`.`parent_id` AS `parent_id`,
		`points`.`level` AS `level`,
		0 AS `orders` 
	FROM
		`points` 
	WHERE
		( `points`.`parent_id` = 0 ) UNION ALL
	SELECT
		`p`.`id` AS `point_id`,
		`hp`.`root_id` AS `root_id`,
		`p`.`parent_id` AS `parent_id`,
		`p`.`level` AS `level`,(
			`hp`.`orders` + 1 
		) AS `orders` 
	FROM
		(
			`points` `p`
			JOIN `recursivepoints` `hp` ON ((
					`p`.`parent_id` = `hp`.`point_id` 
				)))) SELECT * 
FROM
	`recursivepoints`;
        ";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP VIEW IF EXISTS new_view_pra_assessment;";
        DB::statement($sql);
    }
}
