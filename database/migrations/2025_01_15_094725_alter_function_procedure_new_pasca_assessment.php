<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFunctionProcedureNewPascaAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_new_pasca_assessment;
            CREATE PROCEDURE procedure_new_pasca_assessment(praassessment_id integer)
            begin
            SELECT
                pra_assessments.id,
                pra_assessments.pegawai_id AS as_penilai_id,
                pra_assessments.periode_id,
                pra_assessments.jenis_id,
                pra_assessments.level_id,
                pribadi.penilai_id,
                1 AS sesi_id,
                pribadi.praassessment_id,
                pribadi.POINT,
                pribadi.saran,
                pribadi.is_submit,
                pribadi.id AS assessment_id 
            FROM
                pra_assessments
                LEFT JOIN assessments pribadi ON pribadi.penilai_id = pra_assessments.pegawai_id 
                AND pribadi.praassessment_id = praassessment_id 
            WHERE
                pra_assessments.id = praassessment_id
	
	        union all 
	
            SELECT
                pra_assessments.id,
                pra_assessment_has_atasans.atasan_id AS as_penilai_id,
                pra_assessments.periode_id,
                pra_assessments.jenis_id,
                pra_assessments.level_id,
                atasan.penilai_id,
                2 AS sesi_id,
                atasan.praassessment_id,
                atasan.POINT,
                atasan.saran,
                atasan.is_submit,
                atasan.id AS assessment_id 
            FROM
                pra_assessments
                left join pra_assessment_has_atasans on pra_assessment_has_atasans.pra_assessment_id=pra_assessments.id AND pra_assessment_has_atasans.pra_assessment_id = praassessment_id
                LEFT JOIN assessments atasan ON atasan.penilai_id = pra_assessment_has_atasans.atasan_id AND atasan.praassessment_id = praassessment_id
            WHERE
                pra_assessments.id = praassessment_id AND pra_assessment_has_atasans.atasan_id IS NOT NULL
	
	        union all 
	
            SELECT
                pra_assessments.id,
                pra_assessment_has_rekans.rekan_id AS as_penilai_id,
                pra_assessments.periode_id,
                pra_assessments.jenis_id,
                pra_assessments.level_id,
                rekan.penilai_id,
                3 AS sesi_id,
                rekan.praassessment_id,
                rekan.POINT,
                rekan.saran,
                rekan.is_submit,
                rekan.id AS assessment_id 
            FROM
                pra_assessments
                left join pra_assessment_has_rekans on pra_assessment_has_rekans.pra_assessment_id=pra_assessments.id AND pra_assessment_has_rekans.pra_assessment_id = praassessment_id
                LEFT JOIN assessments rekan ON rekan.penilai_id = pra_assessment_has_rekans.rekan_id AND rekan.praassessment_id = praassessment_id
            WHERE
                pra_assessments.id = praassessment_id AND pra_assessment_has_rekans.rekan_id IS NOT NULL
	
	        union all 
	
            SELECT
                pra_assessments.id,
                pra_assessment_has_tims.tim_id AS as_penilai_id,
                pra_assessments.periode_id,
                pra_assessments.jenis_id,
                pra_assessments.level_id,
                tim.penilai_id,
                4 AS sesi_id,
                tim.praassessment_id,
                tim.POINT,
                tim.saran,
                tim.is_submit,
                tim.id AS assessment_id 
            FROM
                pra_assessments
                pra_assessments
                left join pra_assessment_has_tims on pra_assessment_has_tims.pra_assessment_id=pra_assessments.id AND pra_assessment_has_tims.pra_assessment_id = praassessment_id
                LEFT JOIN assessments tim ON tim.penilai_id = pra_assessment_has_tims.tim_id AND tim.praassessment_id = praassessment_id
            WHERE
                pra_assessments.id = praassessment_id AND pra_assessment_has_tims.tim_id IS NOT NULL;   
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_new_pasca_assessment');
    }
}
