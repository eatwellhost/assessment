<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PraAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pra_assessments', function (Blueprint $table){
            $table->id();
            $table->integer('pegawai_id');
            $table->integer('periode_id');
            $table->integer('jenis_id');
            $table->integer('atasan_id');
            $table->integer('rekan_id');
            $table->integer('tim_id')->nullable();
            $table->integer('level_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pra_assessments');
    }
}
