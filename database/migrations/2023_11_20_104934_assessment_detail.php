<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AssessmentDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('assessment_details', function (Blueprint $table){
            $table->id();
            $table->integer('assessment_id');
            $table->integer('question_id');
            $table->boolean('jawaban')->nullable();
            $table->text('detail')->nullable();            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('assessment_details');
    }
}
