<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionProcedureHitungAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_hitung_assessment;
            CREATE PROCEDURE procedure_hitung_assessment(pegawai_id integer, sesi_id integer, periode_id integer, praassessment_id integer, question_id integer)
            begin
            SELECT
	result.*,
	ROUND( result.jawaban/ result.pembagi, 2 ) AS hasil 
FROM
	(
		SELECT
			assessments.pegawai_id,
			assessments.sesi_id,
			assessments.praassessment_id,
			assessments.periode_id,
			assessments.level_id,
			assessment_details.question_id,
			sum( jawaban ) AS jawaban,
		CASE
				WHEN assessments.sesi_id = 2 THEN
				( SELECT count( pra_assessment_id ) FROM pra_assessment_has_atasans WHERE pra_assessment_id = praassessment_id ) 
				WHEN assessments.sesi_id = 3 or assessments.sesi_id = 4 THEN
				( select count(pra_assessment_id) from (select * from pra_assessment_has_rekans union all select * from pra_assessment_has_tims) as test WHERE pra_assessment_id = praassessment_id) 
				ELSE 1 
			END AS pembagi 
		FROM
			assessments
			LEFT JOIN assessment_details ON assessment_details.assessment_id = assessments.id 
		WHERE
			assessments.pegawai_id = pegawai_id 
			AND assessments.sesi_id = sesi_id
			AND assessments.periode_id = periode_id
			AND assessments.praassessment_id = praassessment_id
			AND assessment_details.question_id = question_id 
			GROUP BY
			assessments.pegawai_id,
			assessments.sesi_id,
			assessments.praassessment_id,
			assessments.periode_id,
			assessments.level_id,
			assessment_details.question_id 
		) AS result
			GROUP BY
			result.pegawai_id,
			result.sesi_id,
			result.praassessment_id,
			result.periode_id,
			result.level_id,
			result.question_id ,
			result.jawaban; 
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_hitung_assessment');
    }
}
