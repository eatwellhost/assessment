<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Assessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('assessments', function (Blueprint $table){
            $table->id();
            $table->integer('pegawai_id');
            $table->integer('penilai_id');
            $table->integer('sesi_id');
            $table->integer('praassessment_id');
            $table->integer('periode_id');
            $table->integer('level_id');
            $table->text('point');
            $table->text('saran');
            $table->boolean('is_submit')->default(false);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('assessments');
    }
}
