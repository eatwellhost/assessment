<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Point extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('points', function (Blueprint $table){
            $table->id();
            $table->text('question');
            $table->integer('parent_id')->nullable();
            $table->string('level');
            $table->integer('order')->nullable();
            $table->boolean('status')->default(true)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('points');
    }
}
