<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionProcedureSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_summary;
            CREATE PROCEDURE procedure_summary(praassessment_id integer, assessment_id integer)
            begin
            SELECT
                assessments.pegawai_id,
                assessments.penilai_id,
                assessments.sesi_id,
                assessments.praassessment_id,
                assessments.periode_id,
                assessments.level_id,
                assessment_details.assessment_id,
                points.parent_id,
                ROUND(avg(jawaban) ,1) as ratarata,
                sum(jawaban) as total,
				count(jawaban) as jumlah
                FROM
                assessments
                LEFT JOIN assessment_details ON assessments.id = assessment_details.assessment_id
                left join points on points.id = assessment_details.question_id
                where assessments.praassessment_id=praassessment_id and assessment_details.assessment_id = assessment_id
                group by
                assessments.pegawai_id,
                assessments.penilai_id,
                assessments.sesi_id,
                assessments.praassessment_id,
                assessments.periode_id,
                assessments.level_id,
                assessment_details.assessment_id,
                points.parent_id;
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_assessment');
    }
}
