<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewDetailPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
        CREATE VIEW view_detail_pegawai AS

        SELECT
	pegawais.*,
	level_pegawais.nama as level_pegawais,
	companys.nama as companys,
	divisions.nama as divisions,
	departments.nama as departments,
	sections.nama as sections,
	locations.nama as locations,
	employee_statuss.nama as employee_statuss
FROM
	`pegawais`
	LEFT JOIN level_pegawais ON level_pegawais.id = pegawais.level_id
	LEFT JOIN companys ON companys.id = pegawais.company_id
	LEFT JOIN divisions ON divisions.id = pegawais.division_id
	LEFT JOIN departments ON departments.id = pegawais.department_id
	LEFT JOIN sections ON sections.id = pegawais.section_id
	LEFT JOIN locations ON locations.id = pegawais.location_id
	LEFT JOIN employee_statuss ON employee_statuss.id = pegawais.employee_status_id;
        ";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP VIEW IF EXISTS view_detail_pegawai;";
        DB::statement($sql);
    }
}
