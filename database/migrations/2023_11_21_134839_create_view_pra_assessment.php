<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewPraAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
        CREATE VIEW view_pra_assessment AS

        SELECT
        pra_assessments.*,
        pegawais.nik,
        pegawais.nama,
        atasan.nama AS atasan,
        rekan.nama AS rekan,
        tim.nama AS tim,
        periodes.nama as periode,
        periodes.tgl_mulai,
        periodes.tgl_akhir,
        level_assessments.nama as level,
        jenis_assessments.nama as jenis
    FROM
        pra_assessments
        LEFT JOIN pegawais ON pegawais.id = pra_assessments.pegawai_id
        LEFT JOIN pegawais AS atasan ON atasan.id = pra_assessments.atasan_id
        LEFT JOIN pegawais AS rekan ON rekan.id = pra_assessments.rekan_id
        LEFT JOIN pegawais AS tim ON tim.id = pra_assessments.tim_id
        LEFT JOIN level_assessments ON level_assessments.id = pra_assessments.level_id
        LEFT JOIN periodes ON periodes.id = pra_assessments.periode_id
        LEFT JOIN jenis_assessments ON jenis_assessments.id = pra_assessments.jenis_id;
        ";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP VIEW IF EXISTS view_pra_assessment;";
        DB::statement($sql);
    }
}
