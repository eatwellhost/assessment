<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateNewViewPraAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
        CREATE VIEW new_view_pra_assessment AS

        SELECT
        `pra_assessments`.`id` AS `id`,
		`pra_assessments`.`pegawai_id` AS `pegawai_id`,
		`pra_assessments`.`periode_id` AS `periode_id`,
		`pra_assessments`.`jenis_id` AS `jenis_id`,
        (SELECT GROUP_CONCAT( atasan_id ) FROM pra_assessment_has_atasans WHERE pra_assessment_id = pra_assessments.id GROUP BY pra_assessment_id ) AS atasan_id, 
	    (SELECT GROUP_CONCAT( rekan_id ) FROM pra_assessment_has_rekans WHERE pra_assessment_id = pra_assessments.id GROUP BY pra_assessment_id ) AS rekan_id,
	    (SELECT GROUP_CONCAT( tim_id ) FROM pra_assessment_has_tims WHERE pra_assessment_id = pra_assessments.id GROUP BY pra_assessment_id) AS tim_id,
		`pra_assessments`.`level_id` AS `level_id`,
		`pra_assessments`.`created_at` AS `created_at`,
		`pra_assessments`.`updated_at` AS `updated_at`,
		`pegawais`.`nik` AS `nik`,
		`pegawais`.`nama` AS `nama`,
		`periodes`.`nama` AS `periode`,
		`periodes`.`tgl_mulai` AS `tgl_mulai`,
		`periodes`.`tgl_akhir` AS `tgl_akhir`,
		`level_assessments`.`nama` AS `level`,
		`jenis_assessments`.`nama` AS `jenis`
    FROM
        pra_assessments
        LEFT JOIN pegawais ON pegawais.id = pra_assessments.pegawai_id
        LEFT JOIN level_assessments ON level_assessments.id = pra_assessments.level_id
        LEFT JOIN periodes ON periodes.id = pra_assessments.periode_id
        LEFT JOIN jenis_assessments ON jenis_assessments.id = pra_assessments.jenis_id
        where pra_assessments.periode_id > 4;
        ";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP VIEW IF EXISTS new_view_pra_assessment;";
        DB::statement($sql);
    }
}
