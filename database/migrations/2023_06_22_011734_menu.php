<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Menu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('menus', function (Blueprint $table){
            $table->id();
            $table->integer('parent_id')->nullable();
            $table->string('label')->nullable();
            $table->string('link')->nullable();
            $table->integer('order')->nullable();
            $table->string('icon')->nullable();
            $table->boolean('status')->default(true)->nullable();
            $table->string('route_name')->nullable();
            $table->timestamps();

        });
        //
        Schema::create('role_menu', function (Blueprint $table){
            $table->integer('role_id');
            $table->integer('menu_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('menus');
        Schema::dropIfExists('role_menu');
    }
}
