<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFunctionProcedureNewSummary3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_new_summary;
            CREATE PROCEDURE procedure_new_summary(praassessment_id integer, sesi_id integer)
            begin
            SELECT 
    result.*, 
    (
        CASE 
            WHEN EXISTS (
                SELECT 1 
                FROM view_hierarchy_points 
                WHERE root_id = result.parent_id 
                AND orders = 2
            ) THEN 
                -- If grandchildren exist, count all descendants
                (SELECT COUNT(point_id) 
                 FROM view_hierarchy_points 
                 WHERE root_id = result.parent_id 
                 AND root_id != parent_id 
                 AND point_id != root_id) * result.pembagi
            ELSE 
                -- If no grandchildren, count only immediate children
                (SELECT COUNT(point_id) 
                 FROM view_hierarchy_points 
                 WHERE parent_id = result.parent_id) * result.pembagi
        END
    ) AS jumlah_point,
    ROUND(
        result.total / 
        (
            CASE 
                WHEN EXISTS (
                    SELECT 1 
                    FROM view_hierarchy_points 
                    WHERE root_id = result.parent_id 
                    AND orders = 2
                ) THEN 
                    -- If grandchildren exist, count all descendants
                    (SELECT COUNT(point_id) 
                     FROM view_hierarchy_points 
                     WHERE root_id = result.parent_id 
                     AND root_id != parent_id 
                     AND point_id != root_id) * result.pembagi
                ELSE 
                    -- If no grandchildren, count only immediate children
                    (SELECT COUNT(point_id) 
                     FROM view_hierarchy_points 
                     WHERE parent_id = result.parent_id) * result.pembagi
            END
        ) * result.pembagi, 
        3
    ) AS hasil_akhir
FROM (
    SELECT
        assessments.pegawai_id,
        assessments.praassessment_id,
        assessments.periode_id,
        assessments.level_id,
        assessments.sesi_id,
        hp.root_id AS parent_id,
        SUM(assessment_details.jawaban) AS total,
        CASE
            WHEN assessments.sesi_id = 2 THEN
                (SELECT COUNT(pra_assessment_id) 
                 FROM pra_assessment_has_atasans 
                 WHERE pra_assessment_id = praassessment_id)
            WHEN assessments.sesi_id IN (3, 4) THEN
                (SELECT COUNT(pra_assessment_id) 
                 FROM (
                     SELECT * 
                     FROM pra_assessment_has_rekans 
                     UNION ALL 
                     SELECT * 
                     FROM pra_assessment_has_tims
                 ) AS test 
                 WHERE pra_assessment_id = praassessment_id)
            ELSE 1 
        END AS pembagi 
    FROM
        assessments
    LEFT JOIN assessment_details 
        ON assessments.id = assessment_details.assessment_id
    LEFT JOIN view_hierarchy_points AS hp 
        ON hp.point_id = assessment_details.question_id
    WHERE
        assessments.praassessment_id = praassessment_id 
        AND assessments.sesi_id = sesi_id 
        AND parent_id IS NOT NULL
    GROUP BY
        assessments.pegawai_id,
        assessments.praassessment_id,
        assessments.periode_id,
        assessments.level_id,
        assessments.sesi_id,
        hp.root_id
) AS result 
GROUP BY
    result.pegawai_id,
    result.praassessment_id,
    result.periode_id,
    result.level_id,
    result.sesi_id,
    result.total,
    result.pembagi,
    result.parent_id;
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_new_summary');
    }
}
