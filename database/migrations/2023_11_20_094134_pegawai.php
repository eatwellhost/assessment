<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pegawais', function (Blueprint $table){
            $table->id();
            $table->string('nik');
            $table->string('nama');
            $table->string('email');
            $table->string('position');
            $table->integer('level_id');
            $table->date('join_date');
            $table->integer('company_id');
            $table->integer('division_id');
            $table->integer('department_id');
            $table->integer('section_id');
            $table->integer('location_id');
            $table->integer('employee_status_id');
            $table->boolean('status')->default(true)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pegawais');
    }
}
