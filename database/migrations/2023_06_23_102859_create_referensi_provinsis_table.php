<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferensiProvinsisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referensi_provinsis', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->boolean('is_luar_negeri')->default('0')->nullable();
            $table->integer('api_id')->nullable();
            $table->boolean('is_jabodetabek')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::unprepared("
        INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (11, 'ACEH', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (12, 'SUMATERA UTARA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (13, 'SUMATERA BARAT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (14, 'RIAU', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (15, 'JAMBI', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (16, 'SUMATERA SELATAN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (17, 'BENGKULU', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (18, 'LAMPUNG', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (19, 'KEPULAUAN BANGKA BELITUNG', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (21, 'KEPULAUAN RIAU', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (31, 'DKI JAKARTA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (32, 'JAWA BARAT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (33, 'JAWA TENGAH', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (34, 'DAERAH ISTIMEWA YOGYAKARTA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (35, 'JAWA TIMUR', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (36, 'BANTEN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (51, 'BALI', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (52, 'NUSA TENGGARA BARAT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (53, 'NUSA TENGGARA TIMUR', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (61, 'KALIMANTAN BARAT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (62, 'KALIMANTAN TENGAH', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (63, 'KALIMANTAN SELATAN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (64, 'KALIMANTAN TIMUR', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (65, 'KALIMANTAN UTARA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (71, 'SULAWESI UTARA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (72, 'SULAWESI TENGAH', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (73, 'SULAWESI SELATAN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (74, 'SULAWESI TENGGARA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (75, 'GORONTALO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (76, 'SULAWESI BARAT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (81, 'MALUKU', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (82, 'MALUKU UTARA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (91, 'PAPUA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO referensi_provinsis (id, nama, is_luar_negeri, api_id, is_jabodetabek, deleted_at, created_at, updated_at) VALUES (92, 'PAPUA BARAT', NULL, NULL, NULL, NULL, NULL, NULL);

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referensi_provinsis');
    }
}
