<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionProcedurePascaAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_pasca_assessment;
            CREATE PROCEDURE procedure_pasca_assessment(praassessment_id integer)
            begin
            SELECT
                pra_assessments.*, 
                pribadi.penilai_id,
                1 as sesi_id,
                pribadi.praassessment_id,
                pribadi.point,
                pribadi.saran,
                pribadi.is_submit,
								pribadi.id as assessment_id
                from pra_assessments 
                left join assessments pribadi on pribadi.penilai_id = pra_assessments.pegawai_id and pribadi.praassessment_id=praassessment_id
                where pra_assessments.id =praassessment_id
            union all 
            SELECT
                pra_assessments.*, 
                atasan.penilai_id,
                2 as sesi_id,
                atasan.praassessment_id,
                atasan.point,
                atasan.saran,
                atasan.is_submit,
								atasan.id as assessment_id
                from pra_assessments 
                left join assessments atasan on atasan.penilai_id = pra_assessments.atasan_id and atasan.praassessment_id=praassessment_id
                where pra_assessments.id =praassessment_id
            union all
            SELECT
                pra_assessments.*, 
                rekan.penilai_id,
                3 as sesi_id,
                rekan.praassessment_id,
                rekan.point,
                rekan.saran,
                rekan.is_submit,
			    rekan.id as assessment_id
                from pra_assessments 
                left join assessments rekan on rekan.penilai_id = pra_assessments.rekan_id and rekan.praassessment_id=praassessment_id
                where pra_assessments.id =praassessment_id
            union ALL
            SELECT
                pra_assessments.*, 
                tim.penilai_id,
                4 as sesi_id,
                tim.praassessment_id,
                tim.point,
                tim.saran,
                tim.is_submit,
								tim.id as assessment_id
                from pra_assessments 
                left join assessments tim on tim.penilai_id = pra_assessments.tim_id and tim.praassessment_id=praassessment_id
                where pra_assessments.id =praassessment_id and pra_assessments.tim_id is not null;
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_assessment');
    }
}
