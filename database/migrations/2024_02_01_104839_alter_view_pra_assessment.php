<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterViewPraAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( 'DROP VIEW IF EXISTS view_pra_assessment');
        $sql = "CREATE VIEW view_pra_assessment AS

        SELECT
        pra_assessments.*,
        pegawais.nik,
        pegawais.nama,
        periodes.nama as periode,
        periodes.tgl_mulai,
        periodes.tgl_akhir,
        level_assessments.nama as level,
        jenis_assessments.nama as jenis
    FROM
        pra_assessments
        LEFT JOIN pegawais ON pegawais.id = pra_assessments.pegawai_id
        LEFT JOIN level_assessments ON level_assessments.id = pra_assessments.level_id
        LEFT JOIN periodes ON periodes.id = pra_assessments.periode_id
        LEFT JOIN jenis_assessments ON jenis_assessments.id = pra_assessments.jenis_id
        where pra_assessments.periode_id <= 4;
        ";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP VIEW IF EXISTS view_pra_assessment;";
        DB::statement($sql);
    }
}
