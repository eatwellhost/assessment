<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionProcedureAssessmentAddSubmit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_assessment;
            CREATE PROCEDURE procedure_assessment(pegawai_id integer, penilai_id integer, sesi_id integer, periode_id integer, level integer)
            begin
            WITH RECURSIVE induk AS (SELECT
                    points.id,
                    points.question,
                    points.parent_id,
                    points.`order` AS urut,
                    points.STATUS,
					points.level,
                    assessment.pegawai_id,
                    assessment.penilai_id,
                    assessment.praassessment_id,
                    assessment.sesi_id,
                    assessment.periode_id,
                    assessment.is_submit,	    	
                    assessment.assessment_id,
                    assessment.question_id,
                    assessment.jawaban,
                    assessment.detail
                FROM
                    points
                    LEFT OUTER JOIN (
                    SELECT
                        assessments.pegawai_id,
                        assessments.penilai_id,
                        assessments.praassessment_id,
                        assessments.sesi_id,
                        assessments.periode_id,
                        assessments.is_submit,						
                        assessment_details.assessment_id,
                        assessment_details.question_id,
                        assessment_details.jawaban,
                        assessment_details.detail 
                    FROM
                        assessments
                        LEFT OUTER JOIN assessment_details ON assessment_details.assessment_id = assessments.id 
                    ) AS assessment ON assessment.question_id = points.id 
                    AND assessment.pegawai_id = pegawai_id
                    AND assessment.penilai_id = penilai_id
										AND assessment.sesi_id = sesi_id
										AND assessment.periode_id = periode_id
                WHERE
                    points.parent_id = 0 UNION
                SELECT
                    anak.*,
                    assessment.pegawai_id,
                    assessment.penilai_id,
                    assessment.praassessment_id,
										assessment.sesi_id,
										assessment.periode_id,
												assessment.is_submit,
                    assessment.assessment_id,
                    assessment.question_id,
										assessment.jawaban,
                    assessment.detail
                FROM
                    (
                    SELECT
                        points.id,
                        points.question,
                        points.parent_id,
                        points.`order` AS urut,
                        points.STATUS,
												points.level
					FROM
                        points 
                    ) anak
                    INNER JOIN induk ON induk.id = anak.parent_id
                    LEFT OUTER JOIN (
                    SELECT
                        assessments.pegawai_id,
                        assessments.penilai_id,
                        assessments.praassessment_id,
												assessments.sesi_id,
												assessments.periode_id,
												assessments.is_submit,
                        assessment_details.assessment_id,
                        assessment_details.question_id,
												assessment_details.jawaban,
                        assessment_details.detail
                    FROM
                        assessments
                        LEFT JOIN assessment_details ON assessment_details.assessment_id = assessments.id 
                    ) AS assessment ON assessment.question_id = anak.id 
                    AND assessment.pegawai_id = pegawai_id
                    AND assessment.penilai_id = penilai_id
					AND assessment.sesi_id = sesi_id
					AND assessment.periode_id = periode_id
                ) SELECT
                * 
            FROM
                induk where FIND_IN_SET(level, induk.level);
								
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_assessment');
    }
}
