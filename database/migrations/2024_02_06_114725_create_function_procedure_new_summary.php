<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionProcedureNewSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS procedure_new_summary;
            CREATE PROCEDURE procedure_new_summary(praassessment_id integer, sesi_id integer)
            begin
            SELECT result.*, 
                ((SELECT count( id ) FROM points WHERE parent_id = result.parent_id )*result.pembagi )AS jumlah_point,
                round(result.total/((SELECT count( id ) FROM points WHERE parent_id = result.parent_id )*result.pembagi),3) as hasil_akhir
            FROM
                (
                SELECT
                    assessments.pegawai_id,
                    assessments.praassessment_id,
                    assessments.periode_id,
                    assessments.level_id,
                    assessments.sesi_id,
                    points.parent_id,
                    sum( jawaban ) AS total,
                CASE
                        WHEN assessments.sesi_id = 2 THEN
                        ( SELECT count( pra_assessment_id ) FROM pra_assessment_has_atasans WHERE pra_assessment_id = praassessment_id ) 
                        WHEN assessments.sesi_id = 3 or assessments.sesi_id = 4 THEN
                        ( SELECT count( pra_assessment_id ) FROM ( SELECT * FROM pra_assessment_has_rekans UNION ALL SELECT * FROM pra_assessment_has_tims ) AS test WHERE pra_assessment_id = praassessment_id )
                        ELSE 1 
                    END AS pembagi 
                FROM
                    assessments
                    LEFT JOIN assessment_details ON assessments.id = assessment_details.assessment_id
                    LEFT JOIN points ON points.id = assessment_details.question_id 
                WHERE
                    assessments.praassessment_id = praassessment_id 
                    AND assessments.sesi_id = sesi_id
                GROUP BY
                    assessments.pegawai_id,
                    assessments.praassessment_id,
                    assessments.periode_id,
                    assessments.level_id,
                    assessments.sesi_id,
                    points.parent_id 
                ) AS result 
            GROUP BY
                result.pegawai_id,
                result.praassessment_id,
                result.periode_id,
                result.sesi_id,
                result.level_id,
                result.total,
                result.pembagi,
                result.parent_id;
			end
       ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP procedure IF EXISTS procedure_new_summary');
    }
}
