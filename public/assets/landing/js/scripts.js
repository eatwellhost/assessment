import * as bootstrap from 'bootstrap';
import jquery from 'jquery';
export default (window.$ = window.jQuery = jquery); 

(() => {
    'use strict'

    
    $(function () {
        $(window).scroll(function() 
        {
        if ($(this).scrollTop() > 200)
        {
        $('.c-topnav').addClass("sticky-top");
        }
        else
        {
        $('.c-topnav').removeClass("sticky-top");
        }
        }); 
        
    })
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    const forms = document.querySelectorAll('.loginValidation, .Validation, .registerValidation, .kontakValidation')
  
    // Loop over them and prevent submission
    Array.from(forms).forEach(form => {
      form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
  
        form.classList.add('was-validated')
      }, false)
    });

   

  })()




