    document.addEventListener("DOMContentLoaded", function () {
        // Get all elements with the 'price' class
        var sendiri = document.querySelectorAll('.sendiri');
        var atasan = document.querySelectorAll('.atasan');

        // Initialize subtotal
        var subtotalsendiri = 0;
        var subtotalatasan = 0;

        // Loop through each element and add its value to the subtotal
        sendiri.forEach(function (sendiri) {
            subtotalsendiri += parseFloat(sendiri.textContent);
        });

        // Display the subtotal
        console.log('Subtotal:', subtotalsendiri);
    });
