/**
Custom module for you to write your own javascript functions
**/
var Custom = function() {

    // private functions & variables

    var myFunc = function(ele, url) {

    }

    // public functions
    return {

        //main function
        init: function() {
            //initialize here something.
        },

        //some helper function
        doSomeStuff: function() {
            myFunc();
        }

    };

}();

var Helper = function() {

    return {

        quickAjax: function(sharedObj, url) {
            // This does the ajax request
            $.ajax({
                url: baseUrl + url,
                success:function(data) {
                    sharedObj.data = data;
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });
        },

        select2: function(ele, url, tag, placeholder, clear, min, tanggal) {
            var Dtag = (typeof tag == "undefined" || tag == false ? false : true);
            var Dpcd = (typeof placeholder == "undefined" ? '' : placeholder);
            var Dclr = (typeof clear == "undefined" ? false : clear);
            var min = (typeof min == "undefined" ? 1 : 0);
            
            $(ele).select2({
                width:'100%',
                allowClear: Dclr,
                placeholder: Dpcd,
                ajax: {
                    url: baseUrl + url,
                    dataType: 'json',
                    type: 'get',
                    delay: 2500,
                    data: function(params) {
                        console.log($params);
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: data.item
                        }
                    },
                    cache: true
                },
                tags: Dtag,
                tokeSparator: [','],
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: min,
                templateResult: formatResult, // omitted for brevity, see the source of this page
                templateSelection: formatResult, // omitted for brevity, see the source of this page
                theme: "bootstrap"
            });

            if (typeof tanggal != "undefined") {
                $(ele).on("select2:select", function(e) {
                    let data_optional = e.params.data.tanggal;
                    $(tanggal).val(data_optional);
                });
            }
        },

        select2me: function(ele) {
            $(ele).select2({
                placeholder: "Pilih",
                width:'100%',
                allowClear: true,
                closeOnSelect: true
            });
        },

        select2meAjax: function(ele, url, selected_id = null) {
            var element = $(ele);

            element.select2({
              placeholder: "Pilih",
              width:'100%',
              allowClear: true,
              escapeMarkup: function(markup) {
                  return markup;
              }, // let our custom formatter work
              closeOnSelect: true
            });

            Helper.disabled(element);

            $.ajax({
                type: 'GET',
                url: baseUrl + url
            }).then(function(data) {
                let newOption = [];

                if (selected_id == null || selected_id == '') {
                    newOption.push(new Option(null, "", true, false));
                }

                $.each(data.item, function(index, value) {

                    selected = false;
                    if (selected_id && value.id == selected_id) {
                        selected = true;
                    }
                    newOption.push(new Option(value.text, value.id, false, selected));
                });

                element.html(newOption);

                Helper.enabled(element);
            });
        },

        daterange: function(ele) {
            $(ele).daterangepicker({
                    opens: (App.isRTL() ? 'left' : 'right'),
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    startDate: moment().subtract('days', 29),
                    endDate: moment(),
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    }
                },
                function(start, end) {
                    $(ele + ' input').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                });
        },

        ckeditor: function(ele) {
            var editor = CKEDITOR.replace(ele, {
                allowedContent: true,
                extraAllowedContent: 'table[class]'
            });
            editor.on('change', function(evt) {
                // getData() returns CKEditor's HTML content.
                var dumy = evt.editor.getData();
                $('#' + ele + '_value').val(dumy);
            });
        },

        bsSelect: function() {
            $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        },

        getTableData: function(table, callback) {
            table.on('xhr', function() {
                var ajaxJson = table.ajax.json();
                callback(ajaxJson.data);
            });
        },

        onTableDraw: function(table, callback) {
            table.on('draw', function() {
                callback();
            });
        },

        submitButtonVisibility: function(datatable) {
            $("button[type=submit]").hide();
            this.getTableData(datatable, function(data) {
                if (data.length == 0 || data[0].status == "approve") {
                    $("button[type=submit]").hide();
                } else {
                    $("button[type=submit]").show();
                }
            });
        },

        disabled: function(element) {
            for (let i = 0; i < element.length; i++) {
                $(element[i]).prop("disabled", true);
            }
        },


        enabled: function(element) {
            for (let i = 0; i < element.length; i++) {
                $(element[i]).prop("disabled", false);
            }
        },

        clear: function(element) {
            for (let i = 0; i < element.length; i++) {
                $(element[i]).val('').change();
            }
        },

        show: function(elementToShow) {
            $(elementToShow).show('fast');
            this.enabled([$(elementToShow).find('input, select, textarea')]);
        },

        hide: function(elementToHide) {
            $(elementToHide).hide('fast');
            this.disabled([$(elementToHide).find('input, select, textarea')]);
        },

        checked: function(element) {
            for (let i = 0; i < element.length; i++) {
                $(element[i]).prop("checked", true).change();
            }
        },

        mtCheckBox: function(ele, val) {
            $(ele).find('input[type="radio"]').each(function() {
                inp = $(this).val();
                if (inp == val) $(this).attr("checked", true);
            });
        },

        onTableCheked: function(table) {
            $('.table-container').on('change', '.checked_all', function(e) {
                if ($(this).is(':checked')) {
                    $('.checked_item').prop("checked", true).change();
                } else {
                    $('.checked_item').prop("checked", false).change();
                }
            })

            let list_checked = [];
            Helper.onTableDraw(table, function() {
                $('.table-container .checked_all').prop("checked", false);
                $('.table-container .checked_item').each(function(index, item) {
                    if (list_checked.indexOf($(item).val()) > -1) {
                        $(item).prop("checked", true);
                    }
                })
            });

            $('.table-container').on('change', '.checked_item', function(e) {
                let item = $(this).val();
                if ($(this).is(":checked")) {
                    if (list_checked.indexOf($(this).val()) == -1) {
                        list_checked.push(item);
                    }
                } else {
                    list_checked.splice(list_checked.indexOf(item), 1);
                }

                $(".checked_status").val(list_checked);
            })
        },

        select2Selected: function(ele, data) {
            $(ele).select2("trigger", "select", {
                data: data
            });
        },

        select2SelectedFetch: function(ele, url) {
            var promise = new Promise(function(resolve, reject) {
                $.get(baseUrl + url, function(json) {
                    $.each(json.item, function(index, value) {
                        if ($(ele).data('select2')) {
                            $(ele).select2("trigger", "select", {
                                data: value
                            });
                        }
                    });
                    resolve();
                }).fail(function(xhr) {
                    console.log("select2SelectedFetch gagal");
                    reject();
                });
            });

            return promise;
        },

        tabMenu: function() {
            $("a[data-toggle=tab]").on("click", function(e) {
                const href = $(this).attr("href");

                $.each($(".actions").children("a"), function(index, value) {
                    let dataTab = $(value).attr("data-tab");
                    if (typeof dataTab !== typeof undefined && dataTab !== false) {
                        if (dataTab == href) {
                            $(value).show();
                        } else {
                            $(value).hide();
                        }
                    }
                })
            });
        },

        getProvinsi: function() {
            $('.toggle-provinsi').on("click", function() {
                let is_checked = $('.toggle-provinsi').is(':checked');

                Helper.clear(['.provinsi', '.kota']);
                Helper.disabled(['.kota']);
                if (is_checked) {
                    Helper.select2meAjax('.provinsi', "fetch/provinsi?is_luar_negeri=true");
                } else {
                    Helper.select2meAjax('.provinsi', 'fetch/provinsi');
                }
            })
        },

        getKota: function() {
            $('.provinsi').on('change', function() {
                let parent_id = $('.provinsi').val();

                Helper.clear(['.kota']);
                if (parent_id == null || parent_id == '') {
                    Helper.disabled([".kota"]);
                } else {
                    Helper.enabled([".kota"]);
                    Helper.select2meAjax(".kota", "fetch/kota?provinsi_id=" + parent_id);
                }
            });
        },

        getModal: function(url) {
            App.blockUI();

            $.get(url, function(html) {
                $('#modal .modal-dialog').attr('class', options.modal_size);
                $('#modal .modal-content').html(html);

                App.unblockUI();
                $('#modal').modal('show');
            }).fail(function(xhr) {

                if (xhr.status == 500 || xhr.status == 401) {
                    window.location = url;
                } else {
                    pageContentBody.html(xhr.responseText);
                    App.unblockUI();
                    console.log("Gagal");
                }
            });

            return false;
        },

        convert_angka: function(str) {
            if(typeof str !== "string") {
                str = str.toString();
            }

            var ret = [];
            var jml = str.length;
            for(i=0; i<(jml/3); i++) {
              var res = str.substring(str.length-((i+1)*3), str.length-(i*3));
                ret.push(res);
            }
            ret.reverse();
            ret = ret.join(".");
            return ret;
        },

        convert_rupiah: function(str) {
            str = convert_angka(str);
            str = "Rp. " + str;
        },

        myeditable: function(ele, isAutoSubmit = true) {
            $(ele).css('-webkit-appearance', 'none');
            $(ele).css('border', 'transparent');
            $(ele).css('border-radius', 0);
            $(ele).css('border-bottom', '1px solid #ccc');
            $(ele).css('cursor', 'pointer');
            $(ele).css('background-color', 'transparent');

            $(ele).focusin(function(){
                $(this).toggleClass('myeditable')
            });
            $(ele).focusout(function(e){
                $(this).toggleClass('myeditable')
            });

            if(isAutoSubmit){
                $(ele).change(function(){
                    $(this).closest('form').submit();
                });
            }
        },

        convert_tanggal: function(str){
            var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            var tanggal = new Date(str).getDate();
            var _bulan = new Date(str).getMonth();
            var _tahun = new Date(str).getYear();

            var bulan = bulan[_bulan];

            var tahun = (_tahun < 1000) ? _tahun+1900 : _tahun;

            return tanggal + ' ' + bulan + ' ' + tahun;
        }
    };

    function formatResult(result) {
        return result.text;
    }

}();

var Datepicker = function() {

    return {
        picker: function(clas, attr) {
            $(clas).datepicker(attr);
        },
        full: function(clas) {
            $(clas).datepicker({
                format: "dd MM yyyy",
                language: "id",
                todayHighlight: true,
                beforeShowDay: function(date) {
                    if (date.getDay() == "0" || date.getDay() == "6") {
                        return { tooltip: "Libur", classes: "font-red bold libur tooltips" };
                    }
                    date_now = date.getFullYear() + "-" + pad(date.getMonth() + 1, 2) + "-" + pad(date.getDate(), 2);
                    var b = $.inArray(date_now, hari_libur_tgl_arr);
                    if (b != -1) {
                        if (hari_libur[b].jenis == 2) {
                            return { tooltip: hari_libur[b].keterangan, classes: "font-blue bold libur tooltips" };
                        }
                        if (hari_libur[b].jenis == 1) {
                            return { tooltip: hari_libur[b].keterangan, classes: "font-red bold libur tooltips" };
                        }
                    }
                }
            });
        },
        day: function(clas) {
            $(clas).datepicker({
                format: "DD, dd MM yyyy",
                language: "id",
                todayHighlight: true
            });
        },
        year: function(clas) {
            $(clas).datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                todayHighlight: true,
                autoclose: true,
            });
        },
        month: function(clas) {
            $(clas).datepicker({
                format: "mm",
                viewMode: "months",
                minViewMode: "months"
            });
        },
        month2: function(clas) {
            $(clas).datepicker({
                format: "MM",
                language: 'id',
                viewMode: "months",
                minViewMode: "months",
                autoclose: true,
            });
        },
        monthYear: function(clas) {
            $(clas).datepicker({
                format: 'MM yyyy',
                language: 'id',
                viewMode: "months",
                minViewMode: "months",
                autoclose: true,
                orientation: 'bottom'
            });
        }
    };
}();

var Timepicker = function() {
    return {
        picker: function(clas) {
            $(clas).timepicker({
                autoclose: !0,
                minuteStep: 5,
                showSeconds: !1,
                showMeridian: !1
            });
        }
    }
}();

var Daterangepicker = function() {
    return {
        rangepicker: function(clas, attrrange) {
            $(clas).daterangepicker(attrrange);

            if (typeof attrrange != 'undefined') {
                if (attrrange.autoUpdateInput == false) {
                    $(clas).on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                    });

                    $(clas).on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });
                }
            }
        }
    }
}();

var Modal = function() {
    return {
        show: function(is_show) {
            if (is_show == "true") {
                $('a.btn-add').click();
            }
        }
    }
}();

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
};

(function($) {
    $.fn.setDisable = function(disable) {
        if (disable) {
            if (this.is("input, select, textarea")) {
                this.attr("disabled", true);
            } else {
                this.find("input, select, textarea").attr("disabled", true);
            }
        } else {
            if (this.is("input, select, textarea")) {
                this.attr("disabled", false);
            } else {
                this.find("input, select, textarea").attr("disabled", false);
            }
        }
    };
})(jQuery);

jQuery(document).ready(function() {
    moment.locale('id');
});

/***
Usage
***/
//Custom.doSomeStuff();

// Custom Function
