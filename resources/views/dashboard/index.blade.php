@extends('layouts.admin.app')

@section('content')
{{-- <div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-fluid">
        <!--begin::Card-->
        <div class="card shadow-sm ">
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Home card-->
								<div class="card">
									<!--begin::Body-->
									<div class="card-body p-lg-20">
										<!--begin::Section-->
										<div class="mb-17">
											<!--begin::Title-->
											<h3 class="text-dark mb-7">Homepage Kementerian BUMN @2022</h3>
											<!--end::Title-->
											<!--begin::Separator-->
											<div class="separator separator-dashed mb-9"></div>
											<!--end::Separator-->
											<!--begin::Row-->
											<div class="row">
												<!--begin::Col-->
												<div class="col-md-6">
													<!--begin::Feature post-->
													<div class="h-100 d-flex flex-column justify-content-between pe-lg-6 mb-lg-0 mb-10">
														<!--begin::Video-->
														<div class="mb-3">
															<iframe class="embed-responsive-item card-rounded h-275px w-100" src="https://www.youtube.com/embed/53agHpqOGj8" allowfullscreen="allowfullscreen"></iframe>
														</div>
														<!--end::Video-->
													</div>
													<!--end::Feature post-->
												</div>
												<!--end::Col-->
                                                												<!--begin::Col-->
												<div class="col-md-6">
													<!--begin::Feature post-->
													<div class="h-100 d-flex flex-column justify-content-between pe-lg-6 mb-lg-0 mb-10">
														<!--begin::Video-->
														<div class="mb-3">
															<iframe class="embed-responsive-item card-rounded h-275px w-100" src="https://www.youtube.com/embed/BuMkxcoKp5E" allowfullscreen="allowfullscreen"></iframe>
														</div>
														<!--end::Video-->
													</div>
													<!--end::Feature post-->
												</div>
												<!--end::Col-->

											</div>
											<!--begin::Row-->
										</div>
										<!--end::Section-->

									</div>
									<!--end::Body-->
								</div>
								<!--end::Home card-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
        </div>
    </div>
</div> --}}
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card body-->
            <div class="card-body p-0">
                <!--begin::Wrapper-->
                <div class="card-px text-center py-10">
					<h3 style="text-align: left">Good Day, Welcome {{$user}}</h3><br>
					@if($periode)
					@foreach($param as $key =>$value)
						<div class="card-body" style="padding: 5px !important">
							<div class="d-flex align-items-center bg-light-success rounded p-3">
								<!--begin::Icon-->
								<span class="svg-icon svg-icon-warning mr-5">
									<span class="svg-icon svg-icon-warning svg-icon-4x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo1/dist/../src/media/svg/icons/Code/Warning-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<rect x="0" y="0" width="24" height="24"/>
											<path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"/>
											<rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"/>
											<rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"/>
										</g>
										</svg>
									</span>
								</span>
								<!--end::Icon-->
					
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1 mr-2">
									<span class="text-muted font-weight-bold text-hover-primary font-size-xxl mb-1" style="color: #1BC5BD !important;font-size: 16px;font-weight:bold;">{{$value}}</span>
								</div>
								<!--end::Title-->
							</div>
						</div>
					@endforeach
					@else
					<div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert">
						<div class="alert-icon"><span class="svg-icon svg-icon-danger svg-icon-4x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo1/dist/../src/media/svg/icons/Code/Warning-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24"/>
								<path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"/>
								<rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"/>
								<rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"/>
							</g>
						</svg><!--end::Svg Icon--></span></div>
						<div class="alert-text"><b style="font-size: 16px;font-weight:bold;">Tidak Ada Periode Pengisian Assessment Yang Aktif</b></div>
					</div>
					@endif
					<div class="row mt-10">
						<div class="col-xl-3">
							<div class="card card-custom card-stretch gutter-b">
								<div class="card-body d-flex p-0">
									<div class="flex-grow-1 {{$praAssessment == 0 ? 'bg-danger' : 'bg-success'}} p-8 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/humans/custom-3.svg)">
							
										<h3 class="text-inverse-danger mt-2 font-weight-bolder">Asessment</h3>
							
											<p class="text-inverse-danger my-6" style="font-size: 16px;font-weight: bold;">
											{{$praAssessment}}
											</p>
							
										<a href="{{route('assessment.index')}}" class="btn btn-warning font-weight-bold py-2 px-6">Lihat</a>
									</div>
								</div>
							</div>
						</div>
						@if(Auth()->user()->hasRole('Super Admin') || Auth()->user()->hasRole('Human Capital'))
						<div class="col-xl-3">
							<div class="card card-custom card-stretch gutter-b">
								<div class="card-body d-flex p-0">
									<div class="flex-grow-1 {{$sudahisi == 0 ? 'bg-danger' : 'bg-success'}} p-8 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/humans/custom-3.svg)">
							
										<h3 class="text-inverse-danger mt-2 font-weight-bolder">Pegawai Penilai</h3>
							
											<p class="text-inverse-danger my-6" style="font-size: 16px;font-weight: bold;">
											{{$sudahisi}}
											</p>
							
										<a href="{{route('laporan.assessment.index')}}" class="btn btn-warning font-weight-bold py-2 px-6">Lihat</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3">
							<div class="card card-custom card-stretch gutter-b">
								<div class="card-body d-flex p-0">
									<div class="flex-grow-1 {{$nonkomplit == 0 ? 'bg-danger' : 'bg-success'}} p-8 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/humans/custom-3.svg)">
							
										<h3 class="text-inverse-danger mt-2 font-weight-bolder">Belum Komplit</h3>
							
											<p class="text-inverse-danger my-6" style="font-size: 16px;font-weight: bold;">
											{{$nonkomplit}}
											</p>
							
										<a href="{{route('laporan.assessment.index')}}" class="btn btn-warning font-weight-bold py-2 px-6">Lihat</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3">
							<div class="card card-custom card-stretch gutter-b">
								<div class="card-body d-flex p-0">
									<div class="flex-grow-1 {{$komplit == 0 ? 'bg-danger' : 'bg-success'}} p-8 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/humans/custom-3.svg)">
							
										<h3 class="text-inverse-danger mt-2 font-weight-bolder">Sudah Komplit</h3>
							
											<p class="text-inverse-danger my-6" style="font-size: 16px;font-weight: bold;">
											{{$komplit}}
											</p>
							
										<a href="{{route('laporan.assessment.index')}}" class="btn btn-warning font-weight-bold py-2 px-6">Lihat</a>
									</div>
								</div>
							</div>
						</div>
						@endif
						
					</div>
                </div>
				
                <!--end::Wrapper-->
                <!--begin::Illustration-->
                
                <!--end::Illustration-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content container-->
</div>

@endsection

@section('addafterjs')
@endsection