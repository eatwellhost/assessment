								
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									{{-- <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
									
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start ms-3 mx-2" id="page-title">{!!$title?$title:''!!}</span>
									<!--end::Separator-->
									<!--begin::Description-->
									<small class="text-muted fs-7 fw-bold my-1 ms-1"  id="page-breadcrumb">{!!$breadcrumb?$breadcrumb:''!!}</small>
									<!--end::Description--></h1> --}}
									<!--end::Title-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<li class="breadcrumb-item text-muted">
											<strong>
											<!--begin::Svg Icon | path: /var/www/preview.keenthemes.com/kt-products/docs/metronic/html/releases/2022-10-09-043348/core/html/src/media/icons/duotune/general/gen008.svg-->
											<span class="svg-icon svg-icon-2 svg-icon-lg-1">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
													<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor"></rect>
													<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor"></rect>
													<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor"></rect>
													<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor"></rect>
												</svg>
											</span>
											<!--end::Svg Icon-->
											</strong>
											&nbsp;
										</li>
										<li class="breadcrumb-item text-muted">
											&nbsp;
											<strong>
											<a href="{{url('/')}}" class="text-muted text-hover-primary">HOME</a>&nbsp;
											</strong>
										</li>
									 
										{{-- @for($i = 2; $i <= count(Request::segments()); $i++)
										   <li>
											@if($i == 2)
											  <a href="#" class="text-muted">
												<strong>
												<i class="fas fa-angle-double-right text-warning"></i> 
												{{strlen(strtoupper(Request::segment($i))) > 20 ? '' : strtoupper(Request::segment($i))}} 
												</strong>
											  </a>
											  &nbsp;
											@else
												<a href="{{ URL::to( implode( '/', array_slice(Request::segments(), 0 ,$i, true)))}}" class="text-muted text-hover-primary">
												<strong>
												<i class="fas fa-angle-double-right text-warning"></i> 
												{{strlen(strtoupper(Request::segment($i))) > 20 ? '#' : strtoupper(Request::segment($i))}}
												</strong>
											  </a>
											  &nbsp;
											@endif
										   </li>
										@endfor --}}
										@isset($breadcrumb_arr)
											<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
												<!--begin::Item-->
												@foreach ($breadcrumb_arr as $item)
													@if ($item['url']!='')
													<li class="breadcrumb-item text-muted">
														<a href="{{ $item['url'] }}" class="text-muted text-hover-primary">{{ $item['menu'] }}</a>
													</li>
													<li class="breadcrumb-item">
														<span class="bullet bg-warning w-5px h-2px"></span>
													</li>
													@else
													<li class="breadcrumb-item text-dark">{{ $item['menu'] }}</li>
													@endif
												@endforeach
											</ul>
										@endisset
									</ul>
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
									
									</ul>
									<div class="d-flex justify-content-left">    
									</div>
								</div>
								<!--end::Page title-->
								