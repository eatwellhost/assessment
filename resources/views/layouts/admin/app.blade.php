<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(config('app.env') !== 'local')
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    @endif
    <title>Assessment Employee Eatwell</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="">
    <link rel="canonical" href="">
    <link rel="shortcut icon" href="{{asset('/assets/media/logos/polos.png')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700">
    <!--end::Fonts-->

    @yield('addbeforecss')
    <link href="{{ asset('/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('/assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/assets/plugins/jquery-treegrid-master/css/jquery.treegrid.css')}}" rel="stylesheet" type="text/css" />


    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="{{ asset('/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{ asset('/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/plugins/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('/assets/css/datatables.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('/assets/plugins/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body"
    class="page-loading-enabled page-loading header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">

    <!--layout-partial:layout/_loader.html-->
    @include('layouts.admin.layout._loader')

    <!--layout-partial:layout/master.html-->
    @include('layouts.admin.layout.master')

    
    <div class="modal fade" id="winformlebar" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-1000px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder modal-title">Modal Title</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary modal-close" data-bs-dismiss="modal"
                        data-kt-roles-modal-action="close">
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                    fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5"
                                        transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                        x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="winform" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-750px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder modal-title">Modal Title</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary modal-close" data-bs-dismiss="modal"
                        data-kt-roles-modal-action="close">
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                    fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5"
                                        transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                        x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="winformkecil" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-500px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder modal-title">Modal Title</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary modal-close" data-bs-dismiss="modal" data-kt-roles-modal-action="close">
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5 my-7">
                </div>
            </div>
        </div>
    </div>
	<!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="{{ asset('/assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('/assets/plugins/jquery-treegrid-master/js/jquery.treegrid.js')}}"></script>
    {{-- <script src="{{ asset('assets/js/custom/custom.js') }}" type="text/javascript"></script> --}}
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="{{ asset('/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript">
    </script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="{{ asset('/assets/js/custom/widgets.js') }}"></script>
    <script src="{{ asset('/assets/js/custom/apps/chat/chat.js') }}"></script>
    <script src="{{ asset('/assets/js/custom/modals/create-app.js') }}"></script>
    <script src="{{ asset('/assets/js/custom/modals/upgrade-plan.js') }}"></script>
    {{-- <script src="{{ asset('/assets/plugins/custom/formrepeater/formrepeater.bundle.js')}}"></script> --}}
    <script src="{{ asset('/assets/js/jquery.repeater.js')}}"></script>
    {{-- <script src="{{ asset('/assets/js/custom/intro.js') }}"></script> --}}
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->
    
    <!--end::Page Scripts -->
    @yield('script')
    <script type="text/javascript" src="{{asset('assets/js/generalfunction.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables.bundle.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/jquery.inputmask.bundle.js')}}"></script>

    {{-- <script type="text/javascript" src="{{asset('assets/plugins/datatables.js')}}"></script> --}}
    <script type="text/javascript" src="{{asset('assets/plugins/jquery.blockUI.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/jquery.form.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/webcam-easy.min.js')}}"></script>
    <script type="text/javascript">
        baseUrl = '{{ url('/') }}/';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
    </script>
    @yield('addafterjs')
</body>
<!--end::Body-->

</html>
