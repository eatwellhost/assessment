<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Error 404 - Page Not Found!</title>
    <link href="{{ asset('/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{asset('/assets/media/logos/polos.png')}}">
    <style>
        body {
    background: #fff;
    padding: 0;
    margin: 0;
    font-family: Helvetica, Arial, sans-serif;
}

.container {
    background-color: #fff;
    margin: 0 auto;
    text-align: center;
    padding-top: 50px;
}

h3 {
    font-size: 16px;
    color: #3498db;
    font-weight: bold;
    text-align: center;
    line-height: 130%;
}

.buton {
    background: #3498db;
    padding: 10px 20px;
    color: #fff;
    font-weight: bold;
    text-align: center;
    border-radius: 3px;
    text-decoration: none;
}

a:hover {
    color: #ff0;
}

span {
    font-size: 14px;
    color: #FFF;
    font-weight: normal;
    text-align: center;
}

span a {
    color: #FF0;
    text-decoration: none;
}

span a:hover {
    color: #F00;
}

@media screen and (max-width: 500px) {
    img {
        width: 70%;
    }
    .container {
        padding: 70px 10px 10px 10px;
    }
    h3 {
        font-size: 14px;
    }
}
    </style>
</head>

<body>
    <div class="container">
        <img class="ops" style="height:350px" src="/assets/media/illustrations/404error.png" />
        <br />
        <h3>Halaman yang Anda cari tidak ditemukan.
            <br /> Bisa jadi karena url tersebut salah atau tidak tersedia.</h3>
        <br />
        {{-- <a class="buton" href="https://indrakusuma.web.id/">www.indrakusuma.web.id</a> --}}
    </div>
</body>

</html>