


<html>

<head>

<style type="text/css">
    body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small ;overflow-wrap: break-word;word-wrap: break-word;}
    a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
    a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
    comment { display:none;  } 
    #table-nilai{border: 10px solid black;border-collapse: collapse;}
    .square-box {border:solid 3px black;width:150px;height:150px;}
    b{font-size: "arial"; font-size:"12px"}
</style>


</head>

<body>
<table border="0">
<tr>
    <td rowspan="5" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000;" align="center" valign="middle"></td>
    <td rowspan="5" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000;" align="center" valign="middle"></td>
    <td colspan="5" rowspan="5" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;" align="center" valign="middle"><div style="margin-right: 100px"></div></td>
    <td colspan="2" rowspan="5" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign="middle"><b><font face="Arial" size=6 color="#000000">FORM PENILAIAN KERJA <br/>METODE 360 DERAJAT</font></b></td>
    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">Tipe Dokumen</td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">:  RAHASIA</td>
</tr>
<tr>
    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">No. Dokumen</td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">:  FRM/HC/PER/001</td>
</tr>
<tr>
    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">Revisi</td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">:  2</td>
</tr>
<tr>
    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">Tanggal Efektif</td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">:  4 Maret 2022</td>
</tr>
<tr>
    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">Halaman</td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" valign="middle">:  3</td>
</tr>
</table>
<table>
    <tr></tr>
    <tr>
        <td colspan="12" style="background-color : #e3e3e3; height:35px;" align="center" valign=middle ><b><font face="Arial" size=7 color="#000000">DATA KARYAWAN</font></b></td>
    </tr>
    <tr>
        <td colspan="5" valign=middle ><font face="Arial" size=3 color="#000000">Nama Karyawan</font></td>
        <td colspan="3" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->nama}}</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">NIK</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->nik}}</font></td>
    </tr>
    <tr>
        <td colspan="5" valign=middle ><font face="Arial" size=3 color="#000000">Divisi</font></td>
        <td colspan="3" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->divisions}}</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">Department</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->departments}}</font></td>
    </tr>
    <tr>
        <td colspan="5" valign=middle ><font face="Arial" size=3 color="#000000">Jabatan Awal (Saat Masuk)</font></td>
        <td colspan="3" valign=middle ><font face="Arial" size=3 color="#000000">:  </font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">Jabatan Saat Ini</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->position}}</font></td>
    </tr>
    <tr>
        <td colspan="5" valign=middle ><font face="Arial" size=3 color="#000000">Status Karyawan</font></td>
        <td colspan="3" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->employee_statuss}}</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">Lokasi Kerja</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->locations}}</font></td>
    </tr>
    <tr>
        <td colspan="5" valign=middle ><font face="Arial" size=3 color="#000000">Tanggal Bergabung</font></td>
        <td colspan="3" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$joindate}}</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">Brand / Perusahaan</font></td>
        <td colspan="2" valign=middle ><font face="Arial" size=3 color="#000000">:  {{$pegawai->companys}}</font></td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td colspan="5" style="background-color : #e3e3e3; height:25px;" valign=middle ><b><font face="Arial" size=3 color="#000000">JENIS PENILAIAN KERJA</font></b></td>
        <td colspan="7" style="background-color : #e3e3e3; height:25px;" valign=middle ><b><font face="Arial" size=3 color="#000000">:  {{$praassessment->jenis}}</font></b></td>
    </tr>
    <tr></tr>
    <tr><td colspan="12" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle ><b><font face="Arial" size=3 color="#000000">EVALUASI KINERJA</font></b></td></tr>
    @foreach($assessment as $key => $valassessment)
        @if($valassessment->point != NULL)
            <tr>
                <td colspan="12">* {!!$valassessment->point!!}</td>
            </tr>
        @endif
    @endforeach
    <tr></tr>
    <tr><td colspan="12" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">SARAN PERBAIKAN DAN PENGEMBANGAN</font></b></td></tr>
    @foreach($assessment as $key => $valassessment)
        @if($valassessment->saran != NULL)
            <tr>
                <td colspan="12">* {!!$valassessment->saran!!}</td>
            </tr>
        @endif
    @endforeach
    <tr></tr>
    <tr>
        <td colspan="12" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">HASIL PENILAIAN KESELURUHAN</font></b></td>
    </tr>
    <tr>
        <td colspan="7" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">TOTAL NILAI</font></b></td>
        <td colspan="5" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">KATEGORI NILAI</font></b></td>
    </tr>
    <tr>
        <td colspan="7" style="height: 70px;"  align="center" valign=middle><b><font face="Arial" size=3 color="#000000">{{$summary['total']}}</font></b></td>
        <td colspan="5" style="height: 70px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">{{$summary['totaltext']}}</font></b></td>
    </tr>
    <tr></tr>
    <tr>
        <td colspan="8" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">RANGE NILAI</font></b></td>
        <td colspan="4" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">KATEGORI NILAI</font></b></td>
    </tr>
    @foreach($standar as $keys => $vals)
    <tr>
        <td colspan="8" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle>{{$vals->batas_bawah}} - {{$vals->batas_atas}}</td>
        <td colspan="4" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle>{{$vals->nama}}</td>
    </tr>
    @endforeach
    <tr></tr>
    <tr><td colspan="12" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">KEPUTUSAN PENILAIAN</font></b></td></tr>
    <tr><td colspan="12" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Status Karyawan</font></b></td></tr>
    <tr>
        <td colspan="8" valign=middle><b><font face="Arial" size=2 color="#000000">&#11036; Dapat diperpanjang sebagai karyawan kontrak dengan jangka waktu:</font></b></td>
        <td colspan="4" valign=middle><b><font face="Arial" size=2 color="#000000">&#11036; Tidak dapat diperpanjang sebagai karyawan kontrak</font></b></td>
    </tr>
    <tr>
        <td colspan="8" valign=middle><b><font face="Arial" size=2 color="#000000">&#11036; Dapat diangkat menjadi karyawan tetap</font></b></td>
        <td colspan="4" valign=middle><b><font face="Arial" size=2 color="#000000">&#11036; Tidak lulus masa percobaan</font></b></td>
    </tr>
    <tr>
        <td colspan="8" valign=middle><b><font face="Arial" size=2 color="#000000">&#11036; Promosi Jabatan</font></b></td>
        <td colspan="4" valign=middle><b><font face="Arial" size=2 color="#000000">&#11036; Demosi Jabatan</font></b></td>
    </tr>
<tr></tr>
<tr></tr>
<tr><td colspan="12" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Verifikator Penilaian</font></b></td></tr>
<tr>
    <td colspan="10" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Menyetujui</font></b></td>
    <td colspan="2" style="background-color : #e3e3e3; height:25px;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Mengetahui</font></b></td>
</tr>
<tr>
    <td colspan="5" style="border-top: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Yang Dinilai</font></b></td>
    <td colspan="3" style="border-top: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Penilai</font></b></td>
    <td colspan="2" style="border-top: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Management</font></b></td>
    <td colspan="2" style="border-top: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle></td>
</tr>
<tr>
    <td colspan="5" style="border-left: 2px solid #000000; border-right: 1px solid #000000; height: 150px;" align="center" valign=middle></td>
    <td colspan="3" style="border-left: 2px solid #000000; border-right: 1px solid #000000; height: 150px;" align="center" valign=middle></td>
    <td colspan="2" style="border-left: 2px solid #000000; border-right: 1px solid #000000; height: 150px;" align="center" valign=middle></td>
    <td colspan="2" style="border-left: 2px solid #000000; border-right: 1px solid #000000; height: 150px;" align="center" valign=middle></td>

</tr>
<tr>
    <td colspan="5" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Karyawan/ti</font></b></td>
    <td colspan="3" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Atasan Langsung</font></b></td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Atasan Atasannya Langsung</font></b></td>
    <td colspan="2" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">HC Manager</font></b></td>
</tr>
<tr></tr>
<tr><td style="color: white;">split</td></tr>
</table>
<table>
    <tr>
        <td colspan="12" style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=4 color="#000000">LEMBAR PENILAIAN</font></b></td>
    </tr>
    <tr>
        <td colspan="12" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:70px" align="justify" valign=middle>Pada lembar ini berisikan mengenai indikator dan bobot penilaian. Karyawan yang akan dinilai, WAJIB mengisi terlebih dahulu mengisi sebelum atasan memberikan penilaian. Diharapkan dalam mengisi penilaian dalam kondisi baik, tidak memiliki kecenderungan untuk memihak dan profesional. Segala bentuk penilaian hanya akan berlaku di lingkungan kerja, tidak akan mempengaruhi hubungan personal antara pemberi nilai dan penerima nilai.</td>
    </tr>
    <tr></tr>
    <tr>
        <td colspan="9" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:20px" align="justify" valign=middle>Poin penilaian adalah sebagai berikut:</td>
    </tr>
    @foreach($poin as $key => $poins)
    <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; " align="justify" valign=middle>{{$poins->poin}}</td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="justify" valign=middle>:</td>
        <td colspan="7" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="justify" valign=middle>{{$poins->keterangan}}</td>
    </tr>
    @endforeach
    <tr></tr>
    <tr></tr>
    <tr>
        <td colspan="9" rowspan="2" style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">Indikator Penilaian</font></b></td>
        <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:45px;" align="center" valign=middle><p><font face="Arial" size=3 color="#000000">Penilaian Diri Sendiri</font></p></td>
        <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:45px;" align="center" valign=middle><p><font face="Arial" size=3 color="#000000">Penilaian Atasan</font></p></td>
        <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:45px;" align="center" valign=middle><p><font face="Arial" size=3 color="#000000">Penilaian Rekan Kerja / Tim</font></p></td>
    </tr>
    <tr>
        <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">20%</font></b></td>
        <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">50%</font></b></td>
        <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000;" align="center" valign=middle><b><font face="Arial" size=3 color="#000000">30%</font></b></td>
    </tr>
    @php
    // Convert $datas to an array if it's a collection
    $datasArray = $datas->toArray();

    // Count occurrences of each parent_id
    $parentCounts = array_count_values(array_column($datasArray, 'parent_id'));
    $parentProcessed = []; // To track processed parent_ids
@endphp

@foreach ($datas as $key => $vals)
    {{-- Debugging output if needed --}}
    {{-- @php dd($vals); @endphp --}}
    @if ($vals['parent_id'] == 0)
        <tr>
            <td colspan="12" style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:40px" align="center" valign="middle">
                <b><font face="Arial" size=3 color="#000000">{!! $vals['question'] !!}</font></b>
            </td>
        </tr>
        @php $numbering = 1; @endphp
    @elseif ($vals['kepala'] == 24)
        <tr>
            <td colspan="12" style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height:40px" align="center" valign="middle">
                <b><font face="Arial" size=2 color="#000000">{!! $vals['question'] !!}</font></b>
            </td>
        </tr>
        {{-- @php $numbering = 1; @endphp --}}
    @else
        <tr>
            <td style="height: 45px" valign="middle">{{ $numbering }}</td>
            <td colspan="8" style="height: 45px" valign="middle">{{ $vals['question'] }}</td>
            <td class="sendiri" style="height: 45px" valign="middle">{{ $vals['jawabansendiri'] }}</td>
            <td class="atasan" style="height: 45px" valign="middle">{{ $vals['jawabanatasan'] }}</td>
            <td class="tim" style="height: 45px" valign="middle">{{ $vals['jawabanrekan'] }}</td>
        </tr>

        {{-- Check if this is the last occurrence of the current parent_id --}}
        @php
            $parentProcessed[$vals['parent_id']] = ($parentProcessed[$vals['parent_id']] ?? 0) + 1;
            $isLastForParent = $parentProcessed[$vals['parent_id']] == ($vals['parent_id'] == 24 ? 12 :$parentCounts[$vals['parent_id']]);
        @endphp

        @if ($isLastForParent)
            <tr class='mb-1 summary'>
                <td colspan="9" rowspan="2" style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height: 30px" align="center" valign="middle">
                    <b><font face="Arial" size=3 color="#000000">NILAI</font></b>
                </td>
                <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height: 30px" align="center" valign="middle">
                    {{ $result[$vals['parent_id']]['avgsendiri'] }}
                </td>
                <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height: 30px" align="center" valign="middle">
                    {{ $result[$vals['parent_id']]['avgatasan'] }}
                </td>
                <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height: 30px" align="center" valign="middle">
                    {{ $result[$vals['parent_id']]['rekantim'] }}
                </td>
            </tr>
            <tr class='mb-1 summary'>
               
                <td style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height: 40px" align="center" valign="middle">
                    {{ $result[$vals['parent_id']]['hasilakhir'] }}
                </td>
                <td colspan="2" style="background-color : #e3e3e3; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000; height: 40px" align="center" valign="middle">
                    <b>{{ $result[$vals['parent_id']]['hasiltext'] }}</b>
                </td>
            </tr>
            <tr></tr>
            @php $currentCategory = $vals['parent_id']; @endphp
        @endif

        @php $numbering++; @endphp
    @endif
@endforeach



</table>

</body>

</html>
