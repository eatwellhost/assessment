<form class="kt-form kt-form--label-right" method="POST" id="form-edit">
	@csrf
	<input type="hidden" name="id" id="id" readonly="readonly" value="{{$actionform == 'update'? (int)$data->id : null}}" />
	<input type="hidden" name="actionform" id="actionform" readonly="readonly" value="{{$actionform}}" />

    <div class="form-group row mb-5">
        <div class="col-lg-12">
            <label class="required form-label">Periode</label>
            <input type="text" class="form-control" placeholder="Input Periode" name="nama" id="nama" value="{{!empty(old('nama'))? old('nama') : ($actionform == 'update' && $data->nama != ''? $data->nama : old('nama'))}}" required/>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-6">
            <label class="required form-label">Tanggal Mulai</label>
            <input type="text" class="form-control" placeholder="Input Tanggal Mulai" name="tgl_mulai" id="tgl_mulai" value="{{!empty(old('tgl_mulai'))? old('tgl_mulai') : ($actionform == 'update' && $data->tgl_mulai != ''? $data->tgl_mulai : old('tgl_mulai'))}}" required/>
        </div>
        <div class="col-lg-6">
            <label class="required form-label">Tanggal Akhir</label>
            <input type="text" class="form-control" placeholder="Input Tanggal Join" name="tgl_akhir" id="tgl_akhir" value="{{!empty(old('tgl_akhir'))? old('tgl_akhir') : ($actionform == 'update' && $data->tgl_akhir != ''? $data->tgl_akhir : old('tgl_akhir'))}}" required/>
        </div>
    </div>
    <div class="text-center pt-15">
        <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-roles-modal-action="cancel">Discard</button>
        <button id="submit" type="submit" class="btn btn-primary" data-kt-roles-modal-action="submit">
            <span class="indicator-label">Submit</span>
            <span class="indicator-progress">Please wait...
            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
        </button>
    </div>
</form>

<script type="text/javascript">
    var title = "{{$actionform == 'update'? 'Update' : 'Tambah'}}" + " {{ $pagetitle }}";

    $(document).ready(function(){
        $('.modal-title').html(title);
        $('.form-select').select2();
        $('.modal').on('shown.bs.modal', function () {
            $("#tgl_mulai,#tgl_akhir").flatpickr({});
            setFormValidate();            
        });  
    });

    function setFormValidate(){
        $('#form-edit').validate({
            rules: {
                name:{
                        required: true
                }               		               		                              		               		               
            },
            messages: {
                name: {
                    required: "Nama wajib diinput"
                }                                      		                   		                   
            },	        
            highlight: function(element) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('is-invalid');
            },
            errorElement: 'div',
            errorClass: 'invalid-feedback',
            errorPlacement: function(error, element) {
                if(element.parent('.validated').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
        submitHandler: function(form){
                var typesubmit = $("input[type=submit][clicked=true]").val();
                $(form).ajaxSubmit({
                    type: 'post',
                    url: urlstore,
                    data: {source : typesubmit},
                    dataType : 'json',
                    beforeSend: function(){
                        $.blockUI({
                            theme: false,
                            baseZ: 2000
                        })    
                    },
                    success: function(data){
                        $.unblockUI();
                        if (data.flag == 'success') {
                            interVal = 1000;
                        }else{
                            interVal = null;
                        } 
                        swal.fire({
                                title: data.title,
                                html: data.msg,
                                icon: data.flag,
                                timer:interVal,
                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                   
                        if(data.flag == 'success') {
                            $('#winform').modal('hide');
                            datatable.ajax.reload( null, false );
                        }
                    },
                    error: function(jqXHR, exception){
                        $.unblockUI();
                        var msgerror = '';
                        if (jqXHR.status === 0) {
                            msgerror = 'jaringan tidak terkoneksi.';
                        } else if (jqXHR.status == 404) {
                            msgerror = 'Halaman tidak ditemukan. [404]';
                        } else if (jqXHR.status == 500) {
                            msgerror = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msgerror = 'Requested JSON parse gagal.';
                        } else if (exception === 'timeout') {
                            msgerror = 'RTO.';
                        } else if (exception === 'abort') {
                            msgerror = 'Gagal request ajax.';
                        } else {
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                        swal.fire({
                                title: "Error System",
                                html: msgerror+', coba ulangi kembali !!!',
                                icon: 'error',
                                buttonsStyling: true,
                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                               
                    }
                });
                return false;
        }
        });		
    }

        $('#brand_id').on('change', function(){
            $('#parent_id').val(''); 
            let brand_id = $(this).val();
            var url ='{{ route('general.fetchparentquestion', ['brand_id' => ':param']) }}';
            url = url.replace(':param', brand_id);
            $('#parent_id').prop("disabled", false)
            $('#parent_id').select2({
                width:'100%',
                allowClear: true,
                placeholder: '[ - Pilih Parent Pertanyaan - ]',
                ajax: {
                    url: url,
                    dataType: 'json',
                    data: function(params) {
                        return {
                            q: params.term, 
                            page: params.page
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: data.item
                        }
                    },
                    cache: true
                }
            });
        })

</script>
