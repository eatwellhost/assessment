@extends('layouts.admin.app')
@section('addbeforecss')
<link rel='stylesheet' type='text/css' media='screen' href='{{asset('assets/css/webcam.css')}}'>
@endsection
@section('content')

<div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-fluid">
        <!--begin::Card-->
        <div class="card">

            <!--begin::Card header-->
            <div class="card-header pt-5">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="timeline-icon symbol symbol-circle symbol-40px me-4">
                        <div class="symbol-label bg-light">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com003.svg-->
                            <span class="menu-icon">
                                <i class="bi bi-file-earmark-text-fill fs-3"></i>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                    </div>
                    <h2 class="d-flex align-items-center">{{$pagetitle }}
                    <span class="text-gray-600 fs-6 ms-1"></span></h2>
                </div>
                <!--end::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Search-->
                    {{-- <div class="kt-portlet__head-actions">
                        <button type="button" class="btn btn-primary btn-sm cls-add" data-kt-view-permissions-table-select="delete_selected"><i class="bi bi-file-plus"></i>Tambah Data</button>
                        <a id="kt_horizontal_search_advanced_link"
                            href="#kt_advanced_search_form"
                            class="btn-link collapsed btn btn-sm"
                            data-bs-toggle="collapse">
                            <button type="button" class="btn btn-warning btn-sm btn-flex btn-light btn-active-warning fw-bolder"><i class="bi bi-search"></i>Filter</button>
                        </a>
                        <button id="refresh" type="button" class="btn btn-secondary btn-sm"
                        data-kt-view-roles-table-select="delete_selected"><i class="bi bi-arrow-clockwise"></i>Refresh</button>
                    </div> --}}
                    <!--end::Search-->
                    <!--end::Group actions-->
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--begin::Card body-->
            <div class="card-body p-0">
                <!--begin::Heading-->
                <div class="card-px py-10" style="padding-left: 0.5rem !important;pedding-right: 0.5rem !important">
                    <!--begin: Datatable -->
                    <div class="card-px py-1">
                        @if($can)
                        <form class="kt-form kt-form--label-right" method="POST" id="form-inputchecklist">
                            @csrf
                            <input type="hidden" name="data_id" id="id" value="{{$data->id}}">
                            <input type="hidden" name="pegawai_id" id="pegawai_id" value="{{$data->pegawai_id}}">
                            <input type="hidden" name="actionform" value="{{$actionform}}">
                            <div class="form-group row mb-5">
                                <div class="col-lg-4">
                                    <label class="form-label required">Nomor Induk Karyawan</label>
                                    <input type="text" class="form-control" placeholder="NIK Pegawai" name="nik" id="nik" value="{{$pegawai->nik}}" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label required">Pegawai</label>
                                    <input type="text" class="form-control" placeholder="Nama Pegawai" name="pegawai" id="pegawai" value="{{$pegawai->nama}}" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label required">Perusahaan</label>
                                    <input type="text" class="form-control" placeholder="Perusahaan" name="perusahaan_id" id="perusahaan" value="{{$pegawai->companys}}" readonly/>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-lg-4">
                                    <label class="form-label required">Divisi</label>
                                    <input type="text" class="form-control" placeholder="Divisi Pegawai" name="divisi_id" id="nik" value="{{$pegawai->divisions}}" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label required">Department</label>
                                    <input type="text" class="form-control" placeholder="Department Pegawai" name="department_id" id="department" value="{{$pegawai->departments}}" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label required">Section</label>
                                    <input type="text" class="form-control" placeholder="Section" name="section_id" id="section" value="{{$pegawai->sections}}" readonly/>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-lg-4">
                                    <label class="form-label required">Jabatan</label>
                                    <input type="text" class="form-control" placeholder="Jabatan Pegawai" name="position" id="position" value="{{$pegawai->position}}" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label required">Status Karyawan</label>
                                    <input type="text" class="form-control" placeholder="status Pegawai" name="employee_status_id" id="employee_status_id" value="{{$pegawai->employee_statuss}}" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label required">Lokasi Bekerja</label>
                                    <input type="text" class="form-control" placeholder="Lokasi Bekerja" name="location_id" id="location_id" value="{{$pegawai->locations}}" readonly/>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-lg-6">
                                    <label class="form-label required">Pemberi Nilai</label>
                                    <input type="text" class="form-control" placeholder="Pemberi Nilai" name="penilai" id="penilai_id" value="{{$penilai}}" readonly/>
                                </div>
                                <div class="col-lg-6">
                                    <label class="form-label required">Sesi Pemberi Penilaian</label>
                                    <select class="form-select form-select-solid form-select2" id="sesi" name="sesi_id" data-kt-select2="true" data-placeholder="Pilih Sesi Pemberi Nilai" disabled>
                                        @foreach($sesi as $value)  
                                            <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->sesi_id ? 'selected' : ''}}>{{$value->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <!--begin: Datatable -->
                                <table class="table table-bordered table-checkable tree" id="datatable">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" width='5%'></th>
                                            <th rowspan="2" width='45%' style="text-align:center" valign="middle">Point Assessment</th>
                                            <th colspan="5" style="text-align:center" valign="middle">Penilaian</th>
                                            {{-- <th rowspan="2" style="text-align:center" valign="middle">Keterangan (Alasan memberi angka tsb)</th> --}}
                                        </tr>
                                        <tr>
                                            <td width='3%'>1</td>
                                            <td width='3%'>2</td>
                                            <td width='3%'>3</td>
                                            <td width='3%'>4</td>
                                            <td width='3%'>5</td>
                                        </tr>
                                    </thead>
                                    <tbody class="text-gray-700 fw-bold">
                                        @php
                                        // Identifikasi ID dengan anak
                                        $parentIds = collect($point)->pluck('parent_id')->unique()->toArray();
                                        @endphp
                                        @foreach($point as $key => $val)
                                            @if(in_array($val->id, $parentIds))
                                                <tr class="treegrid-{{$val->id}} treegrid-expanded" style="background-color:#ffc700 !important">
                                                    <td></td>
                                                    <td style="color: #000;">
                                                        <div class="symbol symbol-35px symbol-square" style="word-wrap:break-word;">
                                                        <b>{{$val->question}}</b>
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    {{-- <td></td>                                 --}}
                                                </tr>
                                                @else
                                                <tr class="treegrid-{{$val->id}}  treegrid-parent-{{$val->parent_id}} treegrid-collapsed treegrid-child">
                                                    <td></td>
                                                    <td style="color: #000;">
                                                        <div class="symbol symbol-35px symbol-square" style="word-wrap:break-word;">
                                                        {{$val->question}}
                                                        </div>
                                                        <input type="hidden" name="question_id[{{$val->id}}]" value="{{$val->id}}">
                                                    </td>
                                                    <td><input type="radio" name='jawaban[{{$val->id}}]' class="jawaban" value="1" {{$val->jawaban == '1' ? 'checked':''}} {{$val->is_submit == '1' ? $val->jawaban == '1'  ? 'readonly' : 'disabled' : ''}}  required></td>
                                                    <td><input type="radio" name='jawaban[{{$val->id}}]' class="jawaban" value="2" {{$val->jawaban == '2' ? 'checked': ''}} {{$val->is_submit == '1' ? $val->jawaban == '2'  ? 'readonly' : 'disabled' : ''}}></td>
                                                    <td><input type="radio" name='jawaban[{{$val->id}}]' class="jawaban" value="3" {{$val->jawaban == '3' ? 'checked': ''}} {{$val->is_submit == '1' ? $val->jawaban == '3'  ? 'readonly' : 'disabled' : ''}}></td>
                                                    <td><input type="radio" name='jawaban[{{$val->id}}]' class="jawaban" value="4" {{$val->jawaban == '4' ? 'checked': ''}} {{$val->is_submit == '1' ? $val->jawaban == '4'  ? 'readonly' : 'disabled' : ''}} ></td>
                                                    <td><input type="radio" name='jawaban[{{$val->id}}]' class="jawaban" value="5" {{$val->jawaban == '5' ? 'checked': ''}} {{$val->is_submit == '1' ? $val->jawaban == '5'  ? 'readonly' : 'disabled' : ''}} ></td>
                                                    {{-- <td><textarea name="detail[{{$val->id}}]" id="detail{{$val->id}}" rows="2" cols="50" required>{{$val->detail}}</textarea></td> --}}
                                                </tr>
                                            @endif
                                        @endforeach 
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-lg-6">
                                    <label class="form-label required">Sebutkan Poin-poin Evaluasi Kinerja Karyawan selama ini ?</label>
                                    <textarea class="form-control" name="point" id="point" rows="2" required>{{$data->point}}</textarea>
                                </div>
                                <div class="col-lg-6">
                                    <label class="form-label required">Sebutkan Saran Perbaikan dan Pengembangan untuk Karyawan ?</label>
                                    <textarea class="form-control" name="saran" id="saran" rows="2" required>{{$data->saran}}</textarea>
                                </div>
                            </div>
                            <div class="text-center pt-15">
                                <a href="{{route('laporan.assessment.index')}}" class="btn btn-success">Kembali</a>
                            </div>
                        </form>
                        @else
                        <div>
                            <div class="alert alert-dismissible bg-light-danger border border-danger border-dashed d-flex flex-column flex-sm-row w-100 p-5 mb-10">
                                    <!--begin::Icon-->
                                    <i class="fas fa-info-circle fs-2hx text-danger me-4 mb-5 mb-sm-0"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>                    <!--end::Icon-->

                                    <!--begin::Content-->
                                    <div class="d-flex flex-column pe-0 pe-sm-10">
                                        <h5 class="mb-1">Perhatian</h5>
                                        <span style="color:#000">Pertanyaan Checklist belum di buat, Mohon segera menghubungi Admin Branch.</span>
                                    </div>
                                    <!--end::Content-->

                                    <!--begin::Close-->
                                    <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                                        <i class="ki-duotone ki-cross fs-1 text-danger"><span class="path1"></span><span class="path2"></span></i>                    </button>
                                    <!--end::Close-->
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Card body-->
        </div>
    </div>
</div>
@endsection

@section('addafterjs')
{{-- <script src="{{asset('/assets/js/webcam.js')}}"></script> --}}
{{-- <script type="text/javascript" src="{{asset('assets/js/webcam-easy.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/takepicture.js')}}"></script> --}}

<script>
    var datatable;
    var urlcreate = "{{route('assessment.create')}}";
    var urledit = "{{route('assessment.edit')}}";
    var urlstore = "{{route('assessment.store')}}";
    var urldelete = "{{route('assessment.delete')}}";

    $(document).ready(function(){
        $('#page-title').html("{{ $pagetitle }}");
        $('.form-select').select2();
        $('#page-breadcrumb').html("{{ $breadcrumb }}");

        $('body').on('click','.cls-add',function(){
            winform(urlcreate, {}, 'Tambah Data');
        });

        $('body').on('click','.cls-button-edit',function(){
            winform(urledit, {'id':$(this).data('id')}, 'Ubah Data');
        });
            
        $('.tree').treegrid({
            expanderExpandedClass: 'bi bi-dash-square',
            expanderCollapsedClass: 'bi bi-plus-square',
            initialState: 'expanded',
        });
        // $('.jawaban').on('click', function(){
        //     $('#submit').trigger('click');
        // });
        validateFilter();
    });

    function validateFilter(){
        $('#form-inputchecklist').validate({
            rules: {
                filter_outlet: {
                    required: true,
                }
            },
            messages: {
                filter_outlet: {
                    required: "Outlet wajib dipilih!",
                }
            },
            beforeSend: function () {
                $.blockUI();
            },
            highlight: function (element) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).closest('.form-control').removeClass('is-invalid');
            },
            errorElement: 'div',
            errorClass: 'invalid-feedback',
            errorPlacement: function (error, element) {
                if (element.parent('.validated').length) {
                    error.insertAfter(element.parent());
                } else {
                    if(element.is('select')){
                        error.insertAfter(element.siblings('div'));
                    }else{
                        error.insertAfter(element);
                    }
                }
            },
            submitHandler: function(form){
                var typesubmit = $("input[type=submit][clicked=true]").val();
                
                $(form).ajaxSubmit({
                    type: 'post',
                    url: urlstore,
                    data: {source : typesubmit},
                    dataType : 'json',
                    beforeSend: function(){
                        $.blockUI({
                            theme: false,
                            baseZ: 2000
                        })    
                    },
                    success: function(data){
                        $.unblockUI();
                        if (data.flag == 'success') {
                            interVal = 1000;
                        }else{
                            interVal = null;
                        } 
                        swal.fire({
                                title: data.title,
                                html: data.msg,
                                icon: data.flag,
                                timer:interVal,
                                width: 600,
                                buttonsStyling: true,
                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                   

                        if(data.flag == 'success') {
                            $('#winform').modal('hide');
                            location.href="{{route('assessment.index')}}"
                        }
                    },
                    error: function(jqXHR, exception){
                        $.unblockUI();
                        var msgerror = '';
                        if (jqXHR.status === 0) {
                            msgerror = 'jaringan tidak terkoneksi.';
                        } else if (jqXHR.status == 404) {
                            msgerror = 'Halaman tidak ditemukan. [404]';
                        } else if (jqXHR.status == 500) {
                            msgerror = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msgerror = 'Requested JSON parse gagal.';
                        } else if (exception === 'timeout') {
                            msgerror = 'RTO.';
                        } else if (exception === 'abort') {
                            msgerror = 'Gagal request ajax.';
                        } else {
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                        swal.fire({
                                title: "Error System",
                                html: msgerror+', coba ulangi kembali !!!',
                                icon: 'error',
                                customClass: 'swal-wide',
                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                               
                    }
                });
                return false;
            }
        });

    }
    function setDatatable() {
        datatable = $('#datatable').DataTable({
            searching: false,
            //info:false,
            processing: true,
            serverSide: true,
          aLengthMenu: [[10, 20, 55,100, -1], [10, 20, 50,100, "All"]],
            pageLength: 20,
            lengthChange: false,
            responsive: true,
            ajax: {
                url: urldatatable,
                data: function (d) {
                    d.outlet = $('#filter_name').val();
                }
            },
            columns: [{
                    data: 'id',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'tgl_input',
                    name: 'tgl_input'
                },
                {
                    data: 'action',
                    name: 'action',
                    searchable: false
                },
            ],
            drawCallback: function (settings) {
                var info = datatable.page.info();
                $('[data-toggle="tooltip"]').tooltip();
                datatable.column(0, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function (cell, i) {
                    cell.innerHTML = info.start + i + 1;
                });
            }
        });
    }
</script>
@endsection
