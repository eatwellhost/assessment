<html lang="en" data-bs-theme="light">
    <head>
        <base href="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Assessment Employee Eatwell</title>
        <meta name="csrf-token" content="jqLOsxMbjDsNcizMj7vZUmechbt5Y5v1V1xge1Zf">
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="article">
        <meta property="og:title" content="">
        <link rel="canonical" href="">
        <link rel="shortcut icon" href="{{asset('assets/media/logos/polos.png')}}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700">

        
        <!--begin::Global Stylesheets Bundle(used by all pages)-->
        <link rel="stylesheet" href="{{asset('assets/css/style.bundle.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/global/plugins.bundle.css')}}">
        <!--end::Global Stylesheets Bundle-->

        <!--begin::Vendor Stylesheets(used by this page)-->
        <link rel="stylesheet" href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}">
        <!--end::Vendor Stylesheets-->
    </head>
<!--end::Head-->

<!--begin::Body-->
    <body id="kt_app_body" class="app-blank" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true">
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root" id="kt_app_root">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <!--begin::Body-->
            <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                <!--begin::Form-->
                <div class="d-flex flex-center flex-column flex-lg-row-fluid">
                    <!--begin::Wrapper-->
                    <div class="w-lg-500px p-10">
                        <!--begin::Page-->
                        <!--begin::Form-->
                        <form class="form w-100 kt_change_form" novalidate="novalidate" id="kt_change_form" data-kt-redirect-url="/" action="changepassword">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user_id}}">
                            <div class="text-center mb-11">
                                <!--begin::Title-->
                                <h1 class="text-dark fw-bolder mb-3">
                                    Change Password
                                </h1>
                                <!--end::Title-->

                                <!--begin::Subtitle-->
                                <!--end::Subtitle--->
                            </div>
                            <!--begin::Heading-->

                            <!--begin::Login options-->
                            <div class="fv-row mb-8">
                                <div class="mb-1">
                                <!--begin::Repeat Password-->
                                <input placeholder="Old Password" name="old_password" id="old_password" type="password" autocomplete="off" class="form-control bg-transparent"/>
                                <!--end::Repeat Password-->
                                </div>
                            </div>
                            <!--end::Separator-->
                            <div class="fv-row mb-8" data-kt-password-meter="true">
                                <!--begin::Wrapper-->
                                <div class="mb-1">
                                    <!--begin::Input wrapper-->
                                    <div class="position-relative mb-3">
                                        <input class="form-control bg-transparent" id="password" type="password" placeholder="Password" name="password" autocomplete="off"/>

                                        <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                            <i class="bi bi-eye-slash fs-2"></i>
                                            <i class="bi bi-eye fs-2 d-none"></i>
                                        </span>
                                    </div>
                                    <!--end::Input wrapper-->

                                    <!--begin::Meter-->
                                    <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                                    </div>
                                    <!--end::Meter-->
                                </div>
                                <!--end::Wrapper-->

                                <!--begin::Hint-->
                                <div class="text-muted">
                                    <p><i class="fa fa-square u-c-primary" id="pwd-restriction-length"></i> 8 Hingga 20 karakter<br><i class="fa fa-square u-c-primary" id="pwd-restriction-char-number-special"></i> Huruf, Nomor, dan Karakter khusus</p>
                                </div>
                                <!--end::Hint-->
                            </div>
                            <!--end::Input group--->

                            <!--end::Input group--->
                            <div class="fv-row mb-8">
                                <!--begin::Repeat Password-->
                                <input placeholder="Repeat Password" id ="password_confirmation" name="password_confirmation" type="password" autocomplete="off" class="form-control bg-transparent"/>
                                <!--end::Repeat Password-->
                            </div>
                            <!--end::Input group--->

                            <!--begin::Accept-->
                            {{-- <div class="fv-row mb-8">
                                <label class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="toc" value="1"/>
                                    <span class="form-check-label fw-semibold text-gray-700 fs-base ms-1">
                                        I Accept the <a href="#" class="ms-1 link-primary">Terms</a>
                                    </span>
                                </label>
                            </div> --}}
                            <!--end::Input group--->
                            <div class="d-grid mb-10">
                                <button type="submit" id="kt_sign_up_submit" class="btn btn-primary"> Change
                                </button>
                            </div>
                            <!--begin::Wrapper-->
                            <!--end::Sign up-->
                        </form>
                        <!--end::Form-->
                        <!--end::Page-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Body-->

            <!--begin::Aside-->
            <div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-image: url({{asset('assets/media/misc/auth-bg1.jpg')}}">
                <!--begin::Content-->
                <div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
                    <!--begin::Logo-->
                    <a href="/" class="mb-12">
                        <img alt="Logo" src="{{asset('assets/media/logos/logo.png')}}" class="h-60px h-lg-75px">
                    </a>
                    <!--end::Logo-->

                    <!--begin::Image-->
                    <img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-300px mb-10 mb-lg-20" src="{{asset('assets/media/misc/checklist.png')}}" alt="">
                    <!--end::Image-->

                    <!--begin::Title-->
                    <h1 class="d-none d-lg-block text-black fs-2qx fw-bolder text-center mb-7">
                        Integrity, Collaboration, Adaptability and Pursuit of Growth
                    </h1>
                    <!--end::Title-->

                    <!--begin::Text-->
                    <div class="d-none d-lg-block text-black fs-base text-center">
                        <p></p><p>
                    </p></div>
                    <!--end::Text-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Aside-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Root-->


        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(mandatory for all pages)-->
        <script type="text/javascript" src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/scripts.bundle.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/widgets.bundle.js')}}"></script>
        <!--end::Global Javascript Bundle-->

        <!--begin::Vendors Javascript(used by this page)-->
        <script type="text/javascript" src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
        <!--end::Vendors Javascript-->

        <!--begin::Custom Javascript(optional)-->
        <script type="text/javascript" src="{{asset('assets/js/custom/widgets.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/custom/apps/chat/chat.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/custom/modals/create-app.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/custom/modals/upgrade-plan.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/custom/intro.js')}}"></script>
    
        <script type="text/javascript" src="{{asset('assets/plugins/jquery.blockUI.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/plugins/jquery.validate.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/plugins/jquery.form.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/scripts.bundle.js')}}"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        </script>
        <script> 
        jQuery(document).ready(function(){
            setFormValidateDaftar();
             $('#password').keyup(function(){
                let pwdLength = /^.{8,20}$/;
                let pwdUpper = /[A-Z]+/;
                let pwdLower = /[a-z]+/;
                let pwdNumber = /[0-9]+/;
                let pwdSpecial = /[!@#$%^&()'[\]"?+-/*={}.,;:_]+/;
        
                var pass = $('#password').val();
                if (pass) {
                    if (pwdLength.test(pass)) {
                        $('#pwd-restriction-length').addClass('fa-check-square');
                        $('#pwd-restriction-length').removeClass('fa-square');
                    } else {
                        $('#pwd-restriction-length').addClass('fa-square');
                        $('#pwd-restriction-length').removeClass('fa-check-square');
                    }
        
                    if (pwdUpper.test(pass) && pwdLower.test(pass) && pwdNumber.test(pass) && pwdSpecial.test(pass)) {
                        $('#pwd-restriction-char-number-special').addClass('fa-check-square');
                        $('#pwd-restriction-char-number-special').removeClass('fa-square');
                    }else{
                        $('#pwd-restriction-char-number-special').removeClass('fa-check-square');
                        $('#pwd-restriction-char-number-special').addClass('fa-square');
                    }
        
                    if (pwdLength.test(pass) && pwdUpper.test(pass) && pwdLower.test(pass) && pwdNumber.test(pass) && pwdSpecial.test(pass)) {
                        $('#kt_sign_up_form').removeClass('disabled');
                    }
                } else {
                    $('#pwd-restriction-length').removeClass('fa-check-square');
                    $('#pwd-restriction-char-number-special').removeClass('fa-check-square');
                    $('#kt_sign_up_form').addClass('disabled');
                }
             });
        });
        
        function setFormValidateDaftar(){
                jQuery('#kt_change_form').validate({
                    rules: {
                        old_password:{
                            required: true
                        },
                        password:{
                            required: true
                        },                           		               		                              		               		               
                        password_confirmation:{
                        equalTo: '[name="password"]'
                        },                           		               		                              		               		               
                    },
                    messages: {
                        old_password: {
                            required: "Password lama wajib diinput",
                        },
                        password: {
                        required: "Password wajib diinput",
                        },                                                      		                   		                   
                        password_confirmation: {
                        equalTo: "Password tidak sama, ulangi password.",
                        },                                                      		                   		                   
                    },       
                    ignore: [],
                    highlight: function(element) {
                    jQuery(element).closest('.form-control').addClass('is-invalid');
                    },
                    unhighlight: function(element) {
                    jQuery(element).closest('.form-control').removeClass('is-invalid');
                    },
                    errorElement: 'div',
                    errorClass: 'invalid-feedback',
                    errorPlacement: function(error, element) {
                        if(element.parent('.validated').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    },
                submitHandler: function(form){
                        var typesubmit = jQuery("input[type=submit][clicked=true]").val();
                        jQuery(form).ajaxSubmit({
                            type: 'post',
                            url: "{{route('auth.changepassword.store')}}",
                            data: {source : typesubmit},
                            dataType : 'json',
                            beforeSend: function(){
                            jQuery.blockUI('#kt_change_form',{
                                    theme: true,
                                    baseZ: 2000
                                });      
                            },
                            success: function(data){
                                jQuery.unblockUI();
                                if (data.flag == 'success') {
                                    interVal = 3000;
                                    $('#kt_change_form')[0].reset();
                                    swal.fire({
                                            title: data.title,
                                            html: data.msg,
                                            icon: data.flag,
                                            timer:interVal,
                                            buttonsStyling: true,
                                            confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                                    });	         
                                    window.location.href ="{{route('dashboard.index')}}";
                                }else{
                                    interVal = null;
                                    swal.fire({
                                        title: data.title,
                                        text: data.msg,
                                        icon: data.flag,
                                        timer:interVal,
                                        buttonsStyling: true,
                                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                                    });	      
                                } 
                            },
                            error: function(jqXHR, exception){
                            jQuery.unblockUI();
                                var msgerror = '';
                                if (jqXHR.status === 0) {
                                    msgerror = 'jaringan tidak terkoneksi.';
                                } else if (jqXHR.status == 404) {
                                    msgerror = 'Halaman tidak ditemukan. [404]';
                                } else if (jqXHR.status == 500) {
                                    msgerror = 'Internal Server Error [500].';
                                } else if (exception === 'parsererror') {
                                    msgerror = 'Requested JSON parse gagal.';
                                } else if (exception === 'timeout') {
                                    msgerror = 'RTO.';
                                } else if (exception === 'abort') {
                                    msgerror = 'Gagal request ajax.';
                                } else {
                                    msgerror = 'Error.\n' + jqXHR.responseText;
                                }
                                    swal.fire({
                                        title: "Error System",
                                        html: msgerror+', coba ulangi kembali !!!',
                                        icon: 'error',
    
                                        buttonsStyling: true,
    
                                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                                });	                               
                            }
                        });
                        return false;
                  }
                  });		
              }
        </script>
    <!--end::Custom Javascript-->
<!--end::Javascript-->
</body>
<!--end::Body-->

</html>