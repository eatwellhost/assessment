
@extends('layouts.admin.app')

@section('content')
<div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
		<!--begin::Row-->
		<div class="row row-cols-1 row-cols-md-2 row-cols-xl-3 g-5 g-xl-9">
			<!--begin::Add new card-->
			<div class="ol-md-4">
				<!--begin::Card-->
				<div class="card h-md-100 shadow-sm">
				<!--begin::Card body-->
					<div class="card-body d-flex flex-center">
						<!--begin::Button-->
							<button type="button" class="btn btn-clear d-flex flex-column flex-center cls-add" >
								<!--begin::Illustration-->
								<img src="{{ asset('assets/media/illustrations/sketchy-1/4.png')}}" alt="" class="mw-100 mh-150px mb-7" />
								<!--end::Illustration-->
								<!--begin::Label-->
								<div class="fw-bolder fs-3 text-gray-600 text-hover-primary">Tambah Role</div>
								<!--end::Label-->
							</button>
						<!--begin::Button-->
					</div>
				<!--begin::Card body-->
				</div>
			<!--begin::Card-->
		</div>
	
		<!--begin::Add new card-->                                
        @foreach($role as $roles)    
		<!--begin::Col-->
		<div class="col-md-4">
			<!--begin::Card-->
			<div class="card card-flush h-md-100 shadow-sm">
				<!--begin::Card header-->
				<div class="card-header">
				<!--begin::Card title-->
					<div class="card-title">
						<h2>{{ucfirst($roles->name)}}</h2>
					</div>
				<!--end::Card title-->
				</div>
				<!--end::Card header-->
				<!--begin::Card body-->
				<div class="card-body pt-1">
					<!--begin::Users-->
					<div class="fw-bolder text-gray-600 mb-5">Hak Akses Menu :</div>
					<!--end::Users-->
					<!--begin::Permissions-->
					{{-- <div style="overflow-y: scroll; height:20px;"></div> --}}
						<div class="d-flex flex-column text-gray-600" style="overflow-y: scroll; height:300px;">
							@foreach($parent_menu as $pm)
								@if($roles->name == $pm->name)
									<div class="d-flex align-items-center py-2">
										<span class="bullet bg-primary me-3"></span>
											{{$pm->label}}
									</div>
								@endif
							@endforeach
						</div>
					<!--end::Permissions-->
				</div>
				
				<!--end::Card body-->
				<!--begin::Card footer-->
				<div class="card-footer flex-wrap pt-0 text-center">
					<button type="button" class="btn btn-icon btn-light btn-active-light-primary my-1 cls-view" data-id="{{Crypt::encrypt((int)$roles->id)}}">
                        <i class="fas fa-eye"></i>
                    </button>
                    <button type="button" class="btn btn-icon btn-light btn-active-light-primary my-1 cls-edit" data-id="{{Crypt::encrypt((int)$roles->id)}}">
                        <i class="fas fa-edit"></i>
                    </button>
					<button type="button" class="btn btn-icon btn-light btn-active-light-primary my-1 cls-delete" data-id="{{Crypt::encrypt((int)$roles->id)}}">
                        <i class="fas fa-trash"></i>
                    </button>
				</div>
				<!--end::Card footer-->
			</div>
			<!--end::Card-->
		</div>
		<!--end::Col-->
        @endforeach
		</div>
		<!--end::Row-->
	</div>
<!--end::Container-->
</div>
@endsection

@section('addafterjs')
<script>
    var datatable;
    var urlcreate = "{{route('manajemen.role.create')}}";
    var urldetail = "{{route('manajemen.role.detail')}}";
	var urledit = "{{route('manajemen.role.edit')}}";
    var urlstore = "{{route('manajemen.role.store')}}";
    var urldelete = "{{route('manajemen.role.delete')}}";
                
    $(document).ready(function(){
        $('#page-title').html("{{ $pagetitle }}");

        $('body').on('click','.cls-add',function(){
			window.location= urlcreate;
        });		

        $('body').on('click','.cls-view',function(){
			var id = $(this).data('id');
			window.location= urldetail+'/'+id;
        });

        $('body').on('click','.cls-edit',function(){
			var id = $(this).data('id');
			window.location= urledit+'/'+id;
        });

        $('body').on('click','.cls-delete',function(){
			var id = $(this).data('id');
            onbtndelete(id);
        });
                                       
    });

                    
    function onbtndelete(id){
        swal.fire({
            title: "Pemberitahuan",
            text: "Yakin hapus data ?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus data",
            cancelButtonText: "Tidak"
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                url: urldelete,
                data:{
                    "id": id
                },
                type:'post',
                dataType:'json',
                beforeSend: function(){
                    $.blockUI();
                },
                success: function(data){
                    $.unblockUI();

                    swal.fire({
                        title: data.title,
                        html: data.msg,
                        icon: data.flag,
                        timer:1000,
                        buttonsStyling: true,

                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    });

                    if(data.flag == 'success') {
                        location.reload();
                    }
                    
                },
                error: function(jqXHR, exception) {
                    $.unblockUI();
                    var msgerror = '';
                    if (jqXHR.status === 0) {
                        msgerror = 'jaringan tidak terkoneksi.';
                    } else if (jqXHR.status == 404) {
                        msgerror = 'Halaman tidak ditemukan. [404]';
                    } else if (jqXHR.status == 500) {
                        msgerror = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msgerror = 'Requested JSON parse gagal.';
                    } else if (exception === 'timeout') {
                        msgerror = 'RTO.';
                    } else if (exception === 'abort') {
                        msgerror = 'Gagal request ajax.';
                    } else {
                        msgerror = 'Error.\n' + jqXHR.responseText;
                    }
                    swal.fire({
                        title: "Error System",
                        html: msgerror+', coba ulangi kembali !!!',
                        icon: 'error',

                        buttonsStyling: true,

                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    });  
                    }
                });
            }
        });	
    }
</script>
@endsection  