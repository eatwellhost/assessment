@extends('layouts.admin.app')

@section('content')
<div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
    <div id="kt_content_container" class="container-fluid">
        <div class="card shadow-sm ">
            <div class="card-title">
            </div>
        <div class="card-body py-4">
        <form id="kt_modal_add_role_form" class="form" action="{{route('manajemen.role.store')}}" method="POST">
            <!--begin::Scroll-->
            <div class="d-flex flex-column " id="kt_modal_add_role_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_role_header" data-kt-scroll-wrappers="#kt_modal_add_role_scroll" data-kt-scroll-offset="3000px">
            @csrf
            <input type="hidden" class="form-control form-control-solid" name="actionform" value="{{ $actionform }}"/>
            <input type="hidden" class="form-control form-control-solid" name="role_id" value="{{ $role?$role->id : '' }}"/>
                <!--begin::Input group-->
                <div class="fv-row mb-10">
                    <label class="fs-5 fw-bolder form-label mb-2">
                        <span >Role</span>
                    </label>
                    
                    <input class="form-control form-control-solid" placeholder="Masukkan nama Role" name="name" value="{{$role?$role->name : ''}}" required {{($actionform === 'update') ?  'readonly': ''}}/>
                </div>
                <!--end::Input group-->
            
                <!--begin::Permissions-->
                <label class="fs-5 fw-bolder form-label mb-2">Role Permissions</label>
                <div class="fv-row ">
                    <div class="table-responsive">
                        <table class="table align-middle table-row-bordered fs-6 gy-5 ">
                            <thead>
                                <tr>
                                    <th class="text-muted">List Menu</th>
                                    <th colspan="{{count($permission)}}" class="text-muted text-center">List Permission</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-bold">
                                <tr>
                                    <td class="text-gray-800 bg-light-warning" style="padding-left: 10px;">Semua Akses
                                    <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Allows a full access to the system"></i></td>
                                    <td >
                                        <!--begin::Checkbox-->
                                        <label class="form-check form-check-custom form-check-solid me-9">
                                            <input class="form-check-input" type="checkbox" value="" id="kt_roles_select_all" />
                                            <span class="form-check-label" for="kt_roles_select_all">
                                               <strong>Pilih Semua</strong> 
                                            </span>
                                        </label>
                                        <!--end::Checkbox-->
                                    </td>
                                </tr>
                                @foreach($menu as $menus)
                                <tr>
                                    {{-- list menu --}}
                                    @if($menus->parent_id > 0)
                                        <td class="text-gray-800 bg-light-primary" style="padding-left: 30px;">
                                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                                <i class="{{$menus->icon}}"></i>
                                                {{-- <input class="form-check-input" type="checkbox" value="{{$menus->id}}" name="menu[]" style="border: 1px solid;"/> --}}
                                                <span class="form-check-label">{{$menus->label}}</span>
                                            </label>
                                        </td>
                                    @else
                                        <td class="text-gray-800 bg-light-primary" style="padding-left: 10px;">
                                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                                <i class="{{$menus->icon}}"></i>
                                                {{-- <input class="form-check-input" type="checkbox" value="{{$menus->id}}" name="menu[]" style="border: 1px solid;"/> --}}
                                                <span class="form-check-label">{{$menus->label}}</span>
                                            </label>
                                        </td>
                                    @endif
                                    {{-- list permission --}}                                    
                                    <td >
                                        <div class="d-flex" >
                                            @foreach($permission as $per)
                                                <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                                    <input class="form-check-input chekboks" type="checkbox" value="{{$menus->id.','.$per->id}}" name="permission[]" id="permission{{$menus->id}}" @if($actionform == 'update')@foreach($arr_role_per as $v)@if($menus->id.','.$per->id == $v){!! 'checked' !!}@endif @endforeach @endif />
                                                    <span class="form-check-label">{{ ucfirst($per->name)}}</span>
                                                    <input type="hidden" value="{{$menus->id}}">
                                                </label>
                                            @endforeach
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                <!--end::Table row-->
                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table wrapper-->
                </div>
                <!--end::Permissions-->
            </div>
            <!--end::Scroll-->
            <!--begin::Actions-->
            <div class="text-center pt-15">
                <hr>
                <button type="submit" class="btn btn-primary" data-kt-roles-modal-action="submit">
                    <span class="indicator-label"><i class="fas fa-save text-white"></i> Simpan</span>
                    <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
                <a href="{{route('manajemen.role.index')}}"><button type="button"class="btn btn-secondary me-3" data-bs-dismiss="modal" data-kt-roles-modal-action="cancel"><i class="fas fa-times "></i>Batal</button></a>
            </div>
            <!--end::Actions-->
            </form>
        </div>
    </div>
</div>
@endsection


@section('addafterjs')
<script>                
    $(document).ready(function(){
        var jmlchecked = $('.chekboks:checked').length;
        var jmlcheckbox = $('.chekboks:checkbox').length;

        //ceklis jika semua akses true
        if(jmlchecked == jmlcheckbox){
            $('#kt_roles_select_all').prop('checked', true);
        }

        //ceklis by event checbox all
        $("#kt_roles_select_all").click(function () {
            var jmlchecked = $('.chekboks:checked').length;
            var jmlcheckbox = $('.chekboks:checkbox').length;
            if(jmlchecked < jmlcheckbox ){
                $('.chekboks').not(this).prop('checked', this.checked);
            }else if(jmlchecked == jmlcheckbox ){
                $('.chekboks').not(this).prop('checked', this.checked);
            }
        });

        //ceklis by event checbox        
        $(".chekboks").click(function () {
            var jmlchecked = $('.chekboks:checked').length;
            var jmlcheckbox = $('.chekboks:checkbox').length;
            if(jmlchecked == jmlcheckbox ){
                $('#kt_roles_select_all').prop('checked', this.checked);
            }
            else if(jmlchecked < jmlcheckbox ){
                $('#kt_roles_select_all').prop('checked', false);
            }
        });

    });
</script>
@endsection