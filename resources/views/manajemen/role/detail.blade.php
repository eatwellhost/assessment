
@extends('layouts.admin.app')

@section('content')
<div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
		<!--begin::Layout-->
		<div class="d-flex flex-column flex-lg-row">
			<!--begin::Sidebar-->
			<div class="flex-column flex-lg-row-auto w-100 w-lg-200px w-xl-300px mb-10">
				<!--begin::Card-->
				<div class="card card-flush  shadow-sm">
					<!--begin::Card header-->
					<div class="card-header">
						<!--begin::Card title-->
						<div class="card-title">
							<h2 class="mb-0">
                                {{ucfirst($roles->name)}}
                            </h2>
						</div>
						<!--end::Card title-->
					</div>
					<!--end::Card header-->

                    <!--begin::Card body-->
					<div class="card-body pt-0">
                        <div class="fw-bolder text-gray-600 mb-5">Hak Akses Menu :</div>
						<!--begin::Permissions-->
                        <div class="d-flex flex-column text-gray-600" style="overflow-y: scroll; height:300px;">
                        @foreach($parent_menu as $pm)
                            @if($roles->name == $pm->name)
                                <div class="d-flex align-items-center py-2">
                                    <span class="bullet bg-primary me-3"></span>
                                    {{$pm->label}}
                                </div>
                            @endif
                        @endforeach
                        </div>
            			<!--end::Permissions-->
					</div>
					<!--end::Card body-->

					<!--begin::Card footer-->
					<div class="card-footer pt-0">
                        <a href="{{ url()->previous() }}">
                        <button type="button" class="btn btn-light btn-active-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_update_role">
                            Kembali
                        </button>
                        </a>
					</div>
					<!--end::Card footer-->
				</div>
				<!--end::Card-->
			</div>
			<!--end::Sidebar-->
                                    
			<!--begin::Content-->
			<div class="flex-lg-row-fluid ms-lg-10">
				<!--begin::Card-->
				<div class="card card-flush mb-6 mb-xl-9  shadow-sm">
					<!--begin::Card header-->
					<div class="card-header pt-5">
						<!--begin::Card title-->
						<div class="card-title">
							<h2 class="d-flex align-items-center">Users Assigned
							<span class="text-gray-600 fs-6 ms-1">({{ $role_user[0]['username']? count($role_user) : 0 }})</span></h2>
						</div>
						<!--end::Card title-->
					</div>
					<!--end::Card header-->
					<!--begin::Card body-->
					<div class="card-body pt-0">
					<!--begin::Table-->
					{{-- <table class="table align-middle table-row-dashed fs-6 gy-5 mb-0" id="datatable"> --}}
						<table class="table align-middle table-row-dashed fs-6 gy-5 mb-0" id="kt_roles_view_table">
						<!--begin::Table head-->
							<thead>
								<!--begin::Table row-->
								<tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
									<th class="min-w-50px">ID</th>
									<th class="min-w-150px">User</th>
									<th class="min-w-125px">Joined Date</th>
								</tr>
								<!--end::Table row-->
							</thead>
						<!--end::Table head-->
						<!--begin::Table body-->
							<tbody class="fw-bold text-gray-600">
                                @if($role_user[0]['username'])
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($role_user as $user)
                                    <tr>
                                    <!--begin::ID-->
                                        <td>{{$i++}}</td>
                                    <!--begin::ID-->
                                    <!--begin::User=-->
                                        <td class="d-flex align-items-center">
                                    <!--begin:: Avatar -->
                                    <!--begin::User details-->
                                        <div class="d-flex flex-column">
                                            <a class="text-gray-800 text-hover-primary mb-1">{{$user->name}}</a>
                                                <span>{{$user->email}}</span>
                                        </div>
                                        <!--begin::User details-->
                                        </td>
                                        <!--end::user=-->
                                        <!--begin::Joined date=-->
                                        <td>{{$user->created_at? date('d-M-Y', strtotime($user->created_at)) : ''}}</td>
                                        <!--end::Joined date=-->
                                    </tr>
                                    @endforeach
                                @endif
    						</tbody>
							<!--end::Table body-->
						</table>
					<!--end::Table-->
					</div>
					<!--end::Card body-->
				</div>
				<!--end::Card-->
			</div>
			<!--end::Content-->
		</div>
		<!--end::Layout-->
	</div>
	<!--end::Container-->
</div>
@endsection
    