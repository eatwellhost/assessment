<form class="kt-form kt-form--label-right" method="POST" id="form-edit">
	@csrf
	<input type="hidden" name="id" id="id" readonly="readonly" value="{{$actionform == 'update'? (int)$data->id : null}}" />
    <input type="hidden" name="actionform" id="actionform" readonly="readonly" value="{{$actionform}}" />

    <div class="form-group row mb-5">
        <div class="col-lg-6">
            <label class="required form-label">NIK</label>
            <input type="text" class="form-control" name="nik" id="nik" value="{{!empty(old('nik'))? old('nik') : ($actionform == 'update' && $data->nik != ''? $data->nik : old('nik'))}}" required/>
        </div>
        <div class="col-lg-6">
            <label class="required form-label">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{!empty(old('nama'))? old('nama') : ($actionform == 'update' && $data->nama != ''? $data->nama : old('nama'))}}" required/>
        </div>
    </div>	
    <div class="form-group row mb-5">
        <div class="col-lg-6">
            <label class="required form-label">Email</label>
            <input type="text" class="form-control" name="email" id="email" value="{{!empty(old('email'))? old('email') : ($actionform == 'update' && $data->email != ''? $data->email : old('email'))}}" required/>
        </div>
        <div class="col-lg-6">
            <label class="required form-label">Join Date</label>
            <input type="text" class="form-control" placeholder="Input Tanggal Join" name="join_date" id="join_date" value="{{!empty(old('join_date'))? old('join_date') : ($actionform == 'update' && $data->join_date != ''? $data->join_date : old('join_date'))}}" required/>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-6">
            <label class="required form-label">Company</label>
            <select class="form-select form-select-solid form-select2" id="company" name="company_id" data-kt-select2="true" data-placeholder="Pilih Company" data-dropdown-parent="#winform" required>
                <option></option>
                @foreach($company as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->company_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-6">
            <label class="required form-label">Position</label>
            <input type="text" class="form-control" name="position" id="position" value="{{!empty(old('position'))? old('position') : ($actionform == 'update' && $data->position != ''? $data->position : old('position'))}}" required/>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-4">
            <label class="required form-label">Level</label>           
            <select class="form-select form-select-solid form-select2" id="level" name="level_id" data-kt-select2="true" data-placeholder="Pilih Level" data-dropdown-parent="#winform" required>
                <option></option>
                @foreach($level as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->level_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="required form-label">Status</label>           
            <select class="form-select form-select-solid form-select2" id="employee_status" name="employee_status_id" data-kt-select2="true" data-placeholder="Pilih Status" data-dropdown-parent="#winform" required>
                <option></option>
                @foreach($employee_status as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->employee_status_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="required form-label">Location</label>           
            <select class="form-select form-select-solid form-select2" id="location" name="location_id" data-kt-select2="true" data-placeholder="Pilih Lokasi" data-dropdown-parent="#winform" required>
                <option></option>
                @foreach($location as $value)  
                    <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->location_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-4">
            <label class="required form-label">Division</label>           
            <select class="form-select form-select-solid form-select2" id="division" name="division_id" data-kt-select2="true" data-placeholder="Pilih Divisi" data-dropdown-parent="#winform" required>
                <option></option>
                @foreach($division as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->division_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4 {{$actionform == 'insert' ? 'd-none' : ''}}" id="dept">
            <label class="required form-label">Department</label>           
            <select class="form-select form-select-solid form-select2" id="department" name="department_id" data-kt-select2="true" data-placeholder="Pilih Department" data-dropdown-parent="#winform">
                <option></option>
                @foreach($department as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->department_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4 {{$actionform == 'insert' ? 'd-none' : ''}}" id="sect">
            <label class="required form-label">Section</label>           
            <select class="form-select form-select-solid form-select2" id="section" name="section_id" data-kt-select2="true" data-placeholder="Pilih Section" data-dropdown-parent="#winform" required>
                <option></option>
                @foreach($section as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->section_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    
    <div class="text-center pt-15">
        <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-roles-modal-action="cancel">Discard</button>
        <button id="submit" type="submit" class="btn btn-primary" data-kt-roles-modal-action="submit">
            <span class="indicator-label">Submit</span>
            <span class="indicator-progress">Please wait...
            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
        </button>
    </div>
</form>

<script type="text/javascript">
    var title = "{{$actionform == 'update'? 'Update' : 'Tambah'}}" + " {{ $pagetitle }}";

    $(document).ready(function(){
        $('.modal-title').html(title);
        $('.form-select').select2();
        $('.modal').on('shown.bs.modal', function () {
            $("#join_date").flatpickr({});
            setFormValidate();

            $('#division').change(function(){
                console.log('disini');
                $('#department').val(''); 
                $('#dept').removeClass('d-none');
                $('#department').prop('required', true);
                let division_id = $(this).val();
                var url ='{{ route('general.getdept', ['division_id' => ':param']) }}';
                url = url.replace(':param', division_id);
                $('#department').select2({
                    width:'100%',
                    allowClear: true,
                    multiple:false,
                    placeholder: 'Pilih Department ...',
                    ajax: {
                        url: url,
                        dataType: 'json',
                        data: function(params) {
                            return {
                                q: params.term, 
                                page: params.page
                            };
                        },
                        processResults: function(data, params) {
                            return {
                                results: data.item
                            }
                        },
                        cache: true
                        }
                }); 
            })

            $('#department').change(function(){
                $('#section').val(''); 
                $('#sect').removeClass('d-none');
                $('#section').prop('required', true);
                let department_id = $(this).val();
                var url ='{{ route('general.getsect', ['department_id' => ':param']) }}';
                url = url.replace(':param', department_id);
                $('#section').select2({
                    width:'100%',
                    allowClear: true,
                    multiple:false,
                    placeholder: 'Pilih Section ...',
                    ajax: {
                        url: url,
                        dataType: 'json',
                        data: function(params) {
                            return {
                                q: params.term, 
                                page: params.page
                            };
                        },
                        processResults: function(data, params) {
                            return {
                                results: data.item
                            }
                        },
                        cache: true
                        }
                }); 
            })
        });
    });

    $('#role').change(function(){
        brand();
    })

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
            callback.apply(context, args);
            }, ms || 0);
        };
    }

    function setFormValidate(){
        $('#form-edit').validate({
            rules: {
                name:{
                        required: true
                },
                username:{
                        required: true
                },
                email:{
                        required: true
                },
                roles:{
                        required: true
                }           		               		                              		               		               
            },
            messages: {
                name: {
                    required: "Nama wajib diinput"
                },
                username: {
                    required: "Username wajib diinput"
                },
                email: {
                    required: "Email wajib diinput"
                },
                roles: {
                    required: "Role wajib dipilih"
                },                                      		                   		                   
            },	        
            highlight: function(element) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('is-invalid');
            },
            errorElement: 'div',
            errorClass: 'invalid-feedback',
            errorPlacement: function(error, element) {
                if(element.parent('.validated').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
        submitHandler: function(form){
                var typesubmit = $("input[type=submit][clicked=true]").val();
                
                $(form).ajaxSubmit({
                    type: 'post',
                    url: urlstore,
                    data: {source : typesubmit},
                    dataType : 'json',
                    beforeSend: function(){
                        $.blockUI({
                            theme: true,
                            baseZ: 2000
                        })    
                    },
                    success: function(data){
                        $.unblockUI();
                        if (data.flag == 'success') {
                            interVal = 1000;
                        }else{
                            interVal = null;
                        } 
                        swal.fire({
                                title: data.title,
                                html: data.msg,
                                icon: data.flag,
                                timer: interVal,
                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                   

                        if(data.flag == 'success') {
                            $('#winform').modal('hide');
                            datatable.ajax.reload( null, false );
                        }
                    },
                    error: function(jqXHR, exception){
                        $.unblockUI();
                        var msgerror = '';
                        if (jqXHR.status === 0) {
                            msgerror = 'jaringan tidak terkoneksi.';
                        } else if (jqXHR.status == 404) {
                            msgerror = 'Halaman tidak ditemukan. [404]';
                        } else if (jqXHR.status == 500) {
                            msgerror = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msgerror = 'Requested JSON parse gagal.';
                        } else if (exception === 'timeout') {
                            msgerror = 'RTO.';
                        } else if (exception === 'abort') {
                            msgerror = 'Gagal request ajax.';
                        } else {
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                        swal.fire({
                                title: "Error System",
                                html: msgerror+', coba ulangi kembali !!!',
                                icon: 'error',

                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                               
                    }
                });
                return false;
        }
        });		
    }

    function onlyNumberKey(e) {
        var ASCIICode = (e.which) ? e.which : e.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }

    function brand(){
        var role = $('#role').val();
        console.log(role, jQuery.inArray(3,role));
        if(jQuery.inArray('3',role)){
            $('.brand2,.area2,.outlet2').removeClass('d-none');
            $('.am').addClass('d-none');
            $('#brand,#area_id,#outlet_id').prop('required', true);
            $('#am_id').prop('required', false);
        }else if(jQuery.inArray(2,role) || jQuery.inArray(6,role) || jQuery.inArray(5,role) || jQuery.inArray(10,role)){
            
            $('.outlet2,.area2,.am').addClass('d-none');
            $('#area_id,#outlet_id,#am_id').prop('required', false);
            $('#area_id,#outlet_id,#am_id').val('').trigger('change');
        }else if(jQuery.inArray(7,role)){
            $('.brand2,.area2').removeClass('d-none');
            $('#brand,#area_id').prop('required', true);
            $('.outlet2,.am').addClass('d-none');
            $('#outlet_id,#am_id').prop('required', false);
            $('#outlet_id,#am_id').val('').trigger('change');
        }else if(jQuery.inArray(4,role)){
            $('.brand2,.am').removeClass('d-none');
            $('.outlet2,.area2').addClass('d-none');
            $('#brand,#am_id').prop('required', true);
            $('#outlet_id,#area_id').prop('required', false);
            $('#outlet_id,#area_id').val('').trigger('change');
        }else{
            $('.brand2,.area2,.outlet2,.am').addClass('d-none');
            $('#brand,#area_id,#outlet_id').prop('required', false);
            $('#brand,#area_id,#outlet_id').val('').trigger('change');
        }
        // if(role==3){
        //     $('.brand2,.area2,.outlet2').removeClass('d-none');
        //     $('.am').addClass('d-none');
        //     $('#brand,#area_id,#outlet_id').prop('required', true);
        //     $('#am_id').prop('required', false);
        // }else if(role==2 || role==6 || role==5 || role == 10){
        //     $('.brand2').removeClass('d-none');
        //     $('#brand').prop('required', true);
        //     $('.outlet2,.area2,.am').addClass('d-none');
        //     $('#area_id,#outlet_id,#am_id').prop('required', false);
        //     $('#area_id,#outlet_id,#am_id').val('').trigger('change');
        // }else if(role==7){
        //     $('.brand2,.area2').removeClass('d-none');
        //     $('#brand,#area_id').prop('required', true);
        //     $('.outlet2,.am').addClass('d-none');
        //     $('#outlet_id,#am_id').prop('required', false);
        //     $('#outlet_id,#am_id').val('').trigger('change');
        // }else if(role==4){
        //     $('.brand2,.am').removeClass('d-none');
        //     $('.outlet2,.area2').addClass('d-none');
        //     $('#brand,#am_id').prop('required', true);
        //     $('#outlet_id,#area_id').prop('required', false);
        //     $('#outlet_id,#area_id').val('').trigger('change');
        // }else{
        //     $('.brand2,.area2,.outlet2,.am').addClass('d-none');
        //     $('#brand,#area_id,#outlet_id').prop('required', false);
        //     $('#brand,#area_id,#outlet_id').val('').trigger('change');
        // }

        // if(role==2 || role==4 || role==6){
        //     $('.brand2').removeClass('d-none');
        //     $('#brand').prop('required', true);
        // }else{
        //     $('.brand2').addClass('d-none');
        //     $('#brand').prop('required', false);
        //     $('#brand').val('').trigger('change');
        // }

        // if(role==4){
        //     $('.area2').removeClass('d-none');
        //     $('#area_id').prop('required', true);
        // } else{
        //     $('.area2').addClass('d-none');
        //     $('#area_id').prop('required', false);
        //     $('#area_id').val('').trigger('change');
        // }

    
    }
</script>
