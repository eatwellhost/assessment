<form class="kt-form kt-form--label-right" method="POST" id="form-edit">
	@csrf
	<input type="hidden" name="id" id="id" readonly="readonly" value="{{$actionform == 'update'? (int)$data->id : null}}" />
    <input type="hidden" name="actionform" id="actionform" readonly="readonly" value="{{$actionform}}" />

    <div class="form-group row mb-5">
        <div class="col-lg-12">
            <label class="required form-label">Pegawai</label>
            <select class="form-select form-select-solid form-select2" id="pegawai" name="pegawai_id" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Pegawai" data-dropdown-parent="#winform">
                <option></option>
                @foreach($pegawai as $value)  
                    <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->pegawai_id ? 'selected' : ''}}>{{$value->nik. ' - ' .$value->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row mb-5">
        <div class="col-lg-6">
            <label class="required form-label">Username</label>
            <input type="text" class="form-control" name="username" id="username" value="{{!empty(old('username'))? old('username') : ($actionform == 'update' && $data->username != ''? $data->username : old('username'))}}" required readonly style="color: #787878;"/>
        </div>
        <div class="col-lg-6">
            <label class="required form-label">Nama</label>
            <input type="text" class="form-control" name="name" id="name" value="{{!empty(old('name'))? old('name') : ($actionform == 'update' && $data->name != ''? $data->name : old('name'))}}" required readonly style="color: #787878;"/>
        </div>
    </div>	
    <div class="form-group row mb-5">
        <div class="{{$actionform == 'insert' ? 'col-lg-6' : 'col-lg-12'}}">
            <label class="required form-label">Email</label>
            <input type="text" class="form-control" name="email" id="email" value="{{!empty(old('email'))? old('email') : ($actionform == 'update' && $data->email != ''? $data->email : old('email'))}}" required readonly style="color: #787878;"/>
        </div>
        <div class="col-lg-6 {{$actionform == 'update' ? 'd-none' : ''}}">
            <label>Password</label>
            <input type="text" class="form-control" name="random_password" id="random_password"  value="{{$actionform == 'update' ? $data->random_password : $random_password}}" required readonly style="color: #787878;"/>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-6">
            <label class="required form-label">Roles</label>           
            <select class="form-select form-select-solid form-select2" id="role" name="roles[]" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Role" data-dropdown-parent="#winform" required multiple>
                <option></option>
                @foreach($role as $value)  
					<option value="{{$value->id}}" {{$actionform == 'update' && in_array($value->name, $userRole) ? 'selected' : ''}}>{{$value->name}}</option>
                @endforeach
            </select>
        </div>
        
    </div>
    <div class="text-center pt-15">
        <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-roles-modal-action="cancel">Discard</button>
        <button id="submit" type="submit" class="btn btn-primary" data-kt-roles-modal-action="submit">
            <span class="indicator-label">Submit</span>
            <span class="indicator-progress">Please wait...
            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
        </button>
    </div>
</form>

<script type="text/javascript">
    var title = "{{$actionform == 'update'? 'Update' : 'Tambah'}}" + " {{ $pagetitle }}";

    $(document).ready(function(){
        $('.modal-title').html(title);
        $('.form-select').select2();
        $('.modal').on('shown.bs.modal', function () {
            setFormValidate();
        });

        $('#pegawai').change(function(){
            if($('#actionform').val() == 'insert'){
                $('#name').val(''); 
                $('#username').val(''); 
                $('#email').val('');
            }

            let pegawai_id = $(this).val();
            $.ajax({
                type: 'post',
                url: "{{route('manajemen.pegawai.getpegawai')}}",
                data: {
                    'pegawai_id' : pegawai_id,
                    },
            beforeSend: function () {
                $.blockUI();
            },
            success: function(data){
                var originalEmail = data.email;
                var atIndex = originalEmail.indexOf('@');
                var truncatedEmail = atIndex !== -1 ? originalEmail.substring(0, atIndex) : originalEmail;
                $('#name').val(data.nama); 
                $('#username').val(truncatedEmail); 
                $('#email').val(data.email); 
            }
            });
        });
    });

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
            callback.apply(context, args);
            }, ms || 0);
        };
    }

    function setFormValidate(){
        $('#form-edit').validate({
            rules: {
                name:{
                        required: true
                },
                username:{
                        required: true
                },
                email:{
                        required: true
                },
                roles:{
                        required: true
                }           		               		                              		               		               
            },
            messages: {
                name: {
                    required: "Nama wajib diinput"
                },
                username: {
                    required: "Username wajib diinput"
                },
                email: {
                    required: "Email wajib diinput"
                },
                roles: {
                    required: "Role wajib dipilih"
                },                                      		                   		                   
            },	        
            highlight: function(element) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('is-invalid');
            },
            errorElement: 'div',
            errorClass: 'invalid-feedback',
            errorPlacement: function(error, element) {
                if(element.parent('.validated').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
        submitHandler: function(form){
                var typesubmit = $("input[type=submit][clicked=true]").val();
                
                $(form).ajaxSubmit({
                    type: 'post',
                    url: urlstore,
                    data: {source : typesubmit},
                    dataType : 'json',
                    beforeSend: function(){
                        $.blockUI({
                            theme: true,
                            baseZ: 2000
                        })    
                    },
                    success: function(data){
                        $.unblockUI();
                        if (data.flag == 'success') {
                            interVal = 1000;
                        }else{
                            interVal = null;
                        } 
                        swal.fire({
                                title: data.title,
                                html: data.msg,
                                icon: data.flag,
                                timer: interVal,
                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                   

                        if(data.flag == 'success') {
                            $('#winform').modal('hide');
                            datatable.ajax.reload( null, false );
                        }
                    },
                    error: function(jqXHR, exception){
                        $.unblockUI();
                        var msgerror = '';
                        if (jqXHR.status === 0) {
                            msgerror = 'jaringan tidak terkoneksi.';
                        } else if (jqXHR.status == 404) {
                            msgerror = 'Halaman tidak ditemukan. [404]';
                        } else if (jqXHR.status == 500) {
                            msgerror = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msgerror = 'Requested JSON parse gagal.';
                        } else if (exception === 'timeout') {
                            msgerror = 'RTO.';
                        } else if (exception === 'abort') {
                            msgerror = 'Gagal request ajax.';
                        } else {
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                        swal.fire({
                                title: "Error System",
                                html: msgerror+', coba ulangi kembali !!!',
                                icon: 'error',

                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                               
                    }
                });
                return false;
        }
        });		
    }

    function onlyNumberKey(e) {
        var ASCIICode = (e.which) ? e.which : e.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
</script>
