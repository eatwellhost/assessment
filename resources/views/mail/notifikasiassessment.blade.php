<x-mail::message>
<h2>Dear {{$data['user']->name}},</h2>
<p style="margin-bottom:2px; color:#181C32;text-align:left">Sehubungan dengan adanya {{$data['jenis']}} atas nama pegawai berikut ini:</p>
<table>
    <tbody>
        <tr>
            <td style="width:8%"></td>
            <td style="width:18%"><b>Nama</b></td>
            <td style="width:2%">:</td>
            <td><b>{{$data['pegawai']->nama}}</b></td>
            <td style="width:8%"></td>
        </tr>
        <tr>
            <td style="width:8%"></td>
            <td style="width:18%"><b>NIK</b></td>
            <td style="width:2%">:</td>
            <td><b>{{$data['pegawai']->nik}}</b></td>
            <td style="width:8%"></td>
        </tr>
        <tr>
            <td style="width:8%"></td>
            <td style="width:18%"><b>Jabatan</td>
            <td style="width:2%">:</td>
            <td><b>{{$data['pegawai']->position}}</b></td>
            <td style="width:8%"></td>
        </tr>
    </tbody>
</table>
<p style="margin-bottom:2px; color:#181C32;text-align:justify">maka dengan ini diminta kesediaannya untuk melakukan penilaian terkait kinerja yang bersangkutan. Anda dapat mengklik tombol di bawah ini:</p>

<div align="center" style="margin-bottom: 30px"><a href="{{$data['url']}}" class='button button-primary'>Lihat Detail</a></div>

<p style="margin-bottom:2px; color:#181C32;text-align:justify; background-color:aqua">PS: Batas waktu pengisian maksimal <b>{{$data['akhir']}}</b> , jika dalam batas waktu tersebut karyawan tidak melakukan penilaian, maka link penilaian PA tidak dapat diakses atau nonaktif.</p><br/>
<p style="margin-bottom:2px; color:#181C32;text-align:justify;">Informasi Login : </p><br/>
<table>
    <tbody>
        <tr>
            <td style="width:8%"></td>
            <td style="width:18%"><b>Username</b></td>
            <td style="width:2%">:</td>
            <td><b>{{$data['user']->username}}</b></td>
            <td style="width:8%"></td>
        </tr>
        <tr>
            <td style="width:8%"></td>
            <td style="width:18%"><b>password</b></td>
            <td style="width:2%">:</td>
            <td><b>{{$data['user']->old_password}}</b></td>
            <td style="width:8%"></td>
        </tr>
    </tbody>
</table>

<p style="margin-bottom:2px; color:#181C32;text-align:justify;">Note: Mohon untuk selalu menjaga kerahasiaan akses login anda, segera lakukan perubahan password ketika login pertama kali.</p><br/>

<p style="margin-bottom:2px; color:#181C32;text-align:justify">Demikian pemberitahuan ini kami sampaikan, atas perhatiannya dan kerjasamanya kami ucapkan terimakasih<br>Eatwell Culinary</p>
</x-mail::message>
