@extends('layouts.admin.app')

@section('addbeforecss')
<link href="{{asset('assets/plugins/nestablelist/jquery.nestable.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('content')

<div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-fluid">
        <!--begin::Card-->
        <div class="card">

            <!--begin::Card header-->
            <div class="card-header pt-5">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="timeline-icon symbol symbol-circle symbol-40px me-4">
                        <div class="symbol-label bg-light">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com003.svg-->
                            <span class="menu-icon">
                                <i class="bi bi-file-earmark-text-fill fs-3"></i>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                    </div>
                    <h2 class="d-flex align-items-center">{{$pagetitle }}
                    <span class="text-gray-600 fs-6 ms-1"></span></h2>
                </div>
                <!--end::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Search-->
                    <div class="kt-portlet__head-actions">
                        <button type="button" class="btn btn-primary btn-sm cls-add" data-kt-view-permissions-table-select="delete_selected"><i class="bi bi-file-plus"></i>Tambah Data</button>
                        {{-- <a id="kt_horizontal_search_advanced_link"
                            href="#kt_advanced_search_form"
                            class="btn-link collapsed btn btn-sm"
                            data-bs-toggle="collapse">
                            <button type="button" class="btn btn-warning btn-sm btn-flex btn-light btn-active-warning fw-bolder"><i class="bi bi-search"></i>Filter</button>
                        </a>
                        <button id="refresh" type="button" class="btn btn-secondary btn-sm"
                        data-kt-view-roles-table-select="delete_selected"><i class="bi bi-arrow-clockwise"></i>Refresh</button> --}}
                    </div>
                    <!--end::Search-->
                    <!--end::Group actions-->
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--begin::Card body-->
            <div class="card-body p-0">
                <div class="card-px py-1">
                    <!--begin::Advance form-->
                    {{-- <form id="filterForm" autocomplete="off">
                        <div class="collapse show" id="kt_advanced_search_form">
                            <!--begin::Row-->
                            <div class="row g-8">
                                <!--begin::Col-->
                                <div class="col-xxl-7">
                                    <!--begin::Row-->
                                    <div class="row g-8">
                                        <!--begin::Col-->
                                        <div class="col-lg-6">
                                            <label class="fs-6 form-label fw-bolder text-dark">Brand</label>
                                            <select class="form-select form-select-solid form-select2" name="filter_brand" id='filter_brand' data-kt-select2="true" data-placeholder="Pilih Brand">
                                                <option></option>
                                                @foreach($brand as $value)  
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="fs-6 form-label fw-bolder text-dark">Jenis</label>
                                            <select class="form-select form-select-solid form-select2" name="filter_jenis" id='filter_jenis' data-kt-select2="true" data-placeholder="Pilih Jenis">
                                                <option></option>
                                                @foreach($jenis as $value)  
                                                    <option value="{{$value->id}}">{{$value->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Row-->
                            <!--begin::Actions-->
                            <div class="d-flex justify-content-end py-6">
                                <a id="kt_horizontal_search_advanced_link" class="btn-link collapsed"
                                    data-bs-toggle="collapse" href="#kt_advanced_search_form">
                                    <button type="button"
                                        class="btn btn-sm btn-light fw-bolder btn-active-light-secondary me-2"
                                        data-kt-search-element="advanced-options-form-cancel" href="javascript:;">Tutup</button>
                                </a>
                                <button id="reset" type="button" class="btn btn-warning btn-sm me-2"
                                    href="javascript:;">Reset</button>
                                    
                                <button id="filter" type="button" class="btn btn-primary btn-sm"
                                    href="javascript:;">Lihat</button>
                            </div>
                            <!--end::Actions-->
                        </div>
                    </form> --}}
                    <!--end::Advance form-->
                </div>
                <!--begin::Heading-->
                <div class="card-body p-0">
                    <!--begin::Heading-->
                    <div class="card-px py-10">
                        <!--begin: Datatable -->
                        <div class="dd" id="nestable_list_3"></div>
                    </div>
                </div>
            </div>
            <!--end::Card body-->
        </div>
    </div>
</div>
@endsection

@section('addafterjs')
<script>
    var datatable;
    var urlcreate = "{{route('setting.point_assessment.create')}}";
    var urledit = "{{route('setting.point_assessment.edit')}}";
    var urlstore = "{{route('setting.point_assessment.store')}}";
    var urldatatable = "{{route('setting.point_assessment.datatable')}}";
    var urldelete = "{{route('setting.point_assessment.delete')}}";
    var urlgettreemenu = "{{route('setting.point_assessment.gettreequestion')}}";
    var urlsubmitchangestructure = "{{route('setting.point_assessment.submitchangestructure')}}";
    var urlfetchparentmenu = "{{route('general.fetchparentquestion')}}";


    $(document).ready(function(){
        $('#page-title').html("{{ $pagetitle }}");
        $('.form-select').select2();
        $('#page-breadcrumb').html("{{ $breadcrumb }}");

        $('body').on('click','.cls-add',function(){
            winform(urlcreate, {}, 'Tambah Data');
        });

        $('body').on('click','.cls-button-edit',function(){
            winform(urledit, {'id':$(this).data('id')}, 'Ubah Data');
        });

        $('#nestable_list_3').nestable();
		setTreeMenu();	

        $('body').on('change', '.dd', function() {
            onNestChanged(this);
        });  

        $('body').on('click','.cls-button-delete',function(){
            onbtndelete(this);
        });
        
        $('#filter').on('click', function () {
            setTreeMenu();	
        });

        $('#refresh').on('click', function () {
            setTreeMenu();
        });
        
        $("#reset").on('click',function(event){
            $("#filter_brand").val('').trigger("change");
            $("#filter_jenis").val('').trigger("change");
            setTreeMenu();
        }); 

        // $('#filter_brand,#filter_jenis').on('change', function(){
        //     $('#filter').trigger('click'); 
        // });
    });
    
    function onbtndelete(element){
        swal.fire({
            title: "Pemberitahuan",
            text: "Yakin hapus data "+$(element).data('question')+" ?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus data",
            cancelButtonText: "Tidak"
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                url: urldelete,
                data:{
                    "id": $(element).data('id')
                },
                type:'post',
                dataType:'json',
                beforeSend: function(){
                    $.blockUI();
                },
                success: function(data){
                    $.unblockUI();

                    swal.fire({
                            title: data.title,
                            html: data.msg,
                            icon: data.flag,
                            timer:1000,
                            buttonsStyling: true,

                            confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    });

                    if(data.flag == 'success') {
                        setTreeMenu();
                    }
                    
                },
                error: function(jqXHR, exception) {
                    $.unblockUI();
                    var msgerror = '';
                    if (jqXHR.status === 0) {
                        msgerror = 'jaringan tidak terkoneksi.';
                    } else if (jqXHR.status == 404) {
                        msgerror = 'Halaman tidak ditemukan. [404]';
                    } else if (jqXHR.status == 500) {
                        msgerror = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msgerror = 'Requested JSON parse gagal.';
                    } else if (exception === 'timeout') {
                        msgerror = 'RTO.';
                    } else if (exception === 'abort') {
                        msgerror = 'Gagal request ajax.';
                    } else {
                        var str = jqXHR.responseText;
                        if(str.match(/User does not have the right permissions./)){
                            msgerror = 'User does not have the right permissions.';
                        } else{
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                    }
                    swal.fire({
                        title: "Error System",
                        html: msgerror+', coba ulangi kembali !!!',
                        icon: 'error',

                        buttonsStyling: true,

                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    });  
                    }
                });
            }
        });	
    }
    function setTreeMenu(){
		$.ajax({
            url: urlgettreemenu,
            type:'post',
            data: {
                    brand_id: function() {
                    return $( "#filter_brand" ).val();
                    },
                    jenis_id: function() {
                    return $( "#filter_jenis" ).val();
                    }
                },
            dataType:'json',
		beforeSend: function(){
			$.blockUI({
				theme: false,
				baseZ: 2000
			})    
		},
		success: function(data){
			$.unblockUI();

			$('#nestable_list_3').html(data.html);
			$('[data-toggle="tooltip"]').tooltip();
		},
			error: function(jqXHR, exception) {
			$.unblockUI();
			var msgerror = '';
			if (jqXHR.status === 0) {
				msgerror = 'jaringan tidak terkoneksi.';
			} else if (jqXHR.status == 404) {
				msgerror = 'Halaman tidak ditemukan. [404]';
			} else if (jqXHR.status == 500) {
				msgerror = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msgerror = 'Requested JSON parse gagal.';
			} else if (exception === 'timeout') {
				msgerror = 'RTO.';
			} else if (exception === 'abort') {
				msgerror = 'Gagal request ajax.';
			} else {
				msgerror = 'Error.\n' + jqXHR.responseText;
			}
			swal.fire({
				title: "Error System",
				html: msgerror+', coba ulangi kembali !!!',
				icon: 'error',

				buttonsStyling: true,

				confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
			});  
			}
		});	
	}

	function onNestChanged(element){
			$.ajax({
			type: 'post',
			url: urlsubmitchangestructure,
			data:{'serialized' : $('.dd').nestable('serialize')},
			dataType : 'json',
			beforeSend: function(){
				   $.blockUI();
			},
			success: function(data){
					$.unblockUI();
					setTreeMenu();
			},
			error: function (jqXHR, exception) {
					$.unblockUI();
					var msgerror = '';
					if (jqXHR.status === 0) {
						msgerror = 'jaringan tidak terkoneksi.';
					} else if (jqXHR.status == 404) {
						msgerror = 'Halaman tidak ditemukan. [404]';
					} else if (jqXHR.status == 500) {
						msgerror = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msgerror = 'Requested JSON parse gagal.';
					} else if (exception === 'timeout') {
						msgerror = 'RTO.';
					} else if (exception === 'abort') {
						msgerror = 'Gagal request ajax.';
					} else {
						msgerror = 'Error.\n' + jqXHR.responseText;
					}
					swal.fire({
						title: "Error System",
						html: msgerror+', coba ulangi kembali !!!',
						icon: 'error',

						buttonsStyling: true,

						confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
					}); 
			}
			});
	}
</script>
<script src="{{asset('assets/plugins/nestablelist/jquery.nestable.js')}}" type="text/javascript"></script>
@endsection
