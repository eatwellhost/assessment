@extends('layouts.admin.app')
@section('content')

<div class="post d-flex flex-column-fluid cls-content-data" id="kt_content">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-fluid">
        <!--begin::Card-->
        <div class="card">

            <!--begin::Card header-->
            <div class="card-header pt-5">
                <!--begin::Card title-->
                <div class="card-title">
                    <div class="timeline-icon symbol symbol-circle symbol-40px me-4">
                        <div class="symbol-label bg-light">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com003.svg-->
                            <span class="menu-icon">
                                <i class="bi bi-file-earmark-text-fill fs-3"></i>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                    </div>
                    <h2 class="d-flex align-items-center">{{$pagetitle }}
                    <span class="text-gray-600 fs-6 ms-1"></span></h2>
                </div>
                <!--end::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Search-->
                    <div class="kt-portlet__head-actions">
                        <button type="button" class="btn btn-primary btn-sm cls-add" data-kt-view-permissions-table-select="delete_selected"><i class="bi bi-file-plus"></i>Tambah Data</button>
                        {{-- <a id="kt_horizontal_search_advanced_link"
                            href="#kt_advanced_search_form"
                            class="btn-link collapsed btn btn-sm"
                            data-bs-toggle="collapse">
                            <button type="button" class="btn btn-warning btn-sm btn-flex btn-light btn-active-warning fw-bolder"><i class="bi bi-search"></i>Filter</button>
                        </a>
                        <button id="refresh" type="button" class="btn btn-secondary btn-sm"
                        data-kt-view-roles-table-select="delete_selected"><i class="bi bi-arrow-clockwise"></i>Refresh</button> --}}
                    </div>
                    <!--end::Search-->
                    <!--end::Group actions-->
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--begin::Card body-->
            <div class="card-body p-0">
                <div class="card-px py-1">
                    <!--begin::Advance form-->
                    {{-- <form id="filterForm" autocomplete="off">
                        <div class="collapse" id="kt_advanced_search_form">
                            <!--begin::Row-->
                            <div class="row g-8">
                                <!--begin::Col-->
                                <div class="col-xxl-7">
                                    <!--begin::Row-->
                                    <div class="row g-8">
                                        <!--begin::Col-->
                                        <div class="col-lg-6">
                                            <label class="fs-6 form-label fw-bolder text-dark">Brand</label>
                                            <input type="text"
                                                class="form-control input-sm"
                                                id="filter_brand" />
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Row-->
                            <!--begin::Actions-->
                            <div class="d-flex justify-content-end py-6">
                                <a id="kt_horizontal_search_advanced_link" class="btn-link collapsed"
                                    data-bs-toggle="collapse" href="#kt_advanced_search_form">
                                    <button type="button"
                                        class="btn btn-sm btn-light fw-bolder btn-active-light-secondary me-2"
                                        data-kt-search-element="advanced-options-form-cancel" href="javascript:;">Tutup</button>
                                </a>
                                <button id="reset" type="button" class="btn btn-warning btn-sm me-2"
                                    href="javascript:;">Reset</button>
                                    
                                <button id="filter" type="button" class="btn btn-primary btn-sm"
                                    href="javascript:;">Cari</button>
                            </div>
                            <!--end::Actions-->
                        </div>
                    </form> --}}
                    <!--end::Advance form-->
                </div>
                <!--begin::Heading-->
                <div class="card-px py-1">
                    <!--begin: Datatable -->
                    <table class="table table-hover table-row-bordered table-row-gray-300" id="datatable">
                        <thead>
                            <tr>
                                <th width="10px;">No.</th>
                                <th width="45%">Department</th>
                                <th width="41%">Section</th>
                                <th width="30px" style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="text-gray-700 fw-bold"></tbody>
                    </table>
                </div>
            </div>
            <!--end::Card body-->
        </div>
    </div>
</div>
@endsection

@section('addafterjs')
<script>
    var datatable;
    var urlcreate = "{{route('setting.master.section.create')}}";
    var urledit = "{{route('setting.master.section.edit')}}";
    var urlstore = "{{route('setting.master.section.store')}}";
    var urldatatable = "{{route('setting.master.section.datatable')}}";
    var urldelete = "{{route('setting.master.section.delete')}}";

    $(document).ready(function(){
        $('#page-title').html("{{ $pagetitle }}");
        $('#page-breadcrumb').html("{{ $breadcrumb }}");

        $('body').on('click','.cls-add',function(){
            winform(urlcreate, {}, 'Tambah Data');
        });

        $('body').on('click','.cls-button-edit',function(){
            winform(urledit, {'id':$(this).data('id')}, 'Ubah Data');
        });

        $('body').on('click','.cls-button-delete',function(){
            onbtndelete(this);
        });
        
        $('#filter').on('click', function () {
            datatable.ajax.reload(null, false);
        });

        $('#refresh').on('click', function () {
            datatable.ajax.reload(null, false);
        });
        
        $("#reset").on('click',function(event){
            $("#filter_permission").val('').trigger("change");
            datatable.ajax.reload(null, false);
        }); 

        setDatatable();
    });

    function setDatatable(){
        datatable = $('#datatable').DataTable({
            searching: false,
            //info:false,
            processing: true,
            serverSide: true,
          aLengthMenu: [[10, 20, 55,100, -1], [10, 20, 50,100, "All"]],
            pageLength: 10,
            lengthChange: true,
            responsive: true,
            ajax: {
                url: urldatatable,
                data: function (d) {
                    d.permission = $('#filter_permission').val();
                }
            },
            columns: [
                { data: 'id', orderable: false, searchable: false },
                { data: 'department', name: 'department' },
                { data: 'nama', name: 'nama' },
                { data: 'action', name:'action'},
            ],
            drawCallback: function( settings ) {
                var info = datatable.page.info();
                $('[data-toggle="tooltip"]').tooltip();
                datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = info.start + i + 1;
                } );
            }
        });
    }
    
    function onbtndelete(element){
        swal.fire({
            title: "Pemberitahuan",
            text: "Yakin hapus data "+$(element).data('nama')+" ?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus data",
            cancelButtonText: "Tidak"
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                url: urldelete,
                data:{
                    "id": $(element).data('id')
                },
                type:'post',
                dataType:'json',
                beforeSend: function(){
                    $.blockUI();
                },
                success: function(data){
                    $.unblockUI();

                    swal.fire({
                            title: data.title,
                            html: data.msg,
                            icon: data.flag,
                            timer:1000,
                            buttonsStyling: true,

                            confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    });

                    if(data.flag == 'success') {
                        datatable.ajax.reload( null, false );
                    }
                    
                },
                error: function(jqXHR, exception) {
                    $.unblockUI();
                    var msgerror = '';
                    if (jqXHR.status === 0) {
                        msgerror = 'jaringan tidak terkoneksi.';
                    } else if (jqXHR.status == 404) {
                        msgerror = 'Halaman tidak ditemukan. [404]';
                    } else if (jqXHR.status == 500) {
                        msgerror = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msgerror = 'Requested JSON parse gagal.';
                    } else if (exception === 'timeout') {
                        msgerror = 'RTO.';
                    } else if (exception === 'abort') {
                        msgerror = 'Gagal request ajax.';
                    } else {
                        var str = jqXHR.responseText;
                        if(str.match(/User does not have the right permissions./)){
                            msgerror = 'User does not have the right permissions.';
                        } else{
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                    }
                    swal.fire({
                        title: "Error System",
                        html: msgerror+', coba ulangi kembali !!!',
                        icon: 'error',

                        buttonsStyling: true,

                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    });  
                    }
                });
            }
        });	
    }
</script>
@endsection
