<form class="kt-form kt-form--label-right" method="POST" id="form-edit">
	@csrf
	<input type="hidden" name="id" id="id" readonly="readonly" value="{{$actionform == 'update'? (int)$data->id : null}}" />
	<input type="hidden" name="actionform" id="actionform" readonly="readonly" value="{{$actionform}}" />

    <div class="form-group row mb-5">
        <div class="col-lg-12">
            <label class="required form-label">Pegawai</label>           
            <select class="form-select form-select-solid form-select2" id="pegawai" name="pegawai_id" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Pegawai" data-dropdown-parent="#winformlebar" required>
                <option></option>
                @foreach($pegawai as $value)  
                    <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->pegawai_id? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-4">
            <label class="required form-label">Periode</label>           
            <select class="form-select form-select-solid form-select2" id="periode" name="periode_id" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Periode Assessment" data-dropdown-parent="#winformlebar" required>
                <option></option>
                @foreach($periode as $value)  
                    <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->periode_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="required form-label">Level Assessment</label>           
            <select class="form-select form-select-solid form-select2" id="level" name="level_id" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Level Assessment" data-dropdown-parent="#winformlebar" required>
                <option></option>
                @foreach($levelAssessment as $value)  
                    <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->level_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="required form-label">Jenis Assessment</label>           
            <select class="form-select form-select-solid form-select2" id="jenis" name="jenis_id" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Jenis Assessment" data-dropdown-parent="#winformlebar" required>
                <option></option>
                @foreach($jenisAssesment as $value)  
                    <option value="{{$value->id}}" {{$actionform == 'update' && $value->id == $data->jenis_id ? 'selected' : ''}}>{{$value->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-lg-12">
            <div class="d-flex bd-highlight">
                <div class="p-2 flex-grow-1 bd-highlight required form-label">Atasan</div>
                <div class="p-2 bd-highlight" id="Atasans">
                    <span class="btn btn-primary btn-sm me-3" id="addAtasan"><i class="las la-plus fs-1"></i> Atasan</span>
                </div>
            </div>
            <div class="form-group row mb-10">
                <div class="col-lg-12">
                    <table class="table table-hover table-row-bordered table-row-gray-300" id="table-atasan">
                        <thead>
                            <tr>
                                <th>Atasan</th>
                                <th style="width: 20px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data->periode_id <= '4')
                                <tr>
                                    <td id='selectAtasan'>
                                        <select name="atasan_id" class="form-select form-select-solid entitas atasan" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Atasan"  >
                                            <option></option>
                                            @foreach ($pegawai as $item)
                                                <option value="{{ $item->id }}" {{$data->atasan_id == $item->id ? "selected":""}}>{{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @else
                                @foreach($AssessmentHasAtasan as $key => $value)
                                <tr>
                                    <td id='selectAtasan'>
                                        <select name="atasan_id[]" class="form-select form-select-solid entitas atasan" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Atasan"  >
                                            <option></option>
                                                @foreach ($pegawai as $item)
                                                    <option value="{{ $item->id }}" {{$value->atasan_id == $item->id ? "selected":""}}>{{ $item->nama }}</option>
                                                @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-sm btn-icon me-3"><i class="las la-trash-alt fs-1"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex bd-highlight">
        <div class="p-2 flex-grow-1 bd-highlight required form-label">Rekan</div>
        <div class="p-2 bd-highlight" id="Rekans">
            <span class="btn btn-primary btn-sm me-3" id="addRekan"><i class="las la-plus fs-1"></i> Rekan</span>
        </div>
    </div>
    <div class="form-group row mb-10">
        <div class="col-lg-12">
            <table class="table table-hover table-row-bordered table-row-gray-300" id="table-rekan">
                <thead>
                    <tr>
                        <th>Rekan</th>
                        <th style="width: 20px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data->periode_id <= '4')
                        <tr>
                            <td id='selectRekan'>
                                <select name="rekan_id" class="form-select form-select-solid entitas rekan" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Rekan"  >
                                    <option></option>
                                    @foreach ($pegawai as $item)
                                        <option value="{{ $item->id }}" {{$data->rekan_id == $item->id ? "selected":""}}>{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    @else
                        @foreach($AssessmentHasRekan as $key => $value)
                        <tr>
                            <td id='selectRekan'>
                                <select name="rekan_id[]" class="form-select form-select-solid entitas rekan" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Rekan" data-dropdown-parent="#selectAtasan">
                                    <option></option>
                                    @foreach ($pegawai as $item)
                                        <option value="{{ $item->id }}" {{$value->rekan_id == $item->id ? "selected":""}}>{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-sm btn-icon me-3"><i class="las la-trash-alt fs-1"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex bd-highlight">
        <div class="p-2 flex-grow-1 bd-highlight required form-label">Tim</div>
        <div class="p-2 bd-highlight" id="Tims">
            <span class="btn btn-primary btn-sm me-3" id="addTim"><i class="las la-plus fs-1"></i> Tim</span>
        </div>
    </div>
    <div class="form-group row mb-10">
        <div class="col-lg-12">
            <table class="table table-hover table-row-bordered table-row-gray-300" id="table-tim">
                <thead>
                    <tr>
                        <th>Tim</th>
                        <th style="width: 20px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data->periode_id <= '4')
                        <tr>
                            <td id='selectTim'>
                                <select name="tim_id" class="form-select form-select-solid entitas tim" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Tim" data-dropdown-parent="#selectRekan">
                                    <option></option>
                                    @foreach ($pegawai as $item)
                                        <option value="{{ $item->id }}" {{$data->tim_id == $item->id ? "selected":""}}>{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    @else
                        @foreach($AssessmentHasTim as $key => $value)
                        <tr>
                            <td id='selectTim'>
                                <select name="tim_id[]" class="form-select form-select-solid entitas tim" data-kt-select2="true" data-allow-clear="true" data-placeholder="Pilih Tim" data-dropdown-parent="#selectTim" >
                                    <option></option>
                                    @foreach ($pegawai as $item)
                                        <option value="{{ $item->id }}" {{$value->tim_id == $item->id ? "selected":""}}>{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-sm btn-icon me-3"><i class="las la-trash-alt fs-1"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="text-center pt-15">
        <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-roles-modal-action="cancel">Discard</button>
        <button id="submit" type="submit" class="btn btn-primary" data-kt-roles-modal-action="submit">
            <span class="indicator-label">Submit</span>
            <span class="indicator-progress">Please wait...
            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
        </button>
    </div>
</form>

<script type="text/javascript">
    var title = "{{$actionform == 'update'? 'Update' : 'Tambah'}}" + " {{ $pagetitle }}";

    $(document).ready(function(){
        $('.modal-title').html(title);
        $('.form-select').select2({
            dropdownParent: $("#winformlebar")
        });
        $('.modal').on('shown.bs.modal', function () {
            setFormValidate();
            var countatasan = 0; 
            var countrekan = 0; 
            var counttim = 0;     
            $('#addAtasan').on('click', function(e){
                e.preventDefault();
                var row = '<tr>'+
                    '<td id="selectAtasan">'+
                        '<select name="atasan_id[]" class="form-select form-select-solid entitas aset_induk item" data-kt-select2="true" id="atasan'+countatasan+'" data-allow-clear="true" data-placeholder="Pilih Atasan" data-dropdown-parent="#selectAtasan">'+
                            '<option></option>'+
                            @foreach ($pegawai as $item)
                                '<option value="{{ $item->id }}">{{ $item->nama }}</option>'+
                            @endforeach
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<button class="btn btn-danger btn-sm btn-icon me-3"><i class="las la-trash-alt fs-1"></i></button>'+
                    '</td>'+
                '</tr>';
                $('#table-atasan tbody').append(row);
                $('#atasan'+countatasan, '#table-atasan tbody').select2({
                    dropdownParent: $("#winformlebar")
                });
                countatasan++;
            });

            $('#addRekan').on('click', function(e){
                e.preventDefault();
                var row = '<tr>'+
                    '<td id="selectRekan">'+
                        '<select name="rekan_id[]" class="form-select form-select-solid entitas aset_induk item" data-kt-select2="true" id="rekan'+countrekan+'" data-allow-clear="true" data-placeholder="Pilih Rekan" data-dropdown-parent="#selectRekan">'+
                            '<option></option>'+
                            @foreach ($pegawai as $item)
                                '<option value="{{ $item->id }}">{{ $item->nama }}</option>'+
                            @endforeach
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<button class="btn btn-danger btn-sm btn-icon me-3"><i class="las la-trash-alt fs-1"></i></button>'+
                    '</td>'+
                '</tr>';
                $('#table-rekan tbody').append(row);
                $('#rekan'+countrekan, '#table-rekan tbody').select2({
                    dropdownParent: $("#winformlebar")
                });
                countrekan++;
            });

            $('#addTim').on('click', function(e){
                e.preventDefault();
                var row = '<tr>'+
                    '<td id="selectTim">'+
                        '<select name="tim_id[]" class="form-select form-select-solid entitas aset_induk item" data-kt-select2="true" id="tim'+counttim+'" data-allow-clear="true" data-placeholder="Pilih Tim" data-dropdown-parent="#selectTim">'+
                            '<option></option>'+
                            @foreach ($pegawai as $item)
                                '<option value="{{ $item->id }}">{{ $item->nama }}</option>'+
                            @endforeach
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<button class="btn btn-danger btn-sm btn-icon me-3"><i class="las la-trash-alt fs-1"></i></button>'+
                    '</td>'+
                '</tr>';
                $('#table-tim tbody').append(row);
                $('#tim'+counttim, '#table-tim tbody').select2({
                    dropdownParent: $("#winformlebar")
                });
                counttim++;
            });
        });  
        var periode_id = $('#periode').val();
        console.log(periode_id);
        if(periode_id <= 4){
            $('#Atasans,#Rekans,#Tims').hide();
        }else{
        console.log('disini');
            $('#Atasans,#Rekans,#Tims').show();
        }
    });

    $('#table-atasan').on('click', 'button', function(e){
        e.preventDefault();
        var currentRow = $(this).closest('tr');
        $(this).closest('tr').remove();
    });

    $('#table-rekan').on('click', 'button', function(e){
        e.preventDefault();
        var currentRow = $(this).closest('tr');
        $(this).closest('tr').remove();
    });
    $('#table-tim').on('click', 'button', function(e){
        e.preventDefault();
        var currentRow = $(this).closest('tr');
        $(this).closest('tr').remove();
    });

    $('#periode').on('change', function(){
        var periode_id = $(this).val();
        if(periode_id <= 4){
            $('#Atasans,#Rekans,#Tims').hide();
        }else{
            $('#Atasans,#Rekans,#Tims').show();
        }
    });

    function setFormValidate(){
        $('#form-edit').validate({
            rules: {
                name:{
                        required: true
                }               		               		                              		               		               
            },
            messages: {
                name: {
                    required: "Nama wajib diinput"
                }                                      		                   		                   
            },	        
            highlight: function(element) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('is-invalid');
            },
            errorElement: 'div',
            errorClass: 'invalid-feedback',
            errorPlacement: function(error, element) {
                if(element.parent('.validated').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
        submitHandler: function(form){
                var typesubmit = $("input[type=submit][clicked=true]").val();
                $(form).ajaxSubmit({
                    type: 'post',
                    url: urlstore,
                    data: {source : typesubmit},
                    dataType : 'json',
                    beforeSend: function(){
                        $.blockUI({
                            theme: false,
                            baseZ: 2000
                        })    
                    },
                    success: function(data){
                        $.unblockUI();
                        if (data.flag == 'success') {
                            interVal = 1000;
                        }else{
                            interVal = null;
                        } 
                        swal.fire({
                                title: data.title,
                                html: data.msg,
                                icon: data.flag,
                                timer:interVal,
                                buttonsStyling: true,

                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                   
                        if(data.flag == 'success') {
                            $('#winformlebar').modal('hide');
                            datatable.ajax.reload( null, false );
                        }
                    },
                    error: function(jqXHR, exception){
                        $.unblockUI();
                        var msgerror = '';
                        if (jqXHR.status === 0) {
                            msgerror = 'jaringan tidak terkoneksi.';
                        } else if (jqXHR.status == 404) {
                            msgerror = 'Halaman tidak ditemukan. [404]';
                        } else if (jqXHR.status == 500) {
                            msgerror = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msgerror = 'Requested JSON parse gagal.';
                        } else if (exception === 'timeout') {
                            msgerror = 'RTO.';
                        } else if (exception === 'abort') {
                            msgerror = 'Gagal request ajax.';
                        } else {
                            msgerror = 'Error.\n' + jqXHR.responseText;
                        }
                        swal.fire({
                                title: "Error System",
                                html: msgerror+', coba ulangi kembali !!!',
                                icon: 'error',
                                buttonsStyling: true,
                                confirmButtonText: "<i class='flaticon2-checkmark'></i> OK"
                        });	                               
                    }
                });
                return false;
        }
        });		
    }
</script>
